<?php

// change the following paths if necessary

$debugger =dirname(__FILE__).'/protected/extensions/yii-debug/helpers/Debugger.php';
$global = dirname(__FILE__).'/protected/helpers/global.php';


require_once($debugger);
require_once ($global);
//require_once ($bootstrap);
//require_once ($bootstrapIcon);
//require_once ($bootstrap);
Debugger::init(__DIR__ . '/protected/runtime/debug');
$yii=dirname(__FILE__).'/../yii/framework/yii.php';
$config=__DIR__ . '/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();


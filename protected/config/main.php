<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'AptitudeCenter.com - Best Place to Prepare for Tests and Exams',

	// preloading 'log' component
	'preload'=>array('log'),
        'language' => 'en_us',

	// path aliases
	'aliases' => array(
		'ext' => 'protected/extensions',
		'bootstrap' => 'ext.yiistrap',
		'yiiwheels' => 'ext.yiiwheels',
	
	),

	// autoloading model and component classes
	'import' => array(
		'application.helpers.*',
		//'application.models.ar.*',
		'application.models.form.*',
		'application.components.*',
		'ext.yii-emailer.models.*',
                'bootstrap.helpers.TbHtml',
               'application.modules.user.models.*',
                'application.modules.test.models.*',
                'application.modules.yii-forum.models.*',
                'application.modules.user.controllers.YumController',
                 'application.modules.yiiseo.models.*',
	),
        
        'modules' => array(
		// uncomment the following to enable the auth module
		/*
		'auth' => array(
			'class' => 'vendor.crisu83.yii-auth.AuthModule',
		),
		*/
                'user' => array(
                    'debug' => false,
                    'userTable' => 'user',
                    'translationTable' => 'translation',
                    'loginType'=>7,
                    'hybridAuthProviders'=>array('Facebook', 'Twitter')
                ),
                'usergroup' => array(
                'usergroupTable' => 'usergroup',
                'usergroupMessageTable' => 'user_group_message',
                ),
                'membership' => array(
                'membershipTable' => 'membership',
                'paymentTable' => 'payment',
                ),
                //'friendship' => array(
                //'friendshipTable' => 'friendship',
                //),
                'profile' => array(
                'privacySettingTable' => 'privacysetting',
                'profileTable' => 'profile',
                'profileCommentTable' => 'profile_comment',
                'profileVisitTable' => 'profile_visit',
                ),
                'role' => array(
                'roleTable' => 'role',
                'userRoleTable' => 'user_role',
                'actionTable' => 'action',
                'permissionTable' => 'permission',
                ),
                //'message' => array(
                //'messageTable' => 'message',
                //),
                'registration'=> array(
                    'enableActivationConfirmation'=>false,
                ),
                'avatar',
		
            
                  //gii  modules
                'gii' => array(
                    'class'=>'system.gii.GiiModule',
                    'password'=>'plough',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
                    'ipFilters'=>array('127.0.0.1','::1'),
                    'generatorPaths' => array('bootstrap.gii'),
                ),
                // menubuilder
                'menubuilder'=>array(
                //'theme'=>'bootstrap', //comment for bluegrid theme (=default)
                        'checkInstall'=>false, //uncomment after first usage
                        //'cacheDuration'=> -1, //uncomment for disabling the menucaching
                        'languages'=>array('en_us'),
                        'supportedScenarios'=>array('backend' => 'Backend', 'frontend' => 'Frontend', 'notLoggedIn' => 'NotLoggedIn',),
                        'dataFilterClass'=>'MyDataFilter',
                        //set EMBDbAdapter to switch to mysql (checkInstall=>true on first run)
                        'dataAdapterClass'=> 'EMBDbAdapter', //'EMBMongoDbAdapter',

                        //the available menus/lists for the preview
                        'previewMenus'=>array(
                            'superfish'=>'Superfish',
                            'mbmenu'=>'MbMenu',
                           // 'bootstrapnavbar'=>'Bootstrap Navbar',
                            //'bootstrapmenu'=>'Bootstrap Menu',
                           'dropdownlist'=>'Dropdownlist',
                            'unorderedlist'=>'Unordered list'
                        )
                ),
                 'test',
                  'forum'=>array(
                                'class'=>'application.modules.yii-forum.YiiForumModule',
                            ),
              ),
	/*'modules'=>array(
		// uncomment the following to enable the Gii tool
		/*
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'Enter Your Password Here',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	
	), */

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'class' => 'application.modules.user.components.YumWebUser',
                        'allowAutoLogin'=>true,
                        'loginUrl' => array('//user/user/login'),
		),
                 'format' => array(
                    'class' => 'ext.yii-formatter.components.Formatter',
                   'formatters' => array(
			),// global formatter configurations (name=>config)
                ),
                            // uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                                'testType/<id:\d+>-<name>.html'=>'test/testtype/update',
                                '<id:\d+>-<name>.html'=>'test/testcategory/list',
                               'tests/<id:\d+>-<name>.html'=>'test/tests/details',
                                'contact.html'=>'site/contact',
                                '<view>.html' => 'site/page',
                                ''=>'site/index',
                                
			),
		),
		
		
                'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',
		),
                'yiiwheels' => array(
                   'class' => 'yiiwheels.YiiWheels',   
              ),
                'seo'=>array(
                'class' => 'application.modules.yiiseo.components.SeoExt',
                 ),
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=apptest',
			'emulatePrepare' => true,
			'username' => 'dele',
                        'tablePrefix' => '',
			'password' => 'plough',
			'charset' => 'utf8',
		),
                
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				
				array(
					'class'=>'CWebLogRoute',
				),
			
			),
		),
                'cache' => array('class' => 'system.caching.CDummyCache'),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'bamidele.alegbe@gmail.com',
	),
);
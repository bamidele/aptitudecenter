<?php
Yii::import('zii.widgets.CPortlet');

class YumUserMenu extends CPortlet {
	public function init() {
		$this->title = sprintf('%s: %s',
				Yum::t('Logged in as'),
				Yii::app()->user->data()->username);

		$this->contentCssClass = 'menucontent';
		return parent::init();
	}

	public function run() {
		
                    $this->widget('bootstrap.widgets.TbNav', array(
                            'type' => TbHtml::NAV_TYPE_LIST,
                            'items' => $this->getMenuItems(),
                            ));
    			

		parent::run();
	}

	public function getMenuItems() {
		return array(
				array('label' => 'Profile', 'visible' => Yum::hasModule('profile')), 
				array('label' => 'My profile', 'url' => array(
								'//profile/profile/view')),
				array('label' => 'Edit personal data', 'url' => array(
								'//profile/profile/update')),
				array(
							'label' => 'Upload avatar image',
							'url' => array('/avatar/avatar/editAvatar'),
							'visible' => Yum::hasModule('avatar'),
							),
                                 TbHtml::menuDivider(),  
                                 array('label' => 'Bundles'),
                                 array('label'=>mt('Buy a Bundle'),'url'=>array('//test/bundle/buybundle'),'visible'=>!user()->isGuest),
				array('label'=>mt('Premium Bundles'),'url'=>array('//test/bundle/premium')),
                                array('label'=>mt('Free Bundles'),'url'=>array('//test/bundle/free')),
                               array('label'=>mt('Subscribed Bundles'),'url'=>array('//test/bundle/subscribed'),'visible'=>!user()->isGuest),	
				
                               //TbHtml::menuDivider(),
				//array('label' => 'Membership','visible' => Yum::hasModule('membership')),
                                //array('label' => 'My memberships', 'url' => array('/membership/membership/index')),
				//array('label' => 'Buy memberships', 'url' => array('/membership/membership/order')),
                                TbHtml::menuDivider(),
				array('label' => 'Misc'),
				array('label' => 'Change password', 'url' => array('//user/user/changePassword')),
				array('label' => 'Delete account', 'url' => array('//user/user/delete')),
				array('label' => 'Logout', 'url' => array('//user/user/logout')),
                                    );
					
	}
}
?>







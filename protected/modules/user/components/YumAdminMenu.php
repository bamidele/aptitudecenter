<?php

Yii::import('zii.widgets.CPortlet');

class YumAdminMenu extends CPortlet {
	public function init() {
		$this->title = sprintf('%s: %s',
				Yum::t('Logged in as'),
				Yii::app()->user->data()->username);
		$this->contentCssClass = 'menucontent';
		return parent::init();
	}

	public function run() {
		$this->widget('bootstrap.widgets.TbNav', array(
                            'type' => TbHtml::NAV_TYPE_LIST,
                            'items' => $this->getMenuItems(),
                            ));

		parent::run();
	}

	public function getMenuItems() {
		return array(
                    array('label'=>'Users'), 
			
                    array('label'=> 'Statistics', 'url'=>array('//user/statistics/index')),
                    array('label' => 'User Administration', 'url' => array('//user/user/admin')),
                    array('label' => 'Avatar administration', 'url' => array('//avatar/avatar/admin'), 'visible' => Yum::hasModule('avatar')),
                    array('label' => 'Create new User', 'url' => array('//user/user/create')),
                     TbHtml::menuDivider(),
                    array('label' => 'Tests Management'),
                    
                    array('label' => 'Create new Test', 'url' => array('//test/tests/create')),
                    array('label' => 'Manage Tests', 'url' => array('//test/tests/admin')),
                    array('label' => 'Create new Test Category', 'url' => array('//test/testcategory/create')),
                    array('label' => 'Manage Test Category', 'url' => array('//test/testcategory/admin')),
                    array('label'=>'Create Bundle', 'url'=>array('//test/bundle/create')),
                    array('label'=>'Manage Bundle', 'url'=>array('//test/bundle/admin')),
                    array('label'=>'Manage Transaction', 'url'=>array('//test/bundle/viewtransaction')),
                    TbHtml::menuDivider(),
                
                    
                 array('label'=>'Roles / Access control','active' => Yii::app()->controller->id == 'role' || Yii::app()->controller->id == 'permission' || Yii::app()->controller->id == 'action', 'visible' => Yum::hasModule('role')),
                array('label' => 'Roles', 'url' => array('//role/role/admin')),
                array('label' => 'Create new role', 'url' => array('//role/role/create')),
                array('label' => 'Permissions', 'url' => array('//role/permission/admin')),
                array('label' => 'Grant permission', 'url' => array('//role/permission/create')),
                array('label' => 'Actions', 'url' => array('//role/action/admin')),
                array('label' => 'Create new action', 'url' => array('//role/action/create')),
                    TbHtml::menuDivider(),
						
	array('label'=>'Membership','visible' => Yum::hasModule('membership')), 
        array('label' => 'Ordered memberships', 'url' => array('//membership/membership/admin')),
        array('label' => 'Payment types', 'url' => array('//membership/payment/admin')),
        array('label' => 'Create new payment type', 'url' => array('//membership/payment/create')),
          
                array('label'=>'Profiles','visible' => Yum::hasModule('profile')),
			
                array('label' => 'Manage profiles', 'url' => array('//profile/profile/admin')),
               // array('label' => 'Show profile visits', 'url' => array('//profile/profile/visits')),
                
		array('label' => 'Misc'),
                    TbHtml::menuDivider(),
                    
	
            array('label' => 'Text translations', 'url' => array('//user/translation/admin')),
            array('label' => 'Upload avatar for admin', 'url' => array('//avatar/avatar/editAvatar'),
                    'visible' => Yum::hasModule('avatar')),
            array('label' => 'Change admin Password', 'url' => array('//user/user/changePassword')),
            array('label' => 'Logout', 'url' => array('//user/user/logout')),
           
	);

	}
}
?>







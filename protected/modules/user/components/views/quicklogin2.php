<div   style="padding: 10px;">
<?php 
$url = url('user/auth');
echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_VERTICAL,$url); 
    
    $link = '//' .Yii::app()->controller->uniqueid .
    '/' . Yii::app()->controller->action->id;
    echo TbHtml::hiddenField('quicklogin', $link);
    ?>
    
        <?php echo TbHtml::errorSummary($model); 
        
       echo TbHtml::activeTextFieldControlGroup($model,'username',
    array('label'=>false,'placeholder' => 'Username')); 
     echo TbHtml::activePasswordFieldControlGroup($model, 'password',
    array('label'=>false,'placeholder' => 'Password')); 
    echo (Yum::hasModule('registration')&& Yum::module('registration')->enableRecovery)?
                             TbHtml::link(Yum::t('Lost password?'), Yum::module('registration')->recoveryUrl):"";
                
      ?>
      
        <?php
			?>
        <?php echo TbHtml::formActions(
              array(
                TbHtml::submitButton('Login', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)),
                 TbHtml::button("Sign Up", array(
                     'url'=>array('/registration/registration'),
                     'color'=> TbHtml::BUTTON_COLOR_SUCCESS,
                     'id'=>'register',
                 )),
                  )); ?>
    <?php foreach(Yum::module()->hybridAuthProviders as $provider) 
	echo CHtml::link(
			CHtml::image(
				Yii::app()->getAssetManager()->publish(
					Yii::getPathOfAlias(
						'application.modules.user.assets.images').'/'.strtolower($provider).'.png'),
				$provider) .'',url(
					'//user/auth/login', array('hybridauth' => $provider)), array(
					'class' => 'social')) . ' <br><br> '; ?>
        
    <?php echo TbHtml::endForm(); ?>
</div>

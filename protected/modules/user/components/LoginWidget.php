<?php
Yii::import('zii.widgets.CPortlet');

class LoginWidget extends CPortlet
{
	public $view = 'quicklogin2';
	public $title = null;

	public function init()
	{
		if($this->title === NULL)
		parent::init();
	}

	protected function renderContent()
	{
		$this->render($this->view, array('model' => new YumUserLogin()));
	}
} 
?>

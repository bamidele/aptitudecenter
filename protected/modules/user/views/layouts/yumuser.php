<?php 
Yii::app()->clientScript->registerCssFile(
		Yii::app()->getAssetManager()->publish(
			Yii::getPathOfAlias('YumModule.assets.css').'/yum.css'));

$this->beginContent(Yum::module()->baseLayout); ?>

<!--<div class="row-fluid">
<div class="span2">
    <script type="text/javascript">
ch_client = "bamidele";
ch_width = 120;
ch_height = 600;
ch_type = "mpu";
ch_sid = "Chitika Default";
ch_color_site_link = "C40707";
ch_color_title = "C40707";
ch_color_border = "FFFFFF";
ch_color_text = "232D65";
ch_color_bg = "FFFFFF";
</script>
<script src="http://scripts.chitika.net/eminimalls/amm.js" type="text/javascript">
</script>
</div>-->
<div class="span9">
<?php
if (Yum::module()->debug) {
	echo CHtml::openTag('div', array('class' => 'yumwarning'));
	echo Yum::t(
			'You are running the Yii User Management Module {version} in Debug Mode!', array(
				'{version}' => Yum::module()->version));
	echo CHtml::closeTag('div');
}

 Yum::renderFlash(); 

if($this->title)
	printf('<h1> %s </h1>', $this->title);  
	echo $content;  
?>
</div>

<div class="span3">
 <div class="well">
<?php $this->renderMenu(); ?>
  </div>
</div>
</div>

<?php $this->endContent(); ?>

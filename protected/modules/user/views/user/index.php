<?php
//$this->title = Yum::t('Users');
$this->breadcrumbs=array(Yum::t("Users"));
?>
   
   <?php echo TbHtml::tabbableTabs(array(
    array('label' => TbHtml::lead('Recents tests and Scores'), 'active' => true, 'content' => '<p>I\'m in Section 1.</p>'),
    array('label' =>  TbHtml::lead('Recommended Test'), 'content' => '<p>Howdy, I\'m in Section 2.</p>'),
    array('label' =>  TbHtml::lead('Most Popular Test'), 'content' => '<p>What up girl, this is Section 3.</p>'),
    )); ?>
   

<?php $this->widget('zii.widgets.grid.CGridView', array(
			'dataProvider'=>$dataProvider,
			'columns'=>array(
		array(
			'name' => 'username',
			'type'=>'raw',
			'value' => 'CHtml::link(CHtml::encode($data->username),
				array("//profile/profile/view","id"=>$data->id))',
			),
		array(
			'name' => 'createtime',
			'value' => 'date(UserModule::$dateFormat,$data->createtime)',
		),
		array(
			'name' => 'lastvisit',
			'value' => 'date(UserModule::$dateFormat,$data->lastvisit)',
		),
	),
)); ?>



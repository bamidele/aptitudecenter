<div class="row-fluid">
    <div class="span4">
        
    </div>
<div class="span8">
<h4> <?php echo Yum::t('Register  with  Aptitude center'); ?> </h4>
<p>Join aptitudecenter.com - your best site studying and exam success
Membership on Aptitude Center offers you unlimited tests and practice
questions for job interviews, university entrance exams and general knowledge,
Our Database have over <b><?php echo Questions::model()->count();?></b> questions!</p>  

<?php $this->breadcrumbs = array(Yum::t('Registration')); ?>
<?php $activeform = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id'=>'registration-form',
			'enableAjaxValidation'=>true,
			'enableClientValidation'=>true,
			'focus'=>array($form,'username'),
    
			));
?>

<?php echo Yum::requiredFieldNote(); ?>
<?php echo CHtml::errorSummary(array($form, $profile)); ?>

<?php

    echo $activeform->textFieldControlGroup($form,'username');
?> 
 <?php
    echo $activeform->textFieldControlGroup($profile,'email');
?> 
<?php
    echo $activeform->textFieldControlGroup($profile,'Telephone');
?>

<?php
echo $activeform->textFieldControlGroup($profile,'firstname');
?> 
 <?php
  echo $activeform->textFieldControlGroup($profile,'lastname');
?>

<?php echo $activeform->passwordFieldControlGroup($form,'password'); ?>

<?php echo $activeform->passwordFieldControlGroup($form,'verifyPassword'); ?>

<?php if(extension_loaded('gd') 
			&& Yum::module('registration')->enableCaptcha): ?>
	<div class="row-fluid">
            <div class="span6">
		<?php echo CHtml::activeLabelEx($form,'verifyCode'); ?>
		
		<?php $this->widget('CCaptcha'); ?>
		<?php echo CHtml::activeTextField($form,'verifyCode'); ?>
		
		<p class="hint">
		<?php echo Yum::t('Please enter the letters as they are shown in the image above.'); ?>
		<br/><?php echo Yum::t('Letters are not case-sensitive.'); ?>
                </p>
	</div>
        </div>
	<?php endif; ?>
	
	

		<?php echo CHtml::submitButton(Yum::t('Registration'), array('class'=>'btn')); ?>
       
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

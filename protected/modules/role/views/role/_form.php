
<div class="form">

<?php echo TbHtml::beginForm(); ?>

<?php echo Yum::requiredFieldNote(); ?>

<?php echo TbHtml::errorSummary($model); ?>

<div class="row-fluid">
<?php echo TbHtml::activeLabelEx($model,'title'); ?>
<?php echo TbHtml::activeTextField($model,'title',array('size'=>20,'maxlength'=>50)); ?>
<?php echo TbHtml::error($model,'title'); ?>
</div>
               
<div class="row-fluid">
<?php echo TbHtml::activeLabelEx($model,'description'); ?>
<?php 
                    $attribute = 'description';
                    $value = (isset($model->description))? $model->description:'';
 ?>
       
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>'description',
                        'value'=>$value,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attribute
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attribute
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attribute
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));
                ?> 
                
<?php echo TbHtml::error($model,'description'); ?>
</div>	


<?php if(Yum::hasModule('membership')) { ?>
<div class="row-fluid">
    <?php echo TbHtml::activeLabelEx($model,'membership_priority'); ?>
<?php echo TbHtml::activeTextField($model, 'membership_priority'); ?>
<div class="help-block">
<?php echo Yum::t('Leave empty or set to 0 to disable membership for this role.'); ?>
<?php echo Yum::t('Set to >0 to enable membership for this role and set a priority.'); ?>
<?php echo Yum::t('Higher is usually more worthy. This is used to determine downgrade possibilities.'); ?>
</div>
</div>
<div class="row-fluid">
<?php echo TbHtml::activeLabelEx($model,'price'); ?>
<?php echo TbHtml::activeTextField($model, 'price'); ?>
<?php echo TbHtml::Error($model, 'price'); ?>
</div>
<div class="help-block"> 
<?php echo Yum::t('How expensive is a membership? Set to 0 to disable membership for this role'); ?>
</div>

<div class="row-fluid">
<?php echo TbHtml::activeLabelEx($model,'duration'); ?>
<?php echo TbHtml::activeTextField($model, 'duration'); ?>
<?php echo TbHtml::Error($model, 'duration'); ?>
</div>
<div class="help-block"> 
<?php echo Yum::t('How many days will the membership be valid after payment?'); ?>

</div>
<?php } ?>

<div class="forma-actions">
<?php echo TbHtml::submitButton($model->isNewRecord 
		? Yum::t('Create role') 
		: Yum::t('Save role')); ?>
</div>

<?php echo TbHtml::endForm(); ?>
</div><!-- form -->


<?php if($menu != null): ?>

<?php 
$items= array(
                            array('label'=>mt('Bundles'),'items'=>array( 
                                        array('label'=>mt('Premium Bundles'),'url'=>array('//test/bundle/premium')),
                                        array('label'=>mt('Free Bundles'),'url'=>array('//test/bundle/free')),
                                        array('label'=>mt('Subscribed Bundles'),'url'=>array('//test/bundle/subscribed'),'visible'=>!user()->isGuest)),
                                        ),
                            
                      
                            array('label'=>mt('My Account'), 'visible'=>!user()->isGuest,'items'=>array(
                             
                              array('label'=>mt('My Profile'),'url'=>array('//profile/profile/view'),'visible'=>!user()->isGuest),
                             array('label'=>mt('Edit My Profile'),'url'=>array('//profile/profile/update'),'visible'=>!user()->isGuest),
                             array('label'=>mt('Buy a Bundle'),'url'=>array('//test/bundle/buybundle'),'visible'=>!user()->isGuest),
                             //array('label'=>mt('Membership Subscription'),'url'=>array('//membership/membership/order'),'visible'=>!user()->isGuest),
                             TbHtml::menuDivider(),
                             array('label'=>'Logout ('.  user()->name.')', 'url'=>array('//user/user/logout'), 'visible'=>!user()->isGuest),
                          )),
                      
                        array(
                                        //'icon'=>'icon-tasks icon-white',
                                        'label'=>'Login',
                                        'url'=>'javascript:void(0)',
                                        'visible'=>user()->isGuest,
                                        'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>'dropdown'),
                                        'itemOptions'=>array('class'=>'grey'),
                                        'class'=>'ext.bootstrap-ext.widgets.TbDropdownExt',
                                        'view'=>'_partials._usermenu_loginform',
                                        'submenuOptions'=>array('class' => 'fixed-panel pull-right dropdown-navbar dropdown-caret dropdown-closer'),
                            )
    );

          $final= array_merge($menu,$items);
          
        
        $this->widget('bootstrap.widgets.TbNavbar', array(
            'collapse' => true,
            'display'=> TbHtml::NAVBAR_DISPLAY_NONE,
            'brandLabel'=>false,
            'items' => array(
                array(
                    //'class' => 'bootstrap.widgets.TbNav',
                    //'encodeLabel'=>false,
                   
                    //'items' => $items,
                    'class'=>'ext.bootstrap-ext.widgets.TbMenuExt',
                    'htmlOptions'=>array('class'=>'pull-right'),
                    'items'=>$final,
                    
                )
            ),
            
        ));

?>

<?php endif; ?>
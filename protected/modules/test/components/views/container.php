<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div id="editor" class="">
<?php foreach($model as $m): ?>
    
  <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'questions-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>
    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'test_id',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'questions_numbers',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'question',array('span'=>5,'maxlength'=>2000)); ?>

            <?php echo $form->textFieldControlGroup($model,'option_a',array('span'=>5,'maxlength'=>100)); ?>

            <?php echo $form->textFieldControlGroup($model,'option_b',array('span'=>5,'maxlength'=>100)); ?>

            <?php echo $form->textFieldControlGroup($model,'option_c',array('span'=>5,'maxlength'=>100)); ?>

            <?php echo $form->textFieldControlGroup($model,'option_d',array('span'=>5,'maxlength'=>100)); ?>

            <?php echo $form->textFieldControlGroup($model,'answer',array('span'=>5,'maxlength'=>1000)); ?>

            <?php echo $form->textFieldControlGroup($model,'option_e',array('span'=>5,'maxlength'=>100)); ?>

            <?php echo $form->textFieldControlGroup($model,'working',array('span'=>5,'maxlength'=>1000)); ?>

            <?php echo $form->textFieldControlGroup($model,'date_created',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'user_id',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>
<?php endforeach; ?> 
  
</div>

<?php $this->widget('CLinkPager', array(
    'pages' => $pages,
)) ?>
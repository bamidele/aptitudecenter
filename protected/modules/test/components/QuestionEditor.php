<?php

/**
	* QuestionEditor class file.
	*
	* @author Alegbe Bamidele <bamidele.alegbe@gmail.com>

	* @copyright Copyright &copy; 2013
	* Dual licensed under the MIT and GPL licenses:
	* http://www.opensource.org/licenses/mit-license.php
	* http://www.gnu.org/licenses/gpl.html
	*
	* QuestionEditor - yii extension
	* - create a photo slideshow
	* version: 1.0

	* To use slider:
	* Extract archive in your extensions folder
	* In the site root create a folder named "images" and copy content of image folder from archive
*/

class QuestionEditor  extends CWidget{
    
    public $title;
    
    public $jsPath='js/';
        /**
         * @var string  $csspath -- path to css folder
         */
    public $cssPath='css/';
   
   public $testId; 
   
   protected $_model;
    public function publishAssets()
        {       
		$assets = Yii::app()->getModule('test')->getAssetsPath();
	

		if(is_dir($assets)){
			$this->registerCss($assets);
                        $this->registerJs($assets);
                        $this->registerInitScript();
		}else
			throw new Exception(404, 'Slider - '.Yii::t('app', 'Error: Folder doesn\'t exists.'));
	}
    protected function registerCss($assets){
        $assets.=$this->cssPath;
          css($assets.'');
    }
    
    
    protected function registerJs($assets){
        $assets.=$this->jsPath;
        js($assets.'',CClientScript::POS_HEAD);
    }
    protected function registerInitScript(){
        
        
    }
    public function getPages()
    {
     
      return $pages;

    }
    public function loadModel(){
        
    
    // results per page
     
        $this->_model=  Questions::model()->getQuestionsbyid($this->testid);
        return $this->_model;
    }
    public function init(){
        
        $this->publishAssets();
        parent::init();
        
    } 
    
    
    public function run(){
        
     $criteria=new CDbCriteria();
     //$criteria->select = array('id', 'type', 'author_id', 't.when', 'url', 'project_id', 'subject', 'description', 'DATE(t.when) as theday');
     $criteria->condition = 'test_id = :test_id';
     $criteria->with =array('question_to_test.id_test','question_to_test.test_name', 'question_to_user.id');
     $criteria->params = array('test_id' => $this->testId);
     $criteria->order = 't.questions_numbers ASC';

     $count=Questions::model()->count($criteria);
     $pages=new CPagination($count);
     $pages->pageSize = 1;
     $pages->applyLimit($criteria);
     $models=Questions::model()->findAll($criteria);
     
            
     $this->render('container',array('model'=>$models,'pages'=>$pages));
    }
    
}

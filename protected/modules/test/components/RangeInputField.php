<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class RangeInputField extends CInputWidget
{
    public $attributeFrom;
    public $attributeTo;
    public $nameFrom;
    public $nameTo;
    public $valueFrom;
    public $valueTo;
    public $labelFrom;
    public $labelTo;
    function run()
    {
        if($this->hasModel())
        {
                     echo TbHtml::activeTextFieldControlGroup($this->model,$this->attributeFrom,
                     array('label'=>'From',
                         'size'=>TbHtml::INPUT_SIZE_MINI,
                         //'controlOptions' => array('before' => TbHtml::submitButton('Sign in'))
                         ));
                     echo ' &rarr; ';
                     echo TbHtml::activeTextFieldControlGroup($this->model, $this->attributeTo);
        }
        else {
                echo TbHtml::textFieldControlGroup($this->nameFrom, $this->valueFrom,array(
                    'label'=>  $this->labelFrom,
                     'size'=>TbHtml::INPUT_SIZE_MINI,
                    'disabled'=>true,
                         
                         'controlOptions' => array('after' => '&rarr;  '.$this->labelTo.' :'.TbHtml::textField($this->nameTo, $this->valueTo,
                                                    array(
                                                    
                                                 'size'=>TbHtml::INPUT_SIZE_MINI
                                 ,
                                                        )
                                 ),'style'=>'inline')
                         ));
                //echo ' &rarr; ';
                 //echo TbHtml::textFieldControlGroup($this->nameTo, $this->valueTo);
                }
    }
}
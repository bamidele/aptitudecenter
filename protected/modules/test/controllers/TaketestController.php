<?php

class TakeTestController extends Controller
{
    
    public $layout='//layouts/column1';
    public $category_tree= array();
        public function behaviors()
        {
            return array(
                
                'seo'=>array('class'=>'ext.seo.components.SeoControllerBehavior'),
            );
        }
       public function work($data){
           $s= '<ul>';
            foreach ($data as $key =>$value){
                if(isset($value['id'])){
                    $s.='<li>'.$value.'</li>';
                    if($isset($value['children'])){
                        $this->work($value['children']);
                    }
                }
            }
            $s.='</ul>';
            return $s;
       } 
     public function actionDo(){
         $tree= $this->getTreeRecursive();
         $this->render('do', array('tree'=>$tree));
         

     }
     
        public function getTreeRecursive() {
        $criteria = new CDbCriteria;
        $criteria->order = 'root, lft';
        $criteria->condition = 'level = 1';
        $categories = Testcategory::model()->findAll($criteria);

        foreach($categories as $n => $category) {
            $category_r = array(
                'label'=>$category->cat_name,
                'url'=>"test/testcategory/list/id/$category->cat_id",
                //'level'=>$category->level,
            );              
            $this->category_tree[$n] = $category_r;

            $children = $category->children()->findAll();
            if($children)
                $this->category_tree[$n]['items'] = $this->getChildren($children);
        }
        return $this->category_tree;
    }
    private function getChildren($children) {
        $result = array();
        foreach($children as $i => $child) {
            $category_r = array(
                'label'=>$child->cat_name,
                'url'=>"test/testcategory/list/id/$child->cat_id",
            );          
            $result[$i] = $category_r;
            $new_children = $child->children()->findAll();
            if($new_children) {
                $result[$i]['items'] = $this->getChildren($new_children);
            }           
        }
        return $result_items = $result;
    }
     
     function printNonRec(){
                $criteria=new CDbCriteria;
                $criteria->order='t.lft'; // or 't.root, t.lft' for multiple trees
                $categories=Cat::model()->findAll($criteria);
                $level=0;

                foreach($categories as $n=>$category)
                {
                   if($category->level==$level)
                       echo CHtml::closeTag('li')."\n";
                   else if($category->level>$level)
                       echo CHtml::openTag('ul')."\n";
                   else
                   {
                       echo CHtml::closeTag('li')."\n";

                       for($i=$level-$category->level;$i;$i--)
                       {
                           echo CHtml::closeTag('ul')."\n";
                           echo CHtml::closeTag('li')."\n";
                       }
                   }

                   echo CHtml::openTag('li');
                   echo CHtml::link($category->title,array($category->url));
                   $level=$category->level;
                }

                for($i=$level;$i;$i--)
                {
                   echo CHtml::closeTag('li')."\n";
                   echo CHtml::closeTag('ul')."\n";
                }

     }
    public function actionFinish(){
        
         $taken= TestTakenBy::model()->findByPk($_POST['tid']);
         
         $criteria=new CDbCriteria();
             //$criteria->select = array('id', 'type', 'author_id', 't.when', 'url', 'project_id', 'subject', 'description', 'DATE(t.when) as theday');
        $criteria->condition = 'test_id = :test_id';
        $criteria->with =array('question_to_test','question_to_user','q_to_p','question_to_test.test_to_takenBy');
        $criteria->params = array('test_id' =>$taken->id_test);
        $criteria->order = 't.questions_numbers ASC';
        $test= Tests::model()->findByPk($taken->id_test);
      
        $count=Questions::model()->count($criteria);
        $questions=Questions::model()->findAll($criteria);
         $oans=array();
         $cnt=1;
        foreach($questions as $q){
            $oans[$cnt]= $q->answer;
            $cnt++;
        }
        $ans=array();
        $attempt=0;
        for ($i=1;$i<=$test->num_of_questions; $i++)
        {
            
          if(isset($_POST['group'.$i.''])){
            $p= $_POST['group'.$i.'']; //get answers
           
            $ans[$i]   = $_POST['group'.$i.'']; //if attempted
              $attempt++;
          }
          else{
                    $ans[$i]=0;
            }
        }
      
        $mark=0;
        for($i=1;$i<=$test->num_of_questions; $i++){
            
            if($ans[$i]==$oans[$i])
                {
                     $mark++;
                }
        } 
        
        $timestaken=$test->timestaken+1;
        $totalscores=$test->totalscores+$mark;
         $dbCommand = Yii::app()->db->createCommand("
                            UPDATE tests SET timestaken= '$timestaken', totalscores='$totalscores'
                               
                               WHERE id_test= $taken->id_test
               ")->execute();
         
       
        $taken->total_score= $mark;
        $taken->total_question= $test->num_of_questions;
        $taken->question_attempted= $attempt;
        $taken->date_taken= time();
        if($taken==null)
            $taken= new TestTakenBy();
        if($taken->save(false)){
            
            $this->render('showstats',array('score'=>$mark,'number_question'=>$test->num_of_questions
                    ,'attempted'=>$attempt, 'test'=>$test));
               
            Yii::app()->end();
            
        }
        
        
        
       
        //$this->redirect('index');
        
    }
    public function actionTest(){
        
        $tid= $_GET['tid'];
        $testreg=TestTakenBy::registerTest($tid);
        if($testreg!=0){
            
            
            $criteria2=new CDbCriteria();
            $criteria2->condition = 'id = :id';
            $criteria2->with =array('takenBy_to_test');
            $criteria2->params = array('id' =>$testreg);
            
            $test= TestTakenBy::model()->find($criteria2);
            
            
            if($test->attempt==0){
              $test->attempt= 1;
              $test->date_taken= time();
              
              $test->save();
       
             $criteria=new CDbCriteria();
             //$criteria->select = array('id', 'type', 'author_id', 't.when', 'url', 'project_id', 'subject', 'description', 'DATE(t.when) as theday');
             $criteria->condition = 'test_id = :test_id';
             $criteria->with =array('question_to_test','question_to_user','q_to_p','question_to_test.test_to_takenBy');
             $criteria->params = array('test_id' =>$tid);
             $criteria->order = 't.questions_numbers ASC';

             $count=Questions::model()->count($criteria);
             
             $models=Questions::model()->findAll($criteria);
        
             $this->render('test',array('models'=>$models, 'count'=>$count, 'testReg'=>$test));
            }
            else{
                
                throw new CHttpException(400,'Cant take this particular test  twice, quit and  load a again  from  the index page');
            }
        }
      
    }
    
    public function actionPracticeTest(){
        
        $tid= $_GET['tid'];
        TestTakenBy::registerPracticeTest($tid);
        $criteria=new CDbCriteria();
             //$criteria->select = array('id', 'type', 'author_id', 't.when', 'url', 'project_id', 'subject', 'description', 'DATE(t.when) as theday');
        $criteria->condition = 'test_id = :test_id';
        $criteria->with =array('question_to_test','q_to_p','question_to_test.test_to_tt');
        $criteria->params = array('test_id' =>$tid);
        $criteria->order = 't.questions_numbers ASC';

        $count=Questions::model()->count($criteria);
        $pages=new CPagination($count);
        $pages->pageSize = 5;
        $pages->applyLimit($criteria);
        $models=Questions::model()->findAll($criteria);

        $test= Tests::model()->findByPk($tid);
        $catid= Tests::getCatID($tid);
        
        $this->render('practicetest',array('catid'=>$catid,'test'=>$test, 'models'=>$models,'pages'=>$pages));
            
             
    }
           
     public function actionUpdateForum(){
         
         $criteria=new CDbCriteria();
             //$criteria->select = array('id', 'type', 'author_id', 't.when', 'url', 'project_id', 'subject', 'description', 'DATE(t.when) as theday');
        $criteria->condition = 'topic_id IS :n';
        $criteria->with= array('question_to_test','q_to_p','question_to_test.test_to_tt');
        $criteria->limit = 100;
        $criteria->params = array(':n' =>NULL);
        $Questions=Questions::model()->findAll($criteria);
        $count = Questions::model()->count($criteria);
        echo $count;
        $forum = Forum::model()->findByPk(8);
        if(null == $forum)
            throw new CHttpException(404, 'Forum not found.');
        if($forum->is_locked)
            throw new CHttpException(403, 'Forum is locked.');
        
        $cnt=0;
         foreach($Questions as $q){
             $model=new PostForm;
             if($q->topic_id==null){
             $model->setScenario('create');
             
             $model->lockthread=0;
             $model->subject= $q->question_to_test->test_name." - Question ".$q->questions_numbers;
             }
             $content='<div class=row-fluid>';
             $content.= '<h6>Test : '.$q->question_to_test->test_name.'</h6>';
             if($q->q_to_p)
             $content.= '<p>'.$q->q_to_p->passage.'</p>';
             $content.= '<h6>Question '.$q->questions_numbers.'</h6>';
             
             
             $content.= '<p>'.$q->question.'</p>';
             if($q->question_to_test->test_to_tt->option==1){
              $content.='<table>';
              
              $content.='<tr>
                            <td><p><a>A.</a></p></td>
                            <td>'.$q->option_a .'</td>
                        </tr>';
              $content.='<tr>
                            <td><p><a>B.</a></p></td>
                            <td>'.$q->option_b .'</td>
                        </tr>';
              
              $content.='<tr>
                            <td><p><a>C.</a></p></td>
                            <td>'.$q->option_c .'</td>
                        </tr>';
              if($q->question_to_test->num_of_options >=4){
              $content.='<tr>
                            <td><p><a>D.</a></p></td>
                            <td>'.$q->option_d .'</td>
                        </tr>';
              }
              if($q->question_to_test->num_of_options >=5){
              $content.='<tr>
                            <td><p><a>E.</a></p></td>
                            <td>'.$q->option_e .'</td>
                        </tr>';
              }
              $content.='</table>';
              
             }
             $content.= '<h6> Answer </h6>';
            if($q->question_to_test->test_to_tt->option==1)
             $content.= '<h6>'.Questions::getAnswer ($q->answer).'</h6>';
            
             $content.= '<h6>Explanation</h6>'; 
             $content.= '<p>'.$q->working.'</p>';
             
             $content.='</div>';
             $model->content= $content;
             
             if($model->validate())
            {
                 if($q->topic_id==null){
                $thread = new Thread;
                $thread->forum_id = $forum->id;
                $thread->subject = $model->subject;
                $thread->save(false);
                 
                
                 $post = new Post();
                $post->author_id = Yii::app()->user->forumuser_id;
                $post->thread_id = $thread->id;
                $post->content = $model->content;
                $post->save(false);
                 $t= $thread->id;
                 }
                 else{
                     $post = Post::model()->findByPk($q->post_id);
                     $post->content= $model->content;
                     $post->save();
                     $t= $q->topic_id;
                 }
                 
                $q->topic_id= $t;
                $q->post_id= $post->id;
                $q->save();
               // $this->redirect($thread->url);
         }
        $cnt++;
       }
       
        echo $cnt;
     }
      
    
    
 
       
}
<?php

class QuestionsController extends YumController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/yum';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','massupdate',
                                    'pcreate','passage','editpass','deletepass'),
				'expression' => 'Yii::app()->user->isAdmin()'
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        /**
         * 
         */
        
        public function actionEditpass(){
            
            $arr= array();
            
            if(isset($_POST['passage'])){
                
                $from=$_POST['qid'];
                $passage= $_POST['passage'];
                $criteria=new CDbCriteria();
             //$criteria->select = array('id', 'type', 'author_id', 't.when', 'url', 'project_id', 'subject', 'description', 'DATE(t.when) as theday');
                $criteria->condition = 'qid = :qid';
                $criteria->params = array('qid' =>$from);
                $model = Passages::model()->find($criteria);
                $model->passage= $passage;
                $arr['passage']= $passage;
                if($model->save()){
                    
                    echo je($arr);
                }
            }
        }
        public function actionDeletepass(){
                
                $from=$_GET['id'];
                
                $criteria=new CDbCriteria();
             //$criteria->select = array('id', 'type', 'author_id', 't.when', 'url', 'project_id', 'subject', 'description', 'DATE(t.when) as theday');
                $criteria->condition = 'qid = :qid';
                $criteria->params = array('qid' =>$from);
                $model = Passages::model()->find($criteria);
                $model->delete();
                echo 'success';
        }
        public function actionPassage(){
            if(isset($_POST['ajax'])){
                //$test_id=$_POST['tid'];
                $qNum= $_POST['qum'];
            $criteria=new CDbCriteria();
             //$criteria->select = array('id', 'type', 'author_id', 't.when', 'url', 'project_id', 'subject', 'description', 'DATE(t.when) as theday');
             $criteria->condition = 'qid = :qid';
             $criteria->with =array('q');
             $criteria->params = array('qid' =>$qNum);
             $ques=  Passages::model()->find($criteria);
                
                
              $this->renderPartial('_haspassage'
                        ,array('qNum'=>$qNum,'ques'=>$ques)
                    );
            }
        }
        
        public function actionPcreate()
        {
            $model= new Passages;
            $arr= array();
              $err='';
            if(isset($_POST['passage'])){
                
                $from=$_POST['from'];
                $to=$_POST['to'];
                $passage= $_POST['passage'];
                $err='';
                if($to==null){
                     
                    $err.='<li>'.TbHtml::em('TO cannot be Empty.',
                            array('color' => TbHtml::TEXT_COLOR_ERROR)).'</li>';
                }
                if($to<$from){
                     
                    $err.='<li>'.TbHtml::em('TO cannot be lesser than FROM.',
                            array('color' => TbHtml::TEXT_COLOR_ERROR)).'</li>';
                }
                if($passage==''){
                     $err.='<li>'.TbHtml::em('PASSAGE CANNOT BE  EMPTY!',
                            array('color' => TbHtml::TEXT_COLOR_ERROR)).'</li>';
                }
                
                if($err==''){
                    $qid= $_POST['qid'];
                    $c=0;
                    for($i=$from;$i<=$to;$i++)
                    {
                     $model= new Passages;   
                     $model->qid=$qid+$c;
                     $model->passage=$_POST['passage'];
                     if($model->validate()){
                     if($model->save()){

                     }
                     $c++;
                     }
                    }
                    $arr['passage']=$passage;
                    
                }
                else{
                     $arr['error']=$err;
                    }
                 echo je($arr);
            }
        }
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Questions;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model,'questions-form');

		if (isset($_POST['Questions'])) {
			$model->attributes=$_POST['Questions'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id_question));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                
		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model,'questions-form');

		if (isset($_POST['Questions'])) {
			$model->attributes=$_POST['Questions'];
                        $model->question= $_POST['question'];
                        $option = $_POST['option'];
                        if($option){
                         if(isset($_POST['option_a']))
                        $model->option_a=$_POST['option_a'];
                         if(isset($_POST['option_b']))
                        $model->option_b=$_POST['option_b'];
                         if(isset($_POST['option_c']))
                        $model->option_c=$_POST['option_c'];
                         if(isset($_POST['option_d']))
                        $model->option_d=$_POST['option_d'];
                         if(isset($_POST['option_e']))
                        $model->option_e=$_POST['option_e'];
                         
                        $model->answer  =$_POST['Questions']['answer'];
                            if($_POST['isMultiChoice']){
                            $model->answer  =$_POST['answer'];
                            }
                        }else{
                            $model->answer  =$_POST['answer'];
                        }
                        
                        //$model->answer = $_POST['answer'];
                        $model->working=$_POST['working'];
                        $model->date_created=time();
                        $model->user_id=user()->id;
                        $valid=$model->validate();            
                        if($valid){
                            if ($model->save()) {
                                
                                    $this->updateForum($model->id_question);
                                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS,
                        'File saved!');
                                $this->widget('bootstrap.widgets.TbAlert');
                                //echo CJSON::encode(array('status'=>'success'));

                            }
                       
                        }
                        else{
                                $error = CActiveForm::validate($model);
                                if($error!='[]')
                                    echo $error;
                                Yii::app()->end();
                            }
                        
		}

		//$this->render('update',array(
		//	'model'=>$model,
		//));
	}
        
        public function actionMassupdate($id){

              $criteria=new CDbCriteria();
             //$criteria->select = array('id', 'type', 'author_id', 't.when', 'url', 'project_id', 'subject', 'description', 'DATE(t.when) as theday');
             $criteria->condition = 'test_id = :test_id';
             $criteria->with =array('question_to_test','question_to_user','q_to_p');
             $criteria->params = array('test_id' =>$id);
             $criteria->order = 't.questions_numbers ASC';

             $count=Questions::model()->count($criteria);
             $pages=new CPagination($count);
             $pages->pageSize = 1;
             $pages->applyLimit($criteria);
             $models=Questions::model()->findAll($criteria);
            
           
              if(isset($_POST['ajax'])){
                    if($_POST['ajax']==true){
                        $this->renderPartial('editQuestions',array('models'=>$models,'pages'=>$pages));
                    }
                    else{
                    $this->render('editQuestions',array('models'=>$models,'pages'=>$pages));
                    }
               }
            else{
             $this->render('editQuestions',array('models'=>$models,'pages'=>$pages));
            }
        }
       

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Questions');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Questions('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Questions'])) {
			$model->attributes=$_GET['Questions'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Questions the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id=null)
	{
		$model=Questions::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

        public function updateForum($id){
            
             $criteria=new CDbCriteria();
             //$criteria->select = array('id', 'type', 'author_id', 't.when', 'url', 'project_id', 'subject', 'description', 'DATE(t.when) as theday');
        $criteria->condition = 'id_question =:n';
        $criteria->with= array('question_to_test','q_to_p','question_to_test.test_to_tt');
        
        $criteria->params = array(':n' =>$id);
        $q=Questions::model()->find($criteria);
        
        $forum = Forum::model()->findByPk(8);
        if(null == $forum)
            throw new CHttpException(404, 'Forum not found.');
        if($forum->is_locked)
            throw new CHttpException(403, 'Forum is locked.');
   
             $model=new PostForm;
             if($q->topic_id==null){
             $model->setScenario('create');
             
             $model->lockthread=0;
             $model->subject= $q->question_to_test->test_name." - Question ".$q->questions_numbers;
             }
             $content='<div class=row-fluid>';
             $content.= '<h6>Test : '.$q->question_to_test->test_name.'</h6>';
             if($q->q_to_p)
             $content.= '<p>'.$q->q_to_p->passage.'</p>';
             $content.= '<h6>Question '.$q->questions_numbers.'</h6>';
             
             
             $content.= '<p>'.$q->question.'</p>';
             if($q->question_to_test->test_to_tt->option==1){
              $content.='<table>';
              
              $content.='<tr>
                            <td><p><a>A.</a></p></td>
                            <td>'.$q->option_a .'</td>
                        </tr>';
              $content.='<tr>
                            <td><p><a>B.</a></p></td>
                            <td>'.$q->option_b .'</td>
                        </tr>';
              
              $content.='<tr>
                            <td><p><a>C.</a></p></td>
                            <td>'.$q->option_c .'</td>
                        </tr>';
              if($q->question_to_test->num_of_options >=4){
              $content.='<tr>
                            <td><p><a>D.</a></p></td>
                            <td>'.$q->option_d .'</td>
                        </tr>';
              }
              if($q->question_to_test->num_of_options >=5){
              $content.='<tr>
                            <td><p><a>E.</a></p></td>
                            <td>'.$q->option_e .'</td>
                        </tr>';
              }
              $content.='</table>';
              
             }
             $content.= '<h6> Answer </h6>';
            if($q->question_to_test->test_to_tt->option==1)
             $content.= '<h6>'.Questions::getAnswer ($q->answer).'</h6>';
            
             $content.= '<h6>Explanation</h6>'; 
             $content.= '<p>'.$q->working.'</p>';
             
             $content.='</div>';
             $model->content= $content;
             
             if($model->validate())
            {
                 if($q->topic_id==null){
                $thread = new Thread;
                $thread->forum_id = $forum->id;
                $thread->subject = $model->subject;
                $thread->save(false);
                 
                
                 $post = new Post();
                $post->author_id = Yii::app()->user->forumuser_id;
                $post->thread_id = $thread->id;
                $post->content = $model->content;
                $post->save(false);
                 $t= $thread->id;
                 }
                 else{
                     $post = Post::model()->findByPk($q->post_id);
                     $post->content= $model->content;
                     $post->save();
                     $t= $q->topic_id;
                 }
                 
                $q->topic_id= $t;
                $q->post_id= $post->id;
                $q->save();
               // $this->redirect($thread->url);
            }
           
        }
}
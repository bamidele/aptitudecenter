<?php

class BundleController extends YumController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/yum';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','premium','free'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','buybundle','process',
                                    'doonlinepayment','dofundstransfer','dobankdeposit','subscribed','subscribefree','details'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','viewtransaction','confirmtrans'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
            $model=$this->loadModel($id);
            $tests= Bundle::testWithBundle($id);
		$this->render('view',array(
			'model'=>$model,'tests'=>$tests
		));
	}
        
        
        
        public function actionPremium()
        {
          /* $bundles= Bundle::model()->findAll(array(
                    'condition'=>'isfree=:zero',
                    'params'=>array(':zero'=>0),
                ));
           * */
           
          $bundles= Bundle::getPremium(user()->id);
           $type='premium';
		$this->render('viewbundle',array(
			'model'=>$bundles,'type'=>$type
		));
        }
        public function actionFree()
        {
            $type='free';
           $bundles= Bundle::getFree();
		$this->render('viewbundlefree',array(
			'model'=>$bundles,'type'=>$type
		));
        }
        public function actionSubscribed()
        {
           $bundles= Bundle::getSubBundle(user()->id);
		$this->render('viewbundle2',array(
			'model'=>$bundles
		));
        }
        public function getSubscribed($userid, $bundleid){
            if(Bundle::checkSub($userid,$bundleid)== true){
                Bundle::getSubscribed($userid,$bundleid);
                return true;
            }
            else{
            return false;
            }
        }
        public function actionDetails()
        {
            $bid=$_GET['bid'];
            $tests= Bundle::testWithBundle($bid);
            $bundle= Bundle::model()->findByPk($bid);
            $this->render('_view',array(
			'tests'=>$tests,'data'=>$bundle
            ));
        }
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Bundle;
                $tests= Tests::model()->findAll();

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model,'bundle-form');

		if (isset($_POST['Bundle'])) {
			$model->attributes=$_POST['Bundle'];
                        $model->date_created=time();
                        if(!isset($_POST['Bundle']['isfree'])|| $_POST['Bundle']['isfree']==false)
                        {
                            $model->isfree=0;
                            
                        }else
                        {
                            $model->price=0;
                        }
                     
			if ($model->save()) {
                            foreach($tests as $t){
                             if(isset($_POST[$t->id_test]))
                             {
                                 $model->addTestToBundle($model->idbundle,$t->id_test);
                             }
                            }
				$this->redirect(array('view','id'=>$model->idbundle));
			}
                         
                         
		}

		$this->render('create',array(
			'model'=>$model,'tests'=>$tests
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $testnot= Bundle::testNotInBundle($id);
                $tests= Bundle::testWithBundle($id);
                $test=Tests::model()->findAll();
		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model,'bundle-form');

		if (isset($_POST['Bundle'])) {
			$model->attributes=$_POST['Bundle'];
                        $model->date_created=time();
                        if(!isset($_POST['Bundle']['isfree'])|| $_POST['Bundle']['isfree']==false)
                        {
                            $model->isfree=0;
                        }
                        else
                        {
                            $model->price=0;
                        }
                         Bundle::deleteBundle_test($id);
			if ($model->save()) {
                             foreach($test as $t){
                             if(isset($_POST[$t->id_test]))
                             {
                                 $model->addTestToBundle($model->idbundle,$t->id_test);
                             }
                            }
				$this->redirect(array('view','id'=>$model->idbundle));
			}
		}

		$this->render('update',array(
			'model'=>$model,'tests'=>$tests,'testnot'=>$testnot
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Bundle');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Bundle('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Bundle'])) {
			$model->attributes=$_GET['Bundle'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Bundle the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Bundle::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}
        
        public  function actionBuybundle()
        {
            $this->layout= 'application.views.layouts.main';
		$model = new BundleTransaction() ;
                $bundles= Bundle::getUnsubcribed();
		if(isset($_POST['bundle_id']) || isset($_GET['bundle_id'])) {
                        
			$id= isset($_POST['bundle_id'])? $_POST['bundle_id'] : $_GET['bundle_id'];
                        //print_r($_POST['YumMembership']);
                           $this->redirect(array('process', 'bundle_id'=>$id));
                      
			/*
                        if($model->save()) {
				$this->redirect(array('index'));
			}*/
		}

		$this->render('order',array('model'=>$model,'bundles'=>$bundles));
        }
        public function actionProcess()
        {
            $bundle_id= $_GET['bundle_id'];
            $bundle= Bundle::model()->findByPk($bundle_id);
            $this->layout= 'application.views.layouts.main';
            $model = new BundleTransaction() ;
           
            if(isset($_POST['payment_option'])) {
                 $payment= $_POST['payment_option'];
              switch ($payment){
                 case 3:
                    $this->redirect(array('doonlinepayment','bundle'=> $bundle_id));
                    break;
                case 4:
                    $this->redirect(array('dofundstransfer','bundle'=> $bundle_id));
                    break;
         
                case 6:
                   $this->redirect(array('dobankdeposit','bundle'=> $bundle_id));
                   break;
                
                 }
               
		
            }
		$this->render('order2',array('model'=>$model,'bundle'=>$bundle));
                
        }
	public function actionSuccessonline()
        {
            $bundle= $_GET['bundle'];
            $bund= Bundle::model()->findByPk($bundle);
            $this->render('successonline',array('bundle'=>$bund));
        }
        
        public function actionFailureonline()
        {
            $message= $_GET['message'];
            $this->render('failureonline',array('message'=>$message));
        }
        public function actionDoonlinepayment(){
            $bundle_id= $_GET['bundle'];
            $this->layout= 'application.views.layouts.main';
            //$transaction= new BundleTransaction();
            $bundle= Bundle::model()->findByPk($bundle_id);
            
           $this->render('doonline',array('bundle'=>$bundle));
                
             
        }
        public function actionGetonlinepayment()
        {
            //finish onlne ransaction
            if(isset($_POST['transaction_id'])){
                //get the full transaction details as an xml from voguepay
                $xml = file_get_contents('https://voguepay.com/?v_transaction_id='.$_POST['transaction_id']);
                //parse our new xml
                $xml_elements = new SimpleXMLElement($xml);
                //create new array to store our transaction detail
                $transaction = array();
                //loop through the $xml_elements and populate our $transaction array
                foreach($xml_elements as $key => $value) 
                {
                        $transaction[$key]=$value;
                }
                /*
                Now we have the following keys in our $transaction array
                $transaction['merchant_id'],
                $transaction['transaction_id'],
                $transaction['email'],
                $transaction['total'], 
                $transaction['merchant_ref'], 
                $transaction['memo'],
                $transaction['status'],
                $transaction['date'],
                $transaction['referrer'],
                $transaction['method']
                */

                if($transaction['total'] == 0)
                  $this->redirect(array('failureonline','message'=>'Invalid total'));
                    
                if($transaction['status'] != 'Approved')
                    $this->redirect(array('failureonline','message'=>'Failed transaction'));
                
                
                    //$this->layout= 'application.views.layouts.main';
                    $trans= new BundleTransaction();
                  
                    $trans->trans_id= $transaction['transaction_id'];
                    $trans->user_id= user()->id;
                    $trans->bundle_id= $transaction['memo'];
                    $trans->payment_id= 3;
                    $trans->status=1;
                    $trans->orderdate= time();
                    if($trans->save()){
                        $this->getSubscribed(user()->id, $transaction['memo']);
                       $this->redirect(array('successonline','bundle'=>$trans->bundle_id));    
                    }  
                    else{
                        echo "not saved";
                    }
                }

       }
        /**
         * 
         */
        
        public function actionDofundstransfer(){
            $this->layout= 'application.views.layouts.main';
            if(Bundle::checkTrans(user()->id,$bundle,4)){
                    $transaction= new BundleTransaction();
                    $bundle=$_GET['bundle'];
                    $bund= Bundle::model()->findByPk($bundle);
                    $transaction->trans_id= time().mt_rand(1000,9999);
                    $transaction->payment_id= 4;
                    $transaction->user_id=user()->id;
                    $transaction->status=0;
                    $transaction->bundle_id=$bundle;
                    $transaction->orderdate=time();

                    if($transaction->save()){


                        //$memtype= YumRole::model()->findByPk($mem);
                        $message=  TbHtml::labelTb('Success', array('color' => TbHtml::LABEL_COLOR_SUCCESS)). " You  can  now pay  for  this   {$bund->name} via the Online Transfer !!!";
                        $msg =TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, $message);
                        Yum::setFlash($msg);
                        $user= YumUser::model()->findByPk(user()->id);
                        $this->render('atm', array(
                        'trans'=>$transaction,'bundle'=>$bund,'user'=>$user
                                 ));
                            }
            
                      } else{
                            $message=  TbHtml::labelTb('Failure', array('color' => TbHtml::LABEL_COLOR_WARNING)). " Cannot pay with this method because have already order for this service  check your email!!!";
                            $msg =TbHtml::alert(TbHtml::ALERT_COLOR_DANGER, $message);
                            Yum::setFlash($msg);
                            $this->redirect(array('//profile/profile/view'));
                      }
                      
        }
      
       public function actionDobankdeposit(){
            $this->layout= 'application.views.layouts.main';
            
            
            $bundle=$_GET['bundle'];
            if(Bundle::checkTrans(user()->id,$bundle,6)){
                
            $transaction= new BundleTransaction();
            $bund= Bundle::model()->findByPk($bundle);
            $transaction->trans_id= time().mt_rand(1000,9999);
            $transaction->payment_id= 6;
            $transaction->status=0;
            $transaction->user_id=user()->id;
            $transaction->bundle_id=$bundle;
            $transaction->orderdate=time();
            if($transaction->save()){
                
                //$memtype= YumRole::model()->findByPk($mem);
                $user= YumUser::model()->findByPk(user()->id);
                $message=  TbHtml::labelTb('Success', array('color' => TbHtml::LABEL_COLOR_SUCCESS)). " You  can  now pay  for    {$bund->name} in the Bank!!!";
                $msg =TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, $message);
                Yum::setFlash($msg);
                $this->render('bankdeposit', array(
                'trans'=>$transaction,'bundle'=>$bund,'user'=>$user
                         ));
            }
            }
            else{
                $message=  TbHtml::labelTb('Failure', array('color' => TbHtml::LABEL_COLOR_WARNING)). " Cannot pay with this method because have already order for this service  check your email!!!";
                $msg =TbHtml::alert(TbHtml::ALERT_COLOR_DANGER, $message);
                Yum::setFlash($msg);
                $this->redirect(array('//profile/profile/view'));
            }
        }
        public function actionSubscribefree(){
            
            
            $bundle_id= $_GET['bid'];
            $transaction= new BundleTransaction();
            $transaction->trans_id= time().mt_rand(1000,9999);
            $transaction->payment_id= 0;
            $transaction->user_id=  user()->id;
            $transaction->status=1;
            $transaction->bundle_id=$bundle_id;
            $transaction->orderdate=time();
            if($transaction->save()){
                if($this->getSubscribed(user()->id, $transaction->bundle_id)){
                $bundle= Bundle::model()->findByPk($bundle_id);
               
                $message=  TbHtml::labelTb('Success', array('color' => TbHtml::LABEL_COLOR_SUCCESS)). " You have been subscribed to  {$bundle->name}, Practice  and Enjoy!!!";
                $msg =TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, $message);
                Yum::setFlash($msg);
                $this->redirect(array('//profile/profile/view'));
                }else{
                    $bundle= Bundle::model()->findByPk($bundle_id);
               
                $message=  TbHtml::labelTb('Failure', array('color' => TbHtml::LABEL_COLOR_WARNING)). " You cannot be subscribed to  {$bundle->name}, Sorry";
                $msg =TbHtml::alert(TbHtml::ALERT_COLOR_DANGER, $message);
                Yum::setFlash($msg);
                $this->redirect(array('//profile/profile/view'));
                }
                
            } 
            
        }
        public function actionConfirmtrans($trans_id){
            //$trans= $_POST['trans_id'];
            $trans=$trans_id;
            echo $trans;
            $transaction= BundleTransaction::model()->findByPk($trans);
             $answer= $this->getSubscribed($transaction->user_id, $transaction->bundle_id);
             if($answer){
                    $transaction->status=1;
                    $transaction->update();
             }
           
            
            
        }
        public function actionViewtransaction(){
            
            $model=new BundleTransaction('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Transactionbundle'])) {
			$model->attributes=$_GET['Transactionbundle'];
		}

		$this->render('transadmin',array(
			'model'=>$model,
		));
        }
        
}
<?php

class TestsController extends YumController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/yum';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
                        array('ext.seo.components.SeoFilter + details'),
		);
	}
         public function behaviors()
        {
            return array(

                'seo'=>array('class'=>'ext.seo.components.SeoControllerBehavior'),
            );
        }
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','details','report'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','ajaxupdate','ajaxfree'),
				'expression' => 'Yii::app()->user->isAdmin()'
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        public function actionDetails($id,$cat_id=null){
            
            $this->layout='//layouts/column1';
            if($cat_id==null)
            $model= Tests::getCatID($id);
            
            $test=  Tests::model()->with('test_to_tt')->findByPk($id);
            $criteria=new CDbCriteria;
            
            $rand= rand(1, $test->num_of_questions);
            $criteria->condition='test_id=:tid AND questions_numbers =:qnum';
            $criteria->params=array(':tid'=>$id,':qnum'=>$rand); 
            $question= Questions::model()->find($criteria);
            $cnt=1;
            if($cat_id==null){
            foreach ($model as $m)
            {
                $cat= Testcategory::model()->findByPk($m['cat_id']);
                break;
            }
            }
            else{
                $cat= Testcategory::model()->findByPk($cat_id);
            }
            $this->render('details',array(
			'test'=>$test,
                        'cat'=>$cat,
                        'question'=>$question,
		));
        }
        public function categoryTest($id){
            
            
        }
        
        public function actionAjaxUpdate($id)
        {
            
                $model=$this->loadModel($id);
                
                $model->publish = ($model->publish == 1 ? 0 : 1);
                
                $model->update();
        }
          public function actionAjaxFree($id)
        {
            
                $model=$this->loadModel($id);
                
                $model->isfree = ($model->isfree == 1 ? 0 : 1);
                
                $model->update();
        }
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Tests;
                $model2 = new RangeForm();
		// Uncomment the following line if AJAX validation is needed
                 $this->performAjaxValidation($model,'tests-form');
                 
		if (isset($_POST['Tests'])) {
			$model->attributes=$_POST['Tests'];
                        $model->description=$_POST['description'];
                        $model->timestaken=0;
                        $model->totalscores=0;
                        $model->average_score=0;
                        $model->date_added= time();
                        $model->test_added_by= user()->id;
                        
                        
			if ($model->save()) {
                                 if(isset($_POST['categories'])){
                                   $this->saveCatTest($_POST['categories'],$model->id_test);
                                 }
                                $count= $model->num_of_questions;
                                for($i=1;$i<=$count; $i++){
                                     /*
                                    $qmodel= new Questions;
                                    $qmodel->test_id=$model->id_test;
                                    $qmodel->questions_numbers=$i;
                                    $qmodel->question= 'Not Added';
                                    $qmodel->date_created= time();
                                    $qmodel->user_id= user()->id;
                                    if($qmodel->save()){
                                        
                                    }
                                      * 
                                      */
                              $dbCommand = Yii::app()->db->createCommand("
                  INSERT INTO `questions` (`test_id`, `questions_numbers`, `question`, `date_created`, `user_id`)
                     VALUES ({$model->id_test},{$i},'NotAdded',".time().",".user()->id.")
               ")->execute();
                                }
			$this->redirect(array('questions/massupdate','id'=>$model->id_test));
			}
		}

		$this->render('create',array(
			'model'=>$model,'model2'=>$model2,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=  Tests::model()->findByPk($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Tests'])) {
			$model->attributes=$_POST['Tests'];
                        $model->description=$_POST['description'];
                        $model->date_modified=time();
                         $dbReader = Yii::app()->db->createCommand("
                DELETE FROM cat_test WHERE test_id=$model->id_test
               ")->query();
			if ($model->save()) {
                                if(isset($_POST['categories'])){
                                   $this->saveCatTest($_POST['categories'],$model->id_test);
                                 }
				$this->redirect(array('view','id'=>$model->id_test));
			}
		}
                //echo $model->test_name;

                $this->render('update',array('model'=>$model));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Tests');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Tests('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Tests'])) {
			$model->attributes=$_GET['Tests'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Tests the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id=null)
	{
		$model=Tests::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

	/**
         * Saves the categories and test id into the database  before  creating the questions 
         * @param type $categories
         * @param type $test_id
         */
        protected  function saveCatTest($categories,$test_id){
            
                        
                     $arr= explode(',', $categories);
                     $cats =  Testcategory::model()->findAll();
                     $cnt= 0;
                     foreach($cats as $c){
                         foreach($arr as $a=>$value){
                             $catname = str_replace('-','', $value);
                             if($c->cat_name == $catname){
                                $cnt++;
                                Tests::addTestToCategory($test_id,$c->cat_id);
                             }
                         }  
                     }
         }

        public function actionReport(){
            if(isset($_POST['email'])){
            $model= new Questionreport;
            $model->email=$_POST['email'];
            $model->question_id= $_POST['qid'];
            $model->report= $_POST['report'];
            if($model->save())
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                else
                exit(json_encode(array('result' => 'error', 'msg' => CHtml::errorSummary($model))));
        }else {
            exit(json_encode(array('result' => 'error', 'msg' => 'No input data has been passed.')));
        }
            
        }
        
}
<?php
Yii::import('application.modules.user.models.*');

class TestcategoryController extends YumController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/yum';
        public $category_tree= array();

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete',// we only allow deletion via POST request
                        array('ext.seo.components.SeoFilter + list'),
		);
	}

        
         public function behaviors()
        {
            return array(

                'seo'=>array('class'=>'ext.seo.components.SeoControllerBehavior'),
            );
        }
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','list'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','list'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','ajaxupdate'),
				'expression' => 'Yii::app()->user->isAdmin()'
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
       public function actionAjaxUpdate($id)
        {
           $model=$this->loadModel($id);
           $p = ($model->publish == 1 ? 0 : 1);
           if(!$model->isRoot()){
           $parent=$model->parent()->find();
           if($parent->publish==0)
            $p=0;
           }
           $dbReader = Yii::app()->db->createCommand("
               UPDATE testcategory
                SET publish=$p
                WHERE cat_id=$id
                    ")->query();
           $d=$model->descendants()->findAll();
           if($d!=null){
               foreach($d as $des){
               $dbReader = Yii::app()->db->createCommand("
               UPDATE testcategory
                SET publish=$p
                WHERE cat_id=$des->cat_id
                    ")->query();
               }
           }
                
                
                
               
        }
   
        public function actionList($id){
            $this->layout= '//layouts/column1';
            if($id==null){
            $tree= Testcategory::getTreeRecursive();
             $root= Testcategory::model()->findAll();
            }
            else{
                $dbReader = Yii::app()->db->createCommand("
                SELECT cat.cat_name, cat.level, t.test_name,t.publish, t.id_test
                FROM testcategory cat
                JOIN cat_test ct ON cat.cat_id = ct.cat_id
                JOIN tests t ON ct.test_id= t.id_test
                WHERE cat.cat_id=$id AND  t.publish=1 
               ")->query();
                
            $root=Testcategory::model()->find('cat_id = :catName', array(':catName'=>$id));
            
           
            
            $tree= $dbReader;
            
  
           /* foreach($children as $c){
                $arr[]['name']=$c->cat_name;
                $arr[]['model']=$c;
                $arr[]['tests']=  Testcategory::getAllTest($c->cat_id);
            }*/
            
            }
            $this->render('list',array('models'=>$tree,'children'=>$root));
            
            
        }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Testcategory;

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model,'testcategory-form');

		if (isset($_POST['Testcategory'])) {
			$model->attributes=$_POST['Testcategory'];
                        $model->description=$_POST['description'];
                        $model->meta= $_POST['Testcategory']['meta'];
                        $model->created_by= user()->id;
                        $model->date_created= time();
                        if(isset($_POST['parentid'])&& $_POST['parentid']!=""){
                          
                          
                            $parent=$_POST['parentid'];
                            $arr= explode(',', $parent);
                           
                            
                            foreach($arr as $a=>$value){
                                $catname = str_replace('-', '', $value);
                                $root=Testcategory::model()->find("cat_name=:catName", array(':catName'=>$catname));
                               
                                $model->appendTo($root);
                                 $this->redirect(array('admin'));
                            }
                   
                        }
                        else{
                            if ($model->saveNode()) {
                                
                          
				$this->redirect(array('admin'));
			}
                        }
                      
			
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
        protected  function saveDoCat($categories,$test_id){
            
            
                     $arr= explode(',', $categories);
                     $cats =  Testcategory::model()->findAll();
                     $cnt= 0;
                     foreach($cats as $c){
                         foreach($arr as $a=>$value){
                             if($c->cat_name == $value){
                                $cnt++;
                                Tests::addTestToCategory($test_id,$c->cat_id);
                             }
                         }  
                     }
         }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Testcategory'])) {
			$model->attributes=$_POST['Testcategory'];
                        $name=$_POST['Testcategory']['cat_name'];
                        $meta=$_POST['Testcategory']['meta'];
                        $keyword=$_POST['Testcategory']['keyword'];
                        $description=$_POST['description'];
                        
                        $dbCommand = Yii::app()->db->createCommand("
                            UPDATE testcategory SET cat_name='$name', meta='$meta',
                                keyword='$keyword', description='$description'
                               WHERE cat_id= $id
               ")->execute();

			
				$this->redirect(array('view','id'=>$model->cat_id));
			
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Testcategory');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Testcategory('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Testcategory'])) {
			$model->attributes=$_GET['Testcategory'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Testcategory the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id=null)
	{
                if(!$id)
			$id = Yii::app()->user->id;
		$model=Testcategory::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

}
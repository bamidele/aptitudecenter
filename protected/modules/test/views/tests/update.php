<?php
/* @var $this TestsController */
/* @var $model Tests */
?>

<?php
$this->breadcrumbs=array(
	'Tests'=>array('index'),
	$model->test_name=>array('view','id'=>$model->id_test),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tests', 'url'=>array('index')),
	array('label'=>'Create Tests', 'url'=>array('create')),
	array('label'=>'View Tests', 'url'=>array('view', 'id'=>$model->id_test)),
	array('label'=>'Manage Tests', 'url'=>array('admin')),
);
?>

    <h1>Update Tests <?php echo $model->id_test; ?></h1>

<?php 
        //echo $model->test_name;
$this->renderPartial('_form', array('model'=>$model)); ?>
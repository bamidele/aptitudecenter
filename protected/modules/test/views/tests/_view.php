<?php
/* @var $this TestsController */
/* @var $data Tests */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id_test')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_test),array('view','id'=>$data->id_test)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('test_type')); ?>:</b>
	<?php echo CHtml::encode($data->test_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('test_name')); ?>:</b>
	<?php echo CHtml::encode($data->test_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timestaken')); ?>:</b>
	<?php echo CHtml::encode($data->timestaken); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalscores')); ?>:</b>
	<?php echo CHtml::encode($data->totalscores); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('average_score')); ?>:</b>
	<?php echo CHtml::encode($data->average_score); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('date_added')); ?>:</b>
	<?php echo CHtml::encode($data->date_added); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('test_added_by')); ?>:</b>
	<?php echo CHtml::encode($data->test_added_by); ?>
	<br />

	*/ ?>

</div>
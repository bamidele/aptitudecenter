<?php
/* @var $this TestsController */
/* @var $model Tests */
$this->metaKeywords = $cat->keyword;
if($cat->meta!=null){
  $this->metaDescription = $cat->meta;  
}else{
    $this->metaDescription = $cat->cat_name .' | '.$test->description.''; 
    
}
$this->canonical = $cat->getAbsoluteUrl();
?>
<?php $this->pageTitle = Yii::app()->name .'-'.$cat->cat_name.'-'.$test->test_name; ?>

<?php


$this->breadcrumbs=array(
         
	'Category'=>array("/test/testcategory/list/id/$cat->cat_id"),
	$test->test_name,
);

?>
<div class="span8">
<div class="row-fluid ad">
    
</div>

<div class="row-fluid">

<h4> Test :  <?php echo $test->test_name ?></h4>
<h5>Questions : <?php echo $test->num_of_questions ?> | Duration :  <?php echo $test->duration. ' Minutes' ?> </h5>
<h5>Question Type: <?php echo TestType::returnOptionName($test->test_to_tt->option) ?></h5>
<br>
<p>
    <?php echo TbHtml::i('Description : '.$test->description); ?>
</p>

<div class="addthis_toolbox addthis_default_style">
<a class="addthis_button_facebook_like" fb:like:layout="button_count" fb:like:href="<?php echo $test->getAbsoluteUrl();?>"
></a>
<a class="addthis_button_tweet"></a>
<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
<a class="addthis_button_linkedin_counter"></a>

<!-- AddThis Button BEGIN -->


<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5231aaa22fdab04e"></script>
<!-- AddThis Button END -->
</div>
<h3> Preview: </h3>
<h6> Question : <?php echo $question->questions_numbers ?></h6>
<p> <?php echo $question->question ?></p>
        <?php if($test->test_to_tt->option==1){?>
        <table>
        
            <tr>
                <td><p><b>A:</b></p></td>
                <td><?php echo $question->option_a ?></td>
            </tr>
            <tr>
                <td><p><b>B: </b></p></td>
                <td> <?php echo $question->option_b ?></td>
            </tr>
            <?php if($test->num_of_options >=3){ ?> 
            <tr>
                <td><p><b>C:</b></p></td>
                <td> <?php echo $question->option_c ?></td>
            </tr>
            <?php } ?> 
            <?php if($test->num_of_options >=4){ ?> 
            <tr>
                <td><p><b>D:</b></p></td>
                <td> <?php echo $question->option_d ?></td>
            </tr>
            <?php } ?> 
            <?php if($test->num_of_options >=5){ ?> 
            <tr>
                <td><p><b>E:</b></p></td>
                <td> <?php echo $question->option_e ?></td>
            </tr>
            <?php } ?> 
    </table>
        <?php }?>
<?php 
if(user()->isGuest){
echo TbHtml::formActions(array(
TbHtml::link(TbHtml::Button('<i class="icon-thumbs-up"></i> Login to Practice This Test', 
            array('color' => TbHtml::BUTTON_COLOR_PRIMARY,
                'size'=> TbHtml::BUTTON_SIZE_LARGE,)
                   ), url('/user/auth')),
TbHtml::link(TbHtml::Button('<i class="icon-signal"></i> Login to Take Online Test', 
            array('color' => TbHtml::BUTTON_COLOR_PRIMARY,'id'=>'idSubmit',
                'size'=> TbHtml::BUTTON_SIZE_LARGE,)), url('/user/auth')), 
    
    ));
}
else{
    echo TbHtml::formActions(array(
TbHtml::link(TbHtml::Button('<i class="icon-thumbs-up"></i> Practice This Test', 
            array('color' => TbHtml::BUTTON_COLOR_PRIMARY,
                'size'=> TbHtml::BUTTON_SIZE_LARGE,)
                   ), url('/test/taketest/practicetest',array('tid'=>$test->id_test))),
TbHtml::link(TbHtml::Button('<i class="icon-signal"></i>   Take Online Test', 
            array('color' => TbHtml::BUTTON_COLOR_PRIMARY,'id'=>'idSubmit',
                'size'=> TbHtml::BUTTON_SIZE_LARGE,)), url('/test/taketest/test',array('tid'=>$test->id_test))), 
    
    ));
}
    ?>
<p>
    <h3>The Benefits of using Aptitude Center</h3>
    <ul class="icons-ul">
    <li> <i class="icon-li icon-ok-sign"></i>Just like the real thing</li>
    <li><i class="icon-li icon-ok-sign"></i> Timed test simulations</li>
    <li><i class="icon-li icon-ok-sign"></i>Instant online access 24/7</li>
    <li><i class="icon-li icon-ok-sign"></i>Free access to updates</li>
    <li><i class="icon-li icon-ok-sign"></i>Clear worked solutions</li>
    <li><i class="icon-li icon-ok-sign"></i>Compare your performance</li>
    </ul>
</p>

</div>
<div class="row-fluid">
    <?php //echo buy  bundle widget! ?>
</div>
    </div>
<div class="span4">
  <div class="row-fluid">
     <script type="text/javascript">
      ad_client = "pub-3763";
      ad_slot = 1;
      ad_type = "hybrid-300-2";
      </script>
<script type="text/javascript" src="http://openadnation.com/scripts/adstation.js"></script>
      </div>
    <div class="row-fuild">
        
     <script type="text/javascript">
      ad_client = "pub-3763";
      ad_slot = 1;
      ad_type = "hybrid-300-2";
      </script>
<script type="text/javascript" src="http://openadnation.com/scripts/adstation.js"></script>
    </div>
    
</div>
<?php 


?>
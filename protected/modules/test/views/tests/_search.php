<?php
/* @var $this TestsController */
/* @var $model Tests */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id_test',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'test_type',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'test_name',array('span'=>5,'maxlength'=>100)); ?>

                    <?php echo $form->textFieldControlGroup($model,'description',array('span'=>5,'maxlength'=>1000)); ?>

                    <?php echo $form->textFieldControlGroup($model,'timestaken',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'totalscores',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'average_score',array('span'=>5,'maxlength'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'date_added',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'test_added_by',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
/* @var $this TestsController */
/* @var $model Tests */
?>

<?php
$this->breadcrumbs=array(
	'Tests'=>array('admin'),
	$model->test_name,
);

$this->menu=array(
	array('label'=>'List Tests', 'url'=>array('index')),
	array('label'=>'Create Tests', 'url'=>array('create')),
	array('label'=>'Update Tests', 'url'=>array('update', 'id'=>$model->id_test)),
	array('label'=>'Delete Tests', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_test),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tests', 'url'=>array('admin')),
);
?>

<h3>View Test:  <?php echo $model->test_name; ?></h3>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_test',
                array(
					'type' => 'raw',
					'name' => Yum::t('test_name'),
					'value' => $model->test_name,
                    ),
		array(
					'type' => 'raw',
					'name' => Yum::t('test_type'),
					'value' => $model->test_to_tt->name,
	
			),
		array(
					'type' => 'raw',
					'label' => Yum::t('categories'),
					'value' => Tests::getTestToCat($model->id_test), 
	
			),
                 'year',
		'description',
                array(
					'type' => 'raw',
					'name' => Yum::t('duration'),
					'value' => $model->duration .' Min',
	
			),
                 array(
					'type' => 'raw',
					'name' => Yum::t('num_of_options'),
					'value' =>($model->test_to_tt->option==1)? $model->num_of_options : ' Non', 
                    ),
		'timestaken',
		'totalscores',
		'average_score',
		 array(
				'name'=>'date_added',
				'filter' => false,
				'value'=>date(UserModule::$dateFormat,$model->date_added),
		),
                 array(
				'name'=>'date_modified',
				'filter' => false,
				'value'=>date(UserModule::$dateFormat,$model->date_modified),
		),
              
                array(
					'type' => 'raw',
					'name' => Yum::t('test_added_by'),
					'value' => $model->test_to_user->username,
		),
               array(
					'type' => 'raw',
					//'name' => 'Edit Questions',
					'value' => TbHtml::linkButton('Edit Questions', array(
							'url'=>$this->createUrl("questions/massupdate/id/$model->id_test"),
							
						)),
                   ),
              )
               
)); ?>
<?php
/* @var $this TestsController */
/* @var $model Tests */


$this->breadcrumbs=array(
	'Tests'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Tests', 'url'=>array('index')),
	array('label'=>'Create Tests', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tests-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
/*Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#tests-grid a.ajaxupdate').live('click', function() {
        $('#tests-grid').yiiGridView('update', {
                type: 'POST',
                url: $(this).attr('href'),
                success: function() {
                        $('#tests-grid').yiiGridView.update()},
                }
        );
        return false;
});
");
 * 
 */
Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#tests-grid a.ajaxupdate').live('click', function() {
        $.fn.yiiGridView.update('tests-grid', {
                type: 'POST',
                url: $(this).attr('href'),
                success: function() {
                        $.fn.yiiGridView.update('tests-grid');
                }
        });
        return false;
});
");
Yii::app()->clientScript->registerScript('ajaxfree', "
$('#tests-grid a.ajaxfree').live('click', function() {
        $.fn.yiiGridView.update('tests-grid', {
                type: 'POST',
                url: $(this).attr('href'),
                success: function() {
                        $.fn.yiiGridView.update('tests-grid');
                }
        });
        return false;
});
");
?>

<h2>Manage Tests</h2>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'tests-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                'id_test',
                'test_name',
		 array(
					'type' => 'raw',
					'name' => Yum::t('test_type'),
					'value' => '$data->test_to_tt->name' 
	
			),
                  array(
					'type' => 'raw',
					'name' => Yum::t('duration'),
					'value' => '$data->duration .\' Min\'' 
	
			),
                        'num_of_questions',
                /*  array(
					'type' => 'raw',
					'name' => Yum::t('num_of_options'),
					'value' =>'($data->test_to_tt->option==1)? $data->num_of_options : \' Non\'', 
	
			),
                 * 
                 */
               
                //'timestaken',
		//'average_score',
		
                 array(
					'type' => 'raw',
					'name' => Yum::t('test_added_by'),
					'value' => '$data->test_to_user->username'
					
					),
                 array(
				'name'=>'date_added',
				'filter' => false,
				'value'=>'date(UserModule::$dateFormat,$data->date_added)',
			),
            array(
        'name'=>'isfree',
        'type'=>'raw',
        'value'=>'CHtml::link(($data->isfree==1)? \'yes\':\'no\',array(\'ajaxfree\',
            \'id\'=>$data->id_test), array("class"=>"ajaxfree"))'
),
            array(
        'name'=>'publish',
        'type'=>'raw',
        'value'=>'CHtml::link(($data->publish==1)? \'yes\':\'no\',array(\'ajaxupdate\',
            \'id\'=>$data->id_test), array("class"=>"ajaxupdate"))'
),
                  array(
					'type' => 'raw',
					//'name' => 'Edit Questions',
					'value' => 'CHtml::link(\'Edit Questions\', array(
							\'questions/massupdate/\',
							"id" =>$data->id_test)
						)'
                        ),
	
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
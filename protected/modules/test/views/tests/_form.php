<?php
/* @var $this TestsController */
/* @var $model Tests */
/* @var $form TbActiveForm */

?>

<div class="form">
 

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'tests-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
    <?php echo $form->errorSummary($model); ?>

            <?php
                echo $form->dropDownListControlGroup($model, 'test_type',TestType::getTypeList(),array('span'=>5));            
//echo $form->textFieldControlGroup($model,'test_type',array('span'=>5)); ?>
            
            <?php echo $form->textFieldControlGroup($model,'test_name',array('span'=>5,'maxlength'=>100)); ?>
            <div class="row-fluid">
            <div class="span12">
                
                <label>Categories</label>
                <?php
                
                if($model->isNewRecord ){
                  $this->widget('yiiwheels.widgets.select2.WhSelect2', array(
                'asDropDownList' => false,
                'name' => 'categories',
                'pluginOptions' => array(
                    'tags' =>Testcategory::getCat(),
                    'placeholder' => 'Choose categories',
                    'width' => '40%',
                    'tokenSeparators' => array(',', ' ')
                )));
                }
                else{
              
                    $this->widget('yiiwheels.widgets.select2.WhSelect2', array(
                'asDropDownList' => false,
                'name' => 'categories',
                 'value'=>  Tests::getTestToCat($model->id_test),
                'pluginOptions' => array(
                    'tags' =>Testcategory::getCat(),
                    'placeholder' => 'Choose categories',
                    'width' => '40%',
                    'tokenSeparators' => array(',', ' ')
                )));
                }
                 
            ?>
           </div></div>
               
             <?php //echo TbHtml::FieldControlGroup($model,'timestaken',array('span'=>5)); ?>
          <?php $attributeA = 'description';
                    $valueA = (isset($model->description))? $model->description:'';
                        ?>
                        <div class="row-fluid">
                        <div class="span2">
                            <label> Description:</label>
                        </div>
                        </div>
                        <div class="row-fluid">
                        
                        <div class="span9" style="min-height: 100px;">
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>'description',
                        'value'=>$valueA,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attributeA
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attributeA
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attributeA
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));
                    
                ?> 
    </div></div>
            <?php echo $form->textFieldControlGroup($model,'duration',array('span'=>5,'maxlength'=>100)); ?>
            <?php echo $form->textFieldControlGroup($model,'num_of_questions',array('span'=>5,'maxlength'=>100)); ?>
            <?php echo $form->textFieldControlGroup($model,'num_of_options',array('span'=>5,'maxlength'=>100)); ?>
            <?php echo $form->textFieldControlGroup($model,'year',array('span'=>5)); ?>

            <?php //echo $form->textFieldControlGroup($model,'totalscores',array('span'=>5)); ?>

            <?php //echo $form->textFieldControlGroup($model,'average_score',array('span'=>5,'maxlength'=>5)); ?>

            <?php //echo $form->textFieldControlGroup($model,'date_added',array('span'=>5)); ?>

            <?php //echo $form->textFieldControlGroup($model,'test_added_by',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
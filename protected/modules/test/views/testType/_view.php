<?php
/* @var $this TestTypeController */
/* @var $data TestType */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tt')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tt),array('view','id'=>$data->id_tt)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tt_createdby')); ?>:</b>
	<?php echo CHtml::encode($data->tt_createdby); ?>
	<br />


</div>
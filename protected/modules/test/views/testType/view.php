<?php
/* @var $this TestTypeController */
/* @var $model TestType */
$this->metaKeywords = 'testtype, test, subjective, objective, theory,admin,';
$this->metaDescription = 'this page is be  for add test type';
?>



<?php
$this->breadcrumbs=array(
	'Test Types'=>array('admin'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TestType', 'url'=>array('index')),
	array('label'=>'Create TestType', 'url'=>array('create')),
	array('label'=>'Update TestType', 'url'=>array('update', 'id'=>$model->id_tt)),
	array('label'=>'Delete TestType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tt),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TestType', 'url'=>array('admin')),
);
?>

<h3>View TestType #<?php echo $model->name; ?></h3>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_tt',
		'name',
		'description',
                'options', 
                array(
					'type' => 'raw',
					'name' => Yum::t('isMultiChoice'),
					'value' =>($model->isMultiChoice)? 'True ': ' False', 
                    ),
                     array(
					'type' => 'raw',
					'name' => Yum::t('tt_createdby'),
					'value' => $model->tt_to_user->username
					),
	
	),
)); ?>
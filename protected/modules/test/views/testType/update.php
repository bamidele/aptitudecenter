<?php
/* @var $this TestTypeController */
/* @var $model TestType */
$this->metaKeywords = 'testtype, test, subjective, objective, theory,admin,';
$this->metaDescription = 'this page is be  for updating test type';
//$this->addMetaProperty('fb:app_id',Yii::app()->params['fbAppId']);
$this->canonical = $model->getAbsoluteUrl(); // canonical URLs should always be absolute
?>

<?php
$this->breadcrumbs=array(
	'Test Types'=>array('admin'),
	$model->name=>array('view','id'=>$model->id_tt),
	'Update',
);

$this->menu=array(
	array('label'=>'List TestType', 'url'=>array('index')),
	array('label'=>'Create TestType', 'url'=>array('create')),
	array('label'=>'View TestType', 'url'=>array('view', 'id'=>$model->id_tt)),
	array('label'=>'Manage TestType', 'url'=>array('admin')),
);
?>

    <h2>Update TestType <?php echo $model->id_tt; ?></h2>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
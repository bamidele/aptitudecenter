<?php
/* @var $this TestTypeController */
/* @var $model TestType */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'test-type-form',
        'layout'=>  TbHtml::FORM_LAYOUT_HORIZONTAL,
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'name',array('span'=>5,'maxlength'=>45)); ?>

             <?php //echo $form->radioButtonListControlGroup($model, 'radioButtons', array('Objective','Theory','Subjective'));?>
             <?php echo $form->inlineRadioButtonListControlGroup($model,'option', TestType::getOption());?>
             <?php echo $form->checkBoxControlGroup($model,'isMultiChoice');?>
             <?php //echo $form->textFieldControlGroup($model,'num_of_questions',array('span'=>5,'maxlength'=>45)); ?>
             <?php echo $form->textAreaControlGroup($model, 'description',
                    array('span' => 5, 'rows' => 3)); ?>

            <?php //echo $form->textFieldControlGroup($model,'tt_createdby',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
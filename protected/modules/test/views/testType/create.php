<?php
/* @var $this TestTypeController */
/* @var $model TestType */
?>

<?php
$this->breadcrumbs=array(
	'Test Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TestType', 'url'=>array('index')),
	array('label'=>'Manage TestType', 'url'=>array('admin')),
);
?>

<h1>Create TestType</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
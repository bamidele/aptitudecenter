<?php
/* @var $this TestTypeController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Test Types',
);

$this->menu=array(
	array('label'=>'Create TestType','url'=>array('create')),
	array('label'=>'Manage TestType','url'=>array('admin')),
);
?>

<h1>Test Types</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
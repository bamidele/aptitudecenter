<?php
/* @var $this QuestionsController */
/* @var $data Questions */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id_question')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_question),array('view','id'=>$data->id_question)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('test_id')); ?>:</b>
	<?php echo CHtml::encode($data->test_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('questions_numbers')); ?>:</b>
	<?php echo CHtml::encode($data->questions_numbers); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('question')); ?>:</b>
	<?php echo CHtml::encode($data->question); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('option_a')); ?>:</b>
	<?php echo CHtml::encode($data->option_a); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('option_b')); ?>:</b>
	<?php echo CHtml::encode($data->option_b); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('option_c')); ?>:</b>
	<?php echo CHtml::encode($data->option_c); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('option_d')); ?>:</b>
	<?php echo CHtml::encode($data->option_d); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('answer')); ?>:</b>
	<?php echo CHtml::encode($data->answer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('option_e')); ?>:</b>
	<?php echo CHtml::encode($data->option_e); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('working')); ?>:</b>
	<?php echo CHtml::encode($data->working); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
	<?php echo CHtml::encode($data->date_created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	*/ ?>

</div>
<?php
/* @var $this QuestionsController */
/* @var $model Questions */
?>

<?php
$this->breadcrumbs=array(
	'Questions'=>array('index'),
	$model->id_question,
);

$this->menu=array(
	array('label'=>'List Questions', 'url'=>array('index')),
	array('label'=>'Create Questions', 'url'=>array('create')),
	array('label'=>'Update Questions', 'url'=>array('update', 'id'=>$model->id_question)),
	array('label'=>'Delete Questions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_question),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Questions', 'url'=>array('admin')),
);
?>

<h1>View Questions #<?php echo $model->id_question; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_question',
		'test_id',
		'questions_numbers',
		'question',
		'option_a',
		'option_b',
		'option_c',
		'option_d',
		'answer',
		'option_e',
		'working',
		'date_created',
		'user_id',
	),
)); ?>
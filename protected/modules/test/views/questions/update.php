<?php
/* @var $this QuestionsController */
/* @var $model Questions */
?>

<?php
$this->breadcrumbs=array(
	'Questions'=>array('index'),
	$model->id_question=>array('view','id'=>$model->id_question),
	'Update',
);

$this->menu=array(
	array('label'=>'List Questions', 'url'=>array('index')),
	array('label'=>'Create Questions', 'url'=>array('create')),
	array('label'=>'View Questions', 'url'=>array('view', 'id'=>$model->id_question)),
	array('label'=>'Manage Questions', 'url'=>array('admin')),
);
?>

    <h1>Update Questions <?php echo $model->id_question; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
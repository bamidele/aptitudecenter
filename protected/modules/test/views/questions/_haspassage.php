<?php echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_HORIZONTAL, $this->createUrl('question/pcreate'),
        'post',array('id'=>'passage_1')) ?>
 <?php echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_HORIZONTAL); ?>
    <?php 
    $passage='';
    if($model->q_to_p!=null){
        $passage= $model->q_to_p->passage;
    } ?>

<?php 
                    $attributeP = 'passage';
                    
                        ?>
                        <div class="row-fluid">
                        <div class="span2">
                            <label> Passage  :</label>
                        </div>
                        </div>
                        <div class="row-fluid">
                        
                        <div class="span12" style="min-height: 100px;">
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>$attributeP,
                        'value'=>$passage,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attributeP
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attributeP
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attributeP
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));
                ?> 
    </div></div>
    <br>
    <?php if($model->q_to_p==null){
            $this->widget('RangeInputField', array(
        
                    'nameFrom' =>'from',
                    'nameTo' =>'to',
                    'labelFrom'=>'From',
                    'labelTo'=>'To',
                    'valueFrom'=>$model->questions_numbers,
    )); }
    ?>
    <?php   echo CHtml::hiddenField('qid',$model->id_question);
            echo CHtml::hiddenField('from',$model->questions_numbers);
        $pasNum=0;
        if($model->q_to_p !=null){
            $pasNum=$model->q_to_p->idpassages;
        }
    ?>
    <?php 
    if($pasNum==0){
    echo TbHtml::formActions(array(
    TbHtml::submitButton('Create Passage', array('color' => TbHtml::BUTTON_COLOR_PRIMARY,'id'=>'idSubmit')),
    
        
    ));
    }
    else
    {
        // buttons  if its  for editing
        echo TbHtml::formActions(array(
    TbHtml::submitButton('Save changes', array('color' => TbHtml::BUTTON_COLOR_PRIMARY,'id'=>'idSubmit2')),
    TbHtml::Button('Delete',array('id'=>'delete')),
        
    ));
    }
    ?>
   
    <?php echo TbHtml::endForm(); ?>
            
<script>
    $(document).ready(function(){
        $('#idSubmit').each(function(){
                        $(this).click(function(ev){
                                ev.preventDefault();
                               
                                $.ajax({
                                        "type":"POST",
                                        "url":"<?php echo $this->createUrl('questions/pcreate'); ?>",
                                        "data":$('#passage_1').serialize(),
                                        "dataType": "json",
                                        "success":function(data){
                                        if(data.passage!==null){
                                            
                                           var ms=data.passage;
                                            var p= '<?php echo TbHtml::lead('Passage for this Question'); ?>';
                                            p+='<div class="well well-small">'+ms+'</div>';
                                             
                                            $("#passageheader").html(p).show();
                                            
                                            location.reload();
                                            
                                            $("#inner_error").hide();
                                           
                                        }else{
                                           var s= '<ul>'+data.error+'</ul>';
                                           $("#inner_error").append(s).show();
                                            
                                        }
                                        },
                                        
                                        });
                               
                                });
                });
             $("#cancel").click(function(){
                 $("#passageheader").toggle();
                 $("#hasform").toggle();
              });
              
              $('#idSubmit2').each(function(){
                        $(this).click(function(ev){
                                ev.preventDefault();
                               
                                $.ajax({
                                        "type":"POST",
                                        "url":"<?php echo $this->createUrl('questions/editpass'); ?>",
                                        "data":$('#passage_1').serialize(),
                                        "dataType": "json",
                                        "success":function(data){
                                        if(data.error){
                                            var s= '<ul>'+data.error+'</ul>';
                                           $("#inner_error").append(s).show();
                                           
                                        }else{
                                           
                                            location.reload();
                                        }
                                        },
                                        
                                        });
                               
                                });
                });
                $("#delete").click(function(){
                    $.ajax({
                        'type':'GET',
                         'url':"<?php echo $this->createUrl('questions/deletepass'); ?>",
                         'data':{'ajax':true, 'id': "<?php echo $model->id_question ; ?>"},
                         'success':
                                function(data){
                                location.reload();
                            }
                          ,
                    });
                });
                           
        });
        
</script>
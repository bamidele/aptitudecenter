<?php
/* @var $this QuestionsController */
/* @var $model Questions */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id_question',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'test_id',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'questions_numbers',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'question',array('span'=>5,'maxlength'=>2000)); ?>

                    <?php echo $form->textFieldControlGroup($model,'option_a',array('span'=>5,'maxlength'=>100)); ?>

                    <?php echo $form->textFieldControlGroup($model,'option_b',array('span'=>5,'maxlength'=>100)); ?>

                    <?php echo $form->textFieldControlGroup($model,'option_c',array('span'=>5,'maxlength'=>100)); ?>

                    <?php echo $form->textFieldControlGroup($model,'option_d',array('span'=>5,'maxlength'=>100)); ?>

                    <?php echo $form->textFieldControlGroup($model,'answer',array('span'=>5,'maxlength'=>1000)); ?>

                    <?php echo $form->textFieldControlGroup($model,'option_e',array('span'=>5,'maxlength'=>100)); ?>

                    <?php echo $form->textFieldControlGroup($model,'working',array('span'=>5,'maxlength'=>1000)); ?>

                    <?php echo $form->textFieldControlGroup($model,'date_created',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'user_id',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div id="editor" class="">
    
<?php foreach($models as $model): ?>
  <?php 
        $this->breadcrumbs=array(
	'Test'=>array('tests/view','id'=>$model->question_to_test->id_test),
	'Question'=>array('massupdate','id'=>$model->question_to_test->id_test,'page'=>$model->questions_numbers),
	'Edit Question',
);
?>
    
    <?php echo TbHtml::lead('Edit Questions for Test : '.$model->question_to_test->test_name.' here'); ?>
    <?php $this->widget('bootstrap.widgets.TbPager', array(
    'pages' => $pages,
     'maxButtonCount'=>15,
     'id'=>'link_pager'
)) ?>
    <div id="test" sytle="height:600px;overflow:auto;">
        
    </div>
     <?php 
            
            $this->widget('bootstrap.widgets.TbModal', array(
             
              
            'id' => 'myModal',
            'header' => 'Add Passage to Questions',
            'content' => $this->renderPartial('_haspassage',array('model'=>$model),true),
            'footer' => array(
            
            TbHtml::button('Close', array('data-dismiss' => 'modal')),
            ),
    )); 
   
    
     ?>
    
    <div id="inner_error">
        <?php echo TbHtml::lead('Passage for this Question'); ?>
    </div>
    <div class="row-fluid">
    <div id="first" class="span9">
        <div id="passageheader">
            <?php if($model->q_to_p!=null): ?>
            <?php echo TbHtml::lead('Passage for this Question'); ?>
            <?php echo TbHtml::well($model->q_to_p->passage); ?>
            <?php echo TbHtml::button('Edit Passage?', array(
                    'style' => TbHtml::BUTTON_COLOR_PRIMARY,
                    'size' => TbHtml::BUTTON_SIZE_LARGE,
                    'data-toggle' => 'modal',
                    'data-target' => '#myModal',
                    )); 
            ?>
            <?php  endif;  ?>
        
        </div>
        <div id="hasform">
          <?php if($model->q_to_p !=null):
              
             echo TbHtml::well($model->q_to_p->passage);
         echo TbHtml::button('Edit Passage?', array(
                    'style' => TbHtml::BUTTON_COLOR_PRIMARY,
                    'size' => TbHtml::BUTTON_SIZE_LARGE,
                    'data-toggle' => 'modal',
                    'data-target' => '#myModal',
                    )); 
           
             else:
?>
        <?php echo TbHtml::button('Has a Passages?', array(
                    'style' => TbHtml::BUTTON_COLOR_PRIMARY,
                    'size' => TbHtml::BUTTON_SIZE_LARGE,
                    'data-toggle' => 'modal',
                    'data-target' => '#myModal',
                    )); 
                endif;
                ?>
        </div>
    </div>
   </div>
   
    <div id="form">
  <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_VERTICAL,
      'id'=>'questions-form',
        
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
      'action'=>$this->createUrl("questions/update/id/$model->id_question"),
      'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'afterValidate'=>'js:function(form,data,hasError){
                        if(!hasError){
                                $.ajax({
                                        "type":"POST",
                                        "url":"'.CHtml::normalizeUrl(array("questions/update/id/$model->id_question")).'",
                                        "data":form.serialize(),
                                       
                                        "success":function(data){
                                        $("#test").html(data);
                                        window.scrollTo(0,0);
                                     
                                        $("#test").delay(5000).fadeOut("slow");
                                        
                                        },
                                        
                                        });
                                }
                        }'
        ),
)); ?>
     
    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
 
    <?php echo $form->errorSummary($model); ?>

            <?php //echo $form->textFieldControlGroup($model,'test_name',array('span'=>5)); ?>
                
            <?php //echo TbHtml::label($model->question_to_test->test_name, 'text'); ?>
            <?php echo $form->textFieldControlGroup($model,'questions_numbers',array('span'=>5,'disabled'=>true)); ?>
            
            <?php 
                    $attribute = 'question';
                    $value = (isset($model->question))? $model->question:'';
                        ?>
                        <div class="row-fluid">
                        <div class="span2">
                            <label> Question :</label>
                        </div>
                        </div>
                        <div class="row-fluid">
                        
                        <div class="span9" style="min-height: 100px;">
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>'question',
                        'value'=>$value,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attribute
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attribute
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attribute
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));
                ?> 
    </div></div>
                
                
                
             <?php //echo $form->textFieldControlGroup($model,'question',array('span'=>5,'maxlength'=>2000)); ?>
             
             <?php echo CHtml::hiddenField('option',$model->question_to_test->test_to_tt->option); ?>
             <?php if($model->question_to_test->test_to_tt->option ==1): ?>
    
             <?php 
                    if($model->question_to_test->num_of_options >=1){
                    $attributeA = 'option_a';
                    $valueA = (isset($model->option_a))? $model->option_a:'';
                        ?>
                        <div class="row-fluid">
                        <div class="span2">
                            <label> Option A :</label>
                        </div>
                        </div>
                        <div class="row-fluid">
                        
                        <div class="span9" style="min-height: 100px;">
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>'option_a',
                        'value'=>$valueA,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attributeA
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attributeA
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attributeA
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));
                    }
                ?> 
    </div></div>
            <?php //echo $form->textAreaControlGroup($model, 'option_a', array('span' => 5, 'rows' => 3)); ?>
            
    
            <?php 
                    if($model->question_to_test->num_of_options >=2){
                    $attributeB ='option_b';
                    $valueB = (isset($model->option_b))? $model->option_b:'';
                        ?>
                        <div class="row-fluid">
                        <div class="span2">
                            <label>Option B :</label>
                        </div>
                        </div>
                        <div class="row-fluid">
                        
                        <div class="span9" style="min-height: 100px;">
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>$attributeB,
                        'value'=>$valueB,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attributeB
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attributeB
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attributeB
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));
                    }
                ?> 
    </div></div>
            <?php //echo $form->textAreaControlGroup($model, 'option_b', array('span' => 5, 'rows' => 3)); ?>
            
            <?php 
                    if($model->question_to_test->num_of_options >=3){
                    $attributeC = 'option_c';
                    $valueC = (isset($model->option_c))? $model->option_c:'';
                        ?>
                        <div class="row-fluid">
                        <div class="span2">
                            <label> Option C :</label>
                        </div>
                        </div>
                        <div class="row-fluid">
                        
                        <div class="span9" style="min-height: 100px;">
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>$attributeC,
                        'value'=>$valueC,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attributeC
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attributeC
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attributeC
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));
                        ?>
                   
                        </div></div>
    <?php
                    }
                ?> 
  
            <?php // echo $form->textAreaControlGroup($model, 'option_c', array('span' => 5, 'rows' => 3)); ?>
            <?php //echo $form->textAreaControlGroup($model, 'option_d', array('span' => 5, 'rows' => 3)); ?>
            
            <?php 
                    if($model->question_to_test->num_of_options >=4){
                    $attributeD = 'option_d';
                    $valueD = (isset($model->option_d))? $model->option_d:'';
                        ?>
                        <div class="row-fluid">
                        <div class="span2">
                            <label> Option D:</label>
                        </div>
                        </div>
                        <div class="row-fluid">
                        
                        <div class="span9" style="min-height: 100px;">
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>$attributeD,
                        'value'=>$valueD,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attributeD
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attributeD
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attributeD
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));?>
                   
                        </div></div>
    <?php
                    }
                ?> 
  
                
            <?php //echo $form->textAreaControlGroup($model, 'option_e', array('span' => 5, 'rows' => 3)); ?>
            
            <?php 
                    if($model->question_to_test->num_of_options >=5){
                    $attributeE = 'option_e';
                    $valueE = (isset($model->option_e))? $model->option_e:'';
                        ?>
                        <div class="row-fluid">
                        <div class="span2">
                            <label> Option E :</label>
                        </div>
                        </div>
                        <div class="row-fluid">
                        
                        <div class="span9" style="min-height: 100px;">
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>$attributeE,
                        'value'=>$valueE,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attributeE
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attributeE
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attributeE
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));
                        ?>
                   
                        </div></div>
    <?php
                    }
                ?> 
    
                <?php echo CHtml::hiddenField('isMultiChoice',$model->question_to_test->test_to_tt->isMultiChoice); ?>
            <?php if($model->question_to_test->test_to_tt->isMultiChoice): ?>
            <div class="row-fluid">
            <div class="span12">
            <div class="span1"></div>
                <div class="span1" style="padding-left: 20px"><label>Answers: </label></div>
                <div class="span5" style="padding-left: 30px">
            <?php
                    $this->widget('yiiwheels.widgets.select2.WhSelect2', array(
                    'asDropDownList' => false,
                    'name' => 'answer',
                    'pluginOptions' => array(
                        'tags' =>  Questions::getQuestionOption($model->test_id),
                        'placeholder' => 'Choose options',
                        'width' => '90%',
                        'tokenSeparators' => array(',', ' ')
                    )));
            ?>
                    </div>
                <div class="span5"></div>
            </div></div>
            <br/>
            <?php else: ?>
                <?php 
               $ans= intval($model->answer);
                echo $form->dropDownListControlGroup($model,'answer',Tests::getAnswer($model->test_id),
                        array('empty' => '',
'options'=>array($model->answer=>array('selected'=>'selected')))

                        ); ?>
            <?php endif;?>
            <?php else: ?>
            <?php //echo $form->textAreaControlGroup($model,'answer',array('span'=>5,'rows'=>3)); ?>
            
            <?php 
                    $attributeAns = 'answer';
                    $valueAns = (isset($model->answer))? $model->answer:'';
                        ?>
                        <div class="row-fluid">
                        <div class="span2">
                            <label> Answer :</label>
                        </div>
                        </div>
                        <div class="row-fluid">
                        
                        <div class="span9" style="min-height: 100px;">
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>$attributeAns,
                        'value'=>$valueAns,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attributeAns
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attributeAns
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attributeAns
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));
                ?> 
    </div></div>

             <?php endif; ?>
            

            <?php //echo $form->textAreaControlGroup($model,'working',array('span'=>5,'rows'=>3)); ?>

            <?php 
                    $attributeW = 'working';
                    $valueW = (isset($model->working))? $model->working:'';
                        ?>
                        <div class="row-fluid">
                        <div class="span2">
                            <label> Working :</label>
                        </div>
                        </div>
                        <div class="row-fluid">
                        
                        <div class="span9" style="min-height: 100px;">
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>$attributeW,
                        'value'=>$valueW,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attributeW
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attributeW
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attributeW
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));
                ?> 
    </div></div>
            <?php //echo $form->textFieldControlGroup($model,'date_created',array('span'=>5)); ?>

            <?php //echo $form->textFieldControlGroup($model,'user_id',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    //'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>
    <?php $this->endWidget(); ?>
    </div>
    <?php endforeach; ?> 
  <?php $this->widget('bootstrap.widgets.TbPager', array(
    'pages' => $pages,
     'maxButtonCount'=>15,
     'id'=>'link_pager'
)) ?>
</div>


<script>
    $(document).ready(function(){
        $('#inner_error').hide();
        $('#passageheader').hide();
        $('#hasform').show();
        
    });
/*
 * 
 *$(document).ready(function(){
        $('#link_pager a').each(function(){
                        $(this).click(function(ev){
                                ev.preventDefault();
                               
                                $.ajax({
                                        "type":"POST",
                                        "url":this.href,
                                        "data":{ajax:true},
                                       
                                        "success":function(data){
                                        $("#test").html(data);
                                        $("#test").delay(5000).fadeOut("slow");
                                        },
                                        
                                        });
                                $.get(this.href,{ajax:true},function(html){
                                                $('#editor').html(html);
                                        });
                                });
                });
        });
  */
</script>

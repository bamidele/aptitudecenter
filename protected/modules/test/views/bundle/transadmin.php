<?php
/* @var $this BundleController */
/* @var $model Bundle */


$this->breadcrumbs=array(
	'Bundles'=>array('index'),
	'Manage Transaction',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#transactionbundle-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#transactionbundle-grid a.ajaxupdate').live('click', function() {
        $.fn.yiiGridView.update('transactionbundle-grid', {
                type: 'POST',
                url: $(this).attr('href'),
                success: function() {
                        $.fn.yiiGridView.update('transactionbundle-grid');
                }
        });
        return false;
});
");
?>

<h1>Manage  Transaction Bundle</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'transactionbundle-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'trans_id',
		 array(
					'type' => 'raw',
					'name' => Yum::t('user_id'),
					'value' => '$data->userid' 
	
			),
		 array(
					'type' => 'raw',
					'name' => Yum::t('bundle_id'),
					'value' => '$data->bundle->name' 
	
			),
		array(
				'name'=>'orderdate',
				'filter' => false,
				'value'=>'date(UserModule::$dateFormat,$data->orderdate)',
			),
                  array(
					'type' => 'raw',
					'name' => Yum::t('payment_id'),
					'value' => '$data->payment' 
	
		),
                
            
                array(
					'type' => 'raw',
					'name' => Yum::t('status'),
					'value' => '$data->statusStr' 
	
			),
                 array(
        
        'type'=>'raw',
        'value'=>'CHtml::link(($data->status==0)? TbHtml::button(\'Approve\'):\'\',array(\'confirmtrans\',
            \'trans_id\'=>$data->id), array("class"=>"ajaxupdate"))'
),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
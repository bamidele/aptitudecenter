
<h3>List of Your Subscribed Test Bundles</h3>

<div class="row-fluid">
    
    <?php
    if(isset($model)){
        $cnt=0;
        echo '<div class="row-fluid">';
        foreach ($model as $m){
            if(($cnt%3)==0){
                echo '</div><div class="row-fluid">';
            }
            ?>
    
    <div class="span4">
        <?php 
     echo '<ul class="icons-ul">';
    echo '<li><i class="icon-li icon-unlock"></i>';
    echo CHtml::link($m['name'].'(click to view bundle details)', array('/test/bundle/details','bid'=>$m['idbundle']
            
            ));
    echo '</li>';
    ?>
        </ul>

     </div>
    
    <?php
    $cnt++;
    }
    echo '</div>';
    
  } ?>
    
    </div>

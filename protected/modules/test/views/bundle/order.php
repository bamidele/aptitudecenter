<?php $this->breadcrumbs = array(Yum::t('Exam Bundle'));?>

<?php $this->title = Yum::t('Exam Bundles'); ?>
         <div class ="row-fluid">
            <?php echo TbHtml::lead('Buy Aptitude center Exams Bundles '); ?>
            </div>
	<div class="row-fluid">
           
            <div class="span5 well">
               <div class="row-fluid ">
                   <h4>The Benefits of using Aptitude Center</h4>
                    <p> 
                    <ul class="icons-ul">
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Just like the real thing</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i> Timed test simulations</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Instant online access 24/7</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Free access to updates</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Clear worked solutions</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Compare your performance</li>
                    </ul>
                    </p>
                 </div>
            </div>
             <div class="span7 shadowbox" style="padding:10px;" >
                 <div class="row-fluid">
                 <?php echo TbHtml::lead('Order A Bundle'); ?>
                 <?php echo TbHtml::em('*Please choose one of this options', array('color' => TbHtml::TEXT_COLOR_INFO)); ?>
                 </div>
                <div class="row-fluid">
		<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'membership-has-company-form',
			'enableAjaxValidation'=>true,
			)); 
			echo $form->errorSummary($model);
		?>
           
                    
			<?php echo TbHtml::dropDownListControlGroup('bundle_id', 'bundle_id', 
			CHtml::listData($bundles, 'idbundle', 'nameprice')
			);
			?>
                   
                        
                         <?php  echo TbHtml::submitButton(Yum::t('Continue'),
                             array('size'=>TbHtml::BUTTON_SIZE_DEFAULT, 'color'=>  TbHtml::BUTTON_COLOR_DANGER));?>
                    <?php $this->endWidget(); ?>
		</div>        
           </div>
	</div> <!-- form -->
        </div>
	
		<?php
    Yii::app()->clientScript->registerScript('toggle', "
		$('#detail-information').hide();	  
		$('#more-information').click(function() {
	   	$('#detail-information').fadeToggle('slow');
	   });
    ");
    ?>
	

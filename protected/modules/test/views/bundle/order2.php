<?php $this->breadcrumbs = array(Yum::t('Bundle Payment'));?>

<?php $this->title = Yum::t('Payment Options'); ?>
         <div class ="row-fluid">
            <?php echo TbHtml::lead('Select your payment option'); ?>
            </div>
	<div class="row-fluid">
           
            <div class="span5 well">
               <div class="row-fluid ">
                   <h4>The Benefits of using Aptitude Center</h4>
                    <p> 
                    <ul class="icons-ul">
                    <li> <i class="icon-li icon-ok-sign icon-2x"></i>Just like the real thing</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i> Timed test simulations</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Instant online access 24/7</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Free access to updates</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Clear worked solutions</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Compare your performance</li>
                    </ul>
                    </p>
                 </div>
            </div>
             <div class="span7 shadowbox" style="padding:10px;" >
                 <div class="row-fluid">
                 <?php echo TbHtml::lead('Choose a payment option'); ?>
              
                 </div>
                 <div class="row-fluid"><table border="0" cellpadding="">
                        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                        'id'=>'bundle-process-form',
			'enableAjaxValidation'=>true,
			)); 
			echo $form->errorSummary($model);
		?>
                         <table>
                         
                             <tr>
                                 <td> <b>Item Description: </b></td>
                                 <td> <?php echo $bundle->namewithdesc; ?></td>
                             </tr>
                             <tr>
                                 <td><b>Price :</b></td>
                                 <td><?php echo $bundle ->formatedprice; ?> </td>
                             </tr>
            
                     </table>
                        <br>
                        <br>
                        <p><?php echo TbHtml::lead('Choose Payment Method')?></p>
                        <table>
                                <?php echo CHtml::RadioButtonList('payment_option', 'payment_option', 
			CHtml::listData(YumPayment::model()->findAll(), 'id', 'context'),
			array('template' => '<tr><td><p>{input}</p></td><td><p>{label}</p></td></tr>')
			);
			?>
                             
                                </table>
                         <?php  echo TbHtml::submitButton(Yum::t('CONTINUE'),
                             array('size'=>TbHtml::BUTTON_SIZE_LARGE, 'color'=>  TbHtml::BUTTON_COLOR_DANGER));?>
                 <?php $this->endWidget(); ?>
		</div>        
           </div>
	</div> <!-- form -->
        </div>
	
		<?php
    Yii::app()->clientScript->registerScript('toggle', "
		$('#detail-information').hide();	  
		$('#more-information').click(function() {
	   	$('#detail-information').fadeToggle('slow');
	   });
    ");
    ?>
	

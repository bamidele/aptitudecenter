<?php
/* @var $this BundleController */
/* @var $model Bundle */
?>

<?php
$this->breadcrumbs=array(
	'Bundles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Bundle', 'url'=>array('index')),
	array('label'=>'Manage Bundle', 'url'=>array('admin')),
);
?>

<h1>Create Bundle</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'tests'=>$tests)); ?>
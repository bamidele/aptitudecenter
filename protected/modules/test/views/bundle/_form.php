<?php
/* @var $this BundleController */
/* @var $model Bundle */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'bundle-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'name',array('span'=>5,'maxlength'=>100)); ?>

            <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6,'span'=>8)); ?>
            <?php echo $form->textFieldControlGroup($model,'price',array('span'=>5,'maxlength'=>100)); ?>

            <?php echo $form->checkBoxControlGroup($model,'isfree',array('span'=>5)); ?>
            
    <hr>
   <?php if ($model->isNewRecord){ ?>
            <div class="row-fluid">
                <?php 
                 foreach ($tests as $t){
                ?>
                 <div class="span4">
                     <?php echo TbHtml::checkBox($t->id_test, false, array('label' => $t->test_name)); ?>
                 </div>
                 <?php
                 }
                ?>
            </div>
   <?php } else {
       
   
?>
     <hr>
    <div class="row-fluid">
                <?php 
                 foreach ($tests as $t){
                ?>
                 <div class="span4">
                     <?php echo TbHtml::checkBox($t['id_test'], true, array('label' => $t['test_name'])); ?>
                 </div>
                 <?php
                 }
                ?>
            </div>
      <hr>
     <div class="row-fluid">
         
                <?php  if(isset($testnot)){
                 foreach ($testnot as $ts){
                ?>
                 <div class="span4">
                     <?php echo TbHtml::checkBox($ts['id_test'],false, array('label' => $ts['test_name'])); ?>
                 </div>
                 <?php
                }}
                ?>
        </div>
   <?php } ?>
        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
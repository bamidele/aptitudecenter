<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php $this->breadcrumbs = array(Yum::t('Failure'));?>

<?php $this->title = Yum::t('Transaction Failure'); ?>
         <div class ="row-fluid">
            <?php echo TbHtml::lead('Aptitudecenter Transcation Failure', array('color'=>TbHtml::ALERT_COLOR_ERROR)); ?>
            </div>
	<div class="row-fluid">
           
            <div class="span5 well">
               <div class="row-fluid ">
                   <h4>The Benefits of using Aptitude Center</h4>
                    <p> 
                    <ul class="icons-ul">
                    <li> <i class="icon-li icon-ok-sign icon-2x"></i>Just like the real thing</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i> Timed test simulations</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Instant online access 24/7</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Free access to updates</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Clear worked solutions</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Compare your performance</li>
                    </ul>
                    </p>
                 </div>
            </div>
             <div class="span7 shadowbox" style="padding: 10px;">
                 <div class="row-fluid">
                 <?php echo TbHtml::lead('Sorry, there is a problem with your transaction, contact support center at support@aptitudecenter.com '); ?>
                     <?php echo TbHtml::em('Error Message : '.$message); ?>
                 <?php echo TbHtml::link(TbHtml::Button(Yii::t('Profile Page'), array('color' => TbHtml::BUTTON_COLOR_SUCCESS,'size'=>  TbHtml::BUTTON_SIZE_LARGE)),'/profile/profile/view'); ?>

                </div>
                 <div class="row-fluid">
		
		</div>
           </div>
	</div> <!-- form -->
      
   
	

<?php $this->breadcrumbs = array(Yum::t('Payment Instruction- Bank Deposit'));?>
<?php Yum::renderFlash(); ?> 
<?php $this->title = Yum::t('Payment Instruction- Bank Deposit '); ?>
         <div class ="row-fluid">
            <?php echo TbHtml::lead('Payment Instruction- Bank Deposit'); ?>
            </div>
	<div class="row-fluid">
           
            <div class="span5 well">
               <div class="row-fluid ">
                   <h4>The Benefits of using Aptitude Center</h4>
                    <p> 
                    <ul class="icons-ul">
                    <li> <i class="icon-li icon-ok-sign icon-2x"></i>Just like the real thing</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i> Timed test simulations</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Instant online access 24/7</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Free access to updates</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Clear worked solutions</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Compare your performance</li>
                    </ul>
                    </p>
                 </div>
            </div>
            <div class="span6 well">
                
               <div class="row-fluid">
                    <table>
                        
                        <tbody>
                            <tr>
                                <td> <b>Name :</b> </td>
                                <td><?php  echo $user->profile->name; ?></td>
                            </tr>
                            <tr>
                                <td> <b>Transaction ID : </b></td>
                                <td> <?php  echo $trans->trans_id; ?></td>
                            </tr>
                            <tr>
                                <td><b>Bundle Name : </b></td>
                                <td> <?php  echo $bundle->name; ?></td>
                            </tr>
                            <tr>
                                <td><b> Amount to Pay : </b></td>
                                <td> &#8358; <?php  echo $bundle->price; ?></td>
                            </tr>
                            <tr>
                                <td><b> Description : </b></td>
                                <td> <?php  echo $bundle->description; ?></td>
                            </tr>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="row-fluid">
                    <?php echo TbHtml::lead('How to do  Bank Deposit payment to this account'); ?>
                    <p>
                      Go to any GTBank branch and make cash deposit into our account. Our account information is:
                        <li> <b>Account Name: Bamidele Alegbe </b></li>
                        <li> <b>Account number: 0108963914 </b></li>
                        <li> <b>Bank: GTBank</b> </li>
                       
                    </p>
                    <p>
                    After payment, please send an email to info@aptitudecenter.com indicating:
                        <li> Order ID: <?php echo $trans->trans_id ?> </li>
                        <li>Amount paid :&#8358; <?php  echo $bundle->price; ?></li>
                        <li>Payment Method: Bank Deposit </li>
                        <li>Depositor's name : <?php  echo $user->profile->name; ?> </li>
                        <li>Date of Payment : Date you  made the transfer </li>
Upon receipt of your email and confirmation of your payment, we enable access to your preparation pack and send you a notification email.
For enquiries regarding this transaction, please email info@aptitudecenter.com.
<br>
Regards,
<br>
<br>
Aptitudecenter Team
<br>
http://aptitudenter.com
                        
                    </p>
                    
                    
                </div>
            </div>
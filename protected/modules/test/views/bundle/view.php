<?php
/* @var $this BundleController */
/* @var $model Bundle */
?>

<?php
$this->breadcrumbs=array(
	'Bundles'=>array('index'),
	$model->name,
);
/**
$this->menu=array(
	array('label'=>'List Bundle', 'url'=>array('index')),
	array('label'=>'Create Bundle', 'url'=>array('create')),
	array('label'=>'Update Bundle', 'url'=>array('update', 'id'=>$model->idbundle)),
	array('label'=>'Delete Bundle', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idbundle),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bundle', 'url'=>array('admin')),
);
 * 
 */
?>

<h1>View Bundle #<?php echo $model->idbundle; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'idbundle',
		'name',
		'description',
		'isfree',
                'price',
		'date_created',
	),
)); ?>
<div class="row-fluid">
        <div class="span12">
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><label class="label"> Tests in  this  Bundle</div>
 

  <!-- List group -->
  <ul class="list-group">
      <?php  foreach ($tests as $n){ ?>
      <li class="list-group-item">
             <b><a href="<?php echo $this->createUrl('/test/tests/details',array('id'=>$n['id_test']));?>">
      <?php echo $n['test_name'] ?> 
       </a></b></li>
      <?php }?>
   
 
  </ul>
</div></div>
    
</div>
<?php
/* @var $this BundleController */
/* @var $model Bundle */
?>

<?php
$this->breadcrumbs=array(
	'Bundles'=>array('index'),
	$model->name=>array('view','id'=>$model->idbundle),
	'Update',
);
/*
$this->menu=array(
	array('label'=>'List Bundle', 'url'=>array('index')),
	array('label'=>'Create Bundle', 'url'=>array('create')),
	array('label'=>'View Bundle', 'url'=>array('view', 'id'=>$model->idbundle)),
	array('label'=>'Manage Bundle', 'url'=>array('admin')),
);
 * 
 */
?>

    <h1>Update Bundle <?php echo $model->idbundle; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'tests'=>$tests,'testnot'=>$testnot)); ?>
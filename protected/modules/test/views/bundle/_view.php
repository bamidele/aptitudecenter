<?php
/* @var $this BundleController */
/* @var $data Bundle */
?>
<h4> Details of this Bundle</h4>
<div class="row-fluid">
<div class="view">

    	

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />
         <b><?php echo 'Type : ' ?>:</b>
	<?php echo CHtml::encode(($data->isfree =1)? 'Free':'Paid'); ?>
	<br />




<div class="row-fluid">
        <div class="span12">
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><label class="label"> Tests in  this  Bundle</div>
 

  <!-- List group -->
  <ul class="list-group">
      <?php  foreach ($tests as $n){ ?>
      <li class="list-group-item">
            <a href="<?php echo $this->createUrl('/test/tests/details',array('id'=>$n['id_test']));?>">
             <?php echo '<b>'.$n['test_name'].'</b>   (click to view details)' ?> 
       </a></li>
      <?php }?>
   
 
  </ul>
</div></div>
    
</div>
<?php
/* @var $this BundleController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Bundles',
);

$this->menu=array(
	array('label'=>'Create Bundle','url'=>array('create')),
	array('label'=>'Manage Bundle','url'=>array('admin')),
);
?>

<h1>Bundles</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
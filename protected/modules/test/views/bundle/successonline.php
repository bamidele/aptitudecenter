<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php $this->breadcrumbs = array(Yum::t('Success'));?>

<?php $this->title = Yum::t('Transaction Success'); ?>
         <div class ="row-fluid">
            <?php echo TbHtml::lead('Aptitudecenter Transcation Success'); ?>
            </div>
	<div class="row-fluid">
           
            <div class="span5 well">
               <div class="row-fluid ">
                   <h4>The Benefits of using Aptitude Center</h4>
                    <p> 
                    <ul class="icons-ul">
                    <li> <i class="icon-li icon-ok-sign icon-2x"></i>Just like the real thing</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i> Timed test simulations</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Instant online access 24/7</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Free access to updates</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Clear worked solutions</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Compare your performance</li>
                    </ul>
                    </p>
                 </div>
            </div>
             <div class="span7 shadowbox" style="padding: 10px;">
                 <div class="row-fluid">
                 <?php echo TbHtml::lead('Your Transaction was Successful, go Back to your Profile Page and Start Practicing'); ?>
                    <br>
                  <?php echo TbHtml::lead('Bundle Name: '.$bundle->name); ?>
                  <?php echo TbHtml::lead('Bundle Price: '.$bundle->formattedprice); ?>
                  <?php echo TbHtml::lead('Bundle Description: '.$bundle->description); ?>
                    <br>
                 <?php echo TbHtml::link(TbHtml::Button(Yii::t('Profile Page'), array('color' => TbHtml::BUTTON_COLOR_SUCCESS,'size'=>  TbHtml::BUTTON_SIZE_LARGE)),'/profile/profile/view'); ?>

                </div>
                 <div class="row-fluid">
		
		</div>
           </div>
	</div> <!-- form -->
      
   
	

<?php
$this->metaKeywords = 'tests,practice, aptitude, objective,';
$this->metaDescription = 'this page is be  for online test practice with timer';
$remaining=$testReg->takenBy_to_test->duration;


$this->breadcrumbs=array(
	'Category'=>array('/site/index'),
	$testReg->takenBy_to_test->test_name,
);
 ?>
        <script type="text/javascript">
        function saru()
        {
            document.forms["myform"].submit();
           
        }
        function doonclick(){
           $('#defaultCountdown').countdown({
                until: '+0h <?php echo $remaining?>m +0s',
                format: 'hMS',
                significant: 2,
                onExpiry:saru,
                compact: true,
                layout: ' Time Left: <b>{mnn} min {snn} sec</b> '
            });
           
        }
	$(function(){
             
             $('#myModal').modal('show');
             
             
           $("#submit_but").hide();
            last=<?php echo $testReg->takenBy_to_test->num_of_questions ?>;
            $(".demo5").paginate({
				count 		: last,
				start 		: 1,
				display     : 7,
				border	: true,
				border_color	: '#232D65',
				text_color  	: '#999999',
				background_color  : '#ffffff',	
				border_hover_color : '#ffffff',
				text_hover_color  : '#ffffff',
				background_hover_color	: '#232D65', 
				images			: false,
				mouse			: 'press',
				onChange     		: function(page){
                                                                $('._current','#paginationdemo').removeClass('_current').hide();
                                                                $('#p'+page).addClass('_current').fadeIn();
                                                                 $("#submit_but").fadeOut();
                                                                 window.scrollTo(0,300);
                                                                if(page==last)
                                                                {
                                                                    $("#submit_but").fadeIn();
                                                                }
                                                          }
			});
                        
                      
                    });
              
        </script>
        
        <?php 
            
            $this->widget('bootstrap.widgets.TbModal', array(
             
              
            'id' => 'myModal',
            'header' => TbHtml::em('Instructions', array('color' => TbHtml::TEXT_COLOR_INFO)),
            'content' => $this->renderPartial('_start',array('model'=>$testReg),true),
            'footer' => array(
            
            TbHtml::button('Start Test', array('data-dismiss' => 'modal','id'=>'start',
                'onclick'=>'return doonclick()',
                'color' => TbHtml::BUTTON_COLOR_PRIMARY)),
            ),
    )); 
   
    
     ?>
   <div id="test">
       
   </div>
 <div id="quizbox" class="row-fluid">
 <div class="span9">
     
   <div id="begin" class="">
   <div class="row-fluid">
            <div class="span3">
            <div id="name" class="well">
                <h5>Welcome : <?php echo user()->name; ?></h5>
            </div>
            </div>
            <div class="span6">
            <h3> <div id="logo">Aptitude Center Quiz</div></h3>
            <h4><?php echo $testReg->takenBy_to_test->test_name;  ?></h4>
            </div>   
           <div class="span3">
            <div id="defaultCountdown" class="well"></div>
           </div>
    </div>
     <div class="row-fluid">
    <div id="paginationdemo" class="demo">

         <form action="<?php echo $this->createUrl('taketest/finish')?>" name="myform" id="full" method="post"> 

        <?php
       
        //shuffle($questions);
        $c=1;
        foreach ($models as $row)
        {
         
              ?>
             <div id="p<?php echo $row->questions_numbers; ?>" class="<?php echo($c==1)?'pagedemo _current':'pagedemo'; ?>" style="<?php echo($c==1)?'':'display:none;'; ?>">
             <div class="row_fluid ">
                 <div class="span12">
                      <h5><?php echo TbHtml::em('Question: '.$row->questions_numbers ,array('color' => TbHtml::TEXT_COLOR_INFO)); ?></h5>
                    
                 </div>
             </div>
             <div class="row_fluid ">
                 <div class="span12">
                    <?php if($row->q_to_p !== null){ ?>
                     <h6> Passage </h6>
                     <p><?php echo TbHtml::well($row->q_to_p->passage) ?></p>
                    <?php } ?>
                    
                 </div>
             </div>
             <div class="row_fluid">
                 <div class="span12">
                    <?php echo $row->question; ?>
                 </div>
             </div>       
                 
     
    
                  
                 <div class="row_fluid">
                     <div class="span6">
                     <table border="0">
                             <thead>
                                 <tr>
                                     <th></th>
                                     <th></th>
                                     <th></th>
                                 </tr>
                             </thead>
                             <tbody>
                                 <tr>
                                     <td><p>A</p></td>
                                     <td><p><input type="radio" name="group<?php echo $row->questions_numbers; ?>" value="1"></p></td>
                                     <td><?php echo $row->option_a; ?></td>
                                 </tr>
                                 <tr>
                                     <td><p>B</p></td>
                                     <td> <p><input type="radio" name="group<?php echo $row->questions_numbers; ?>" value="2"></p></td>
                                     <td><?php echo $row->option_b; ?></td>
                                 </tr>
                                 <tr>
                                     <td><p>C</p></td>
                                     <td><p><input type="radio" name="group<?php echo $row->questions_numbers; ?>" value="3"></p></td>
                                     <td><?php echo $row->option_c; ?></td>
                                 </tr>
                                 <?php if ($testReg->takenBy_to_test->num_of_options >=4){?>
                                 <tr>
                                     <td><p>D</p></td>
                                     <td><p><input type="radio" name="group<?php echo $row->questions_numbers; ?>" value="4"></p></td>
                                     <td><?php echo $row->option_d; ?></td>
                                 </tr>
                                 <?php } ?>
                                 <?php if ($testReg->takenBy_to_test->num_of_options >=5){?>
                                 <tr>
                                     <td><p>E</p></td>
                                     <td><p><input type="radio" name="group<?php echo $row->questions_numbers; ?>" value="5"></p></td>
                                     <td><?php echo $row->option_e; ?></td>
                                 </tr>
                                  <?php } ?>
                             </tbody>
                         </table>

                        
                     </div></div>
                     
                         
                    
            </div>
        <?php
            $c++;
        
        } ?>
           
         <div class="row_fluid">
             
             <div class="span3"> 
            </div>
             <div class="span6"> 
                     <br>
                    <br>
                    <?php echo CHtml::hiddenField('tid',$testReg->id);?>
               <?php echo TbHtml::submitButton('Submit',array('block' => true,
                   'color' => TbHtml::BUTTON_COLOR_PRIMARY, 'id'=>'submit_but',
                   'size'=>TbHtml::BUTTON_SIZE_LARGE
                   )); ?>
                    <br>
           <br>
             </div>
                <div class="span3"> 
                </div>
       
         </div>
     
 </form>
</div><div class="row_fluid">
    <div class="span12"> 
        <br>
        <div class="demo5">
        </div>
        <br>
        <br>
    </div>
</div>
    
 </div>
  </div>
</div>
 <div class="span3">
    <div class="row-fuild">
             <div class="well" style="font-size: 12px; font-weight: normal;">
         <h5><?php echo TbHtml::em('Notes:',array('color' => TbHtml::TEXT_COLOR_INFO)); ?></h5>
       
      <ul>
        <li>Click the <b>'Submit Test'</b> button given in the bottom of this page to Submit your answers.</li>
        <li>Test will be <b>submitted </b>automatically if the <b>time expired.</b></li>
        <li><?php echo TbHtml::em(" <b>Don't refresh the page</b>",array('color' => TbHtml::TEXT_COLOR_WARNING)); ?></li>
      </ul>
             </div>
    </div>
     <div class ="row-fluid">
         <script type="text/javascript">
      ad_client = "pub-3763";
      ad_slot = 1;
      ad_type = "imagead-200-3";
      </script>
<script type="text/javascript" src="http://openadnation.com/scripts/adstation.js"></script>
     </div>
     
 </div>
 </div>
 
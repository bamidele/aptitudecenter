<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    $am= array(
             
                      array('label'=>mt('My Account'), 'visible'=>!user()->isGuest,'items'=>array(
                             
                             array('label'=>mt('My Profile'),'url'=>array('//profile/profile'),'visible'=>!user()->isGuest),
                             array('label'=>mt('Edit My Profile'),'url'=>array('//profile/profile'),'visible'=>!user()->isGuest),
                             array('label'=>mt('Buy a Bundle'),'url'=>array('//profile/profile'),'visible'=>!user()->isGuest),
                             array('label'=>mt('Membership'),'url'=>array('//profile/profile'),'visible'=>!user()->isGuest),
                             TbHtml::menuDivider(),
                             array('label'=>'Logout ('.  user()->name.')', 'url'=>array('//user/user/logout'), 'visible'=>!user()->isGuest),
                          )),
                      
                        array(
                                        'icon'=>'icon-tasks icon-white',
                                        'label'=>'Login',
                                        'url'=>'javascript:void(0)',
                                        'visible'=>user()->isGuest,
                                        'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>'dropdown'),
                                        'itemOptions'=>array('class'=>'grey'),
                                        'class'=>'ext.bootstrap-ext.widgets.TbDropdownExt',
                                        'view'=>'_partials._usermenu_loginform',
                                        'submenuOptions'=>array('class' => 'fixed-panel pull-right dropdown-navbar dropdown-caret dropdown-closer'),
                            ),
                        
                    );
                    array_push($am, $tree);
 
  $this->widget('bootstrap.widgets.TbNavbar', array(
    'brandLabel' => 'Title',
    'display' => null, // default is static to top
   'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbNav',
            'items' =>$am,
        ),
    ),
            
       
)); 
?>

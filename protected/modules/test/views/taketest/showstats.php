   <?php 
            
            $this->widget('bootstrap.widgets.TbModal', array(
             
              
            'id' => 'myModal',
            'header' => TbHtml::em('Results', array('color' => TbHtml::TEXT_COLOR_INFO)),
            'content' =>  $this->renderPartial('_show',array('score'=>$score,'number_question'=>$number_question
                    ,'attempted'=>$attempted, 'test'=>$test),true),
            'footer' => array(
            
            TbHtml::button('Go to HomePage', array('data-dismiss' => 'modal','color' => TbHtml::BUTTON_COLOR_PRIMARY, 'id'=>'home', 'onclick'=> 'return doonclick()')),
            ),
    )); 
   
    
     ?>
<script>
  function doonclick(){
      window.location.href = "<?php echo Yii::app()->getBaseUrl(true); ?>";
  }
$(document).ready(function(){
   $('#myModal').modal('show');
  
});
</script>
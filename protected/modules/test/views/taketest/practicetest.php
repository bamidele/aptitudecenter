<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
     <?php 
     $cat= $catid[0]['cat_id'];
        $this->breadcrumbs=array(
	'Category'=>array("/test/testcategory/list/id/$cat"), 'Practice Test / ',
	$test->test_name, );
?>
<div class="row-fluid">
<div class="span9">

 
    
    <?php $this->widget('bootstrap.widgets.TbPager', array(
    'pages' => $pages,
     'maxButtonCount'=>15,
     'id'=>'link_pager'
)) ?>
<?php foreach($models as $model): ?>
  
    <div class="row-fluid">
       <?php if($model->q_to_p){
           
       } ?>
        
    </div>
<div class="row-fluid">
     <h6>Question : <?php echo $model->questions_numbers ?></h6>
    <p>
       
        <?php echo $model->question ?>
        
    </p>
    <table class="ques_<?php echo $model->questions_numbers?>">
        <?php  if ($model->question_to_test->test_to_tt->option==1){?>
                <tr>
                    <td><p  id="ans_a_<?php echo $model->questions_numbers ?>" class="ans"><?php echo ($model->answer==1)? "<i class=\"icon-ok-sign ans_$model->questions_numbers\"><i>":'';?></p></td>
                    <td><p><a  class="option" onClick="doClick(this.id);" id="lnkOptionLink_a_<?php echo $model->questions_numbers ?>" href="javascript: void 0;">A.</a></p></td>
                    <td><?php echo $model->option_a ?></td>
                </tr>
                <tr>
                    <td><p  id="ans_b_<?php echo $model->questions_numbers ?>" class="ans"><?php echo ($model->answer==2)? "<i class=\"icon-ok-sign ans_$model->questions_numbers\"><i>":'';?></p></td>
                    <td><p><a class="option" onClick="doClick(this.id);" id="lnkOptionLink_b_<?php echo $model->questions_numbers ?>" href="javascript: void 0;">B.</a></p></td>
                    <td><?php echo $model->option_b ?></td>
                </tr>
                <?php if($model->question_to_test->num_of_options >=3){ ?>
                <tr>
                     
                    <td><p id="ans_c_<?php echo $model->questions_numbers ?>" class="ans"><?php echo ($model->answer==3)? "<i class=\"icon-ok-sign ans_$model->questions_numbers\"><i>":'';?></p></td>
                    <td><p><a class="option" onClick="doClick(this.id);" id="lnkOptionLink_c_<?php echo $model->questions_numbers ?>" href="javascript: void 0;">C.</a></p></td>
                    <td><?php echo $model->option_c ?></td>
                </tr>
                <?php }?>
                
                <?php if($model->question_to_test->num_of_options >=4){ ?> 
                <tr>
                    <td><p id="ans_d_<?php echo $model->questions_numbers ?>" class="ans"><?php echo ($model->answer==4)? "<i class=\"icon-ok-sign ans_$model->questions_numbers\"><i>":'';?></p></td>
                    <td><p><a class="option"  onClick="doClick(this.id);" id="lnkOptionLink_d_<?php echo $model->questions_numbers ?>" href="javascript: void 0;">D.</a></p></td>
                    <td><?php echo $model->option_d ?></td>
                </tr>
                <?php }?>
                <?php if($model->question_to_test->num_of_options >=5){ ?> 
                <tr>
                    <td><p id="ans_e_<?php echo $model->questions_numbers ?>" class="ans"><?php echo ($model->answer==5)? "<i class=\"icon-ok-sign ans_$model->questions_numbers\"><i>":'';?></p></td>
                    <td><p><a class="option" onClick="doClick(this.id);" id="lnkOptionLink_e_<?php echo $model->questions_numbers ?>" href="javascript: void 0;">E.</a></p></td>
                    <td><?php echo $model->option_e ?></td>
                </tr>
                <?php }?>
        <?php } ?>
        </table>
        <div class="row_fluid">
            
     
  <div class="accordion-group" style="display: none" id="explain_<?php echo $model->questions_numbers ?>">
    <div class="accordion-heading">
      <a class="accordion-toggle" href="javascript: void 0;" onclick="$('#explain_<?php echo $model->questions_numbers?>').slideToggle('slow');">
        <i class="icon-remove-sign"></i>   Close Explanation
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse in">
      <div class="accordion-inner">
          <p> Answer : <?php echo Questions::getAnswer($model->answer)?> </p>
          <?php if ($model->working!=null){?>
          <h6>Explanation</h6>
          <p> <?php echo $model->working  ?></p>
          <?php } ?>
    </div>
    </div>
  </div>
  
   <div class="accordion-group" style="display: none" id="report_<?php echo $model->questions_numbers ?>">
    <div class="accordion-heading">
      <a class="accordion-toggle" href="javascript: void 0;" onclick="$('#report_<?php echo $model->questions_numbers?>').slideToggle('slow');">
        <i class="icon-remove-sign"></i>   Close Report
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse in">
      <div class="accordion-inner">
          <div id="send_<?php echo $model->questions_numbers ?>" style="display: none;">
          </div>
                <?php echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_HORIZONTAL,'','post', array('id'=>"reportForm_$model->questions_numbers")); ?>
                <?php echo TbHtml::emailFieldControlGroup('email', '',
                    array('label' => 'Email', 'placeholder' => 'Email')); ?>
                 <?php echo CHtml::hiddenField('qid', $model->id_question); ?>
                <?php echo TbHtml::textAreaControlGroup('report', '',
                    array('label' => 'Report', 'placeholder' => 'write your report here...','span'=>11,'rows'=> 6,
                        
                             ));
                ?> 
                <div class="row-fluid">
                    
                <div class="span9 offset3">
                <?php 
                   echo TbHtml::ajaxSubmitButton('Send Report',  url('/test/tests/report'), array(

                    'type'=>'POST',
                     "dataType" => "json",
                     'beforeSend' => "function(data){
                         $(\"#send_$model->questions_numbers\").toggle();
                         $(\"#send_$model->questions_numbers\").html('sending...');
                       }",
                       'data'=>"js:$(\"#reportForm_$model->questions_numbers\").serialize()",//this oe
                       'success'=>"js:function(data){
                           if(data.result===\"success\"){
                              $(\"#send_$model->questions_numbers\").html('your report was sent succesfully');
                                 $(\"#submit_$model->questions_numbers\").attr(\"disabled\", \"true\");
                                     $(\"#reportForm_$model->questions_numbers\").each(function(){
                                            this.reset();
                                        });
                           }else{
                              $(\"#send_$model->questions_numbers\").html('failed to send report... try again later');
                           }
                           }",
                    ), array('id'=>"submit_$model->questions_numbers"));
               ?></div>
               </div>
            <?php echo TbHtml::endForm(); ?>

    </div>
    </div>
  </div>

         <div class="btn-group">
             
             
             
            <button type="button"  onclick="$('#explain_<?php echo $model->questions_numbers?>').slideToggle('slow');" class="btn btn-default" id="ans<?php echo $model->questions_numbers ?>"><i class="icon-book"></i>  View Answer</button>
            <button type="button" onclick="$('#report_<?php echo $model->questions_numbers?>').slideToggle('slow');"  class="btn btn-default" id="ans<?php echo $model->questions_numbers ?>"> <i class="icon-warning-sign"></i>  Report Error</button>
            <a href="<?php echo url('/forum/thread/view',array('id'=>$model->topic_id))?>"><button type="button" class="btn btn-default"  id="ans<?php echo $model->questions_numbers ?>"><i class="icon-comments-alt"></i> Discuss in Forum</button></a>
        </div>
         
        </div>
               
               
          </div>
        <hr>
        <?php endforeach; ?>
        <?php $this->widget('bootstrap.widgets.TbPager', array(
    'pages' => $pages,
     'maxButtonCount'=>15,
     'id'=>'link_pager'
)) ?>
        </div>
        <div class="span3">
        <script type="text/javascript">
      ad_client = "pub-3763";
      ad_slot = 1;
      ad_type = "imagead-200-7";
      </script>
<script type="text/javascript" src="http://openadnation.com/scripts/adstation.js"></script>
        </div> 
    </div>
    
<script>  
    function doClick(clicked_id)
    {
      var num= clicked_id.slice(14);
      $('table tr').each(function () {
        //processing this row
        //how to process each cell(table td) where there is checkbox
        var ans= '#ans_'+num;
       
        $(this).find(ans).each(function () {
           
            if( this.id.indexOf(num)!== -1){
                $(ans).show();
              }
              {
                  $(clicked_id).css({
		
                    'opacity':'0.5'
	});
              } 
             });
        });
        
      
    }
    
  $(document).ready(function(){
        
        $('.ans').hide();
       
  
});
</script>

<div class="row-fluid">
    
    <div class="span6 instruction">
        
       
        <h3> <?php echo TbHtml::em('Instruction:',array('color' => TbHtml::TEXT_COLOR_INFO)); ?></h3>
          
      <ul>
        <li>Total number of questions : <b><?php echo $model->takenBy_to_test->num_of_questions;  ?></b></li>
        <li>Time alloted : <b><?php echo $model->takenBy_to_test->duration;  ?> minutes</b></li>
        <li>Each question carry 1 mark, no negative marks.</li>
        <li></li>
      </ul>

        
        <p>
        
    </div>
    <div class="span6">
        <h3><?php echo TbHtml::em('Notes:',array('color' => TbHtml::TEXT_COLOR_INFO)); ?></h3>
       
      <ul>
        <li>Click the <b>'Submit Test'</b> button given in the bottom of this page to Submit your answers.</li>
        <li>Test will be <b>submitted </b>automatically if the <b>time expired.</b></li>
        <li><?php echo TbHtml::em(" <b>Don't refresh the page</b>",array('color' => TbHtml::TEXT_COLOR_WARNING)); ?></li>
      </ul>
       
    </div>
</div>

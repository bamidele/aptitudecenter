<div class="row-fluid">
    <div class="span2"></div>
    <div class="span8">
     <div class="row-fluid">
        <table border="0">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><h5><?php echo TbHtml::em('Number of Questions',array('color' => TbHtml::TEXT_COLOR_SUCCESS)); ?></h5></td>
                    <td><h5><?php echo TbHtml::em($number_question,array('color' => TbHtml::TEXT_COLOR_SUCCESS)); ?></h5></td>
                </tr>
                <tr>
                    <td><h5><?php echo TbHtml::em('Number of Attempted Questions',array('color' => TbHtml::TEXT_COLOR_INFO)); ?></h5></td>
                    <td><h5><?php 
                    echo TbHtml::em($attempted,array('color' => TbHtml::TEXT_COLOR_INFO));
                    ?></h5></td>
                </tr>
                <hr>
                <tr>
                    <td><h4><?php echo TbHtml::em('Your Score: ',array('color' => TbHtml::TEXT_COLOR_INFO)); ?></h4></td>
                    <td><h4><?php echo TbHtml::em($score,array('color' => TbHtml::TEXT_COLOR_INFO));  ?></h4></td>
                </tr>
                <tr>
                    <td><h4><?php echo TbHtml::em('Pecentage',array('color' => TbHtml::TEXT_COLOR_INFO)); ?></h4></td>
                    <td><h4><?php 
                    $percent = round(($score/$number_question)*100,1);
                    echo TbHtml::em($percent.'%',array('color' => TbHtml::TEXT_COLOR_INFO));  ?></h4></td>
                </tr>
            </tbody>
        </table>
        </div>
        <div class="row-fluid">
            <h6>Share your Score </h6>
            <!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style addthis_32x32_style"
     addthis:url="http://aptitudecenter.com"
       addthis:title="My Online Practice Score for <?php echo $test->test_name; ?>  is  <?php echo $percent.'%'.'' ?> "
      
     >
<a class="addthis_button_facebook" 
   addthis:url="http://aptitudecenter.com"
   addthis:title="My Online Practice Score for <?php echo $test->test_name; ?>  is  <?php echo $percent.'%'.'' ?> "></a>
<a class="addthis_button_google_plusone_share"></a>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_linkedin"></a>
</div>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5231aaa22fdab04e"></script>
<!-- AddThis Button END -->
        </div>
  
    </div>
   <div class="span2"></div>
</div>


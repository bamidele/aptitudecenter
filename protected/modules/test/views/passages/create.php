<?php
/* @var $this PassagesController */
/* @var $model Passages */
?>

<?php
$this->breadcrumbs=array(
	'Passages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Passages', 'url'=>array('index')),
	array('label'=>'Manage Passages', 'url'=>array('admin')),
);
?>

<h1>Create Passages</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
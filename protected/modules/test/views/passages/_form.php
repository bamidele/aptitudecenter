<?php
/* @var $this PassagesController */
/* @var $model Passages */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'passages-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'qid',array('span'=>5)); ?>

         
                <?php 
$attribute = 'passage';
$value = (isset($model->passage))? $model->passage:'';
    ?>
    <div class="row-fluid">
    <div class="span7" style="min-height: 200px;">
    <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
    'name'=>'passage',
    'value'=>$value,
    'pluginOptions' => array(
        
        'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
        'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
            'attr' => $attribute
        )),
        'fileUploadErrorCallback' => new CJavaScriptExpression(
            'function(obj,json) { alert(json.error); }'
        ),
        'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
            'attr' => $attribute
        )),
        'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
            'attr' => $attribute
        )),
        'imageUploadErrorCallback' => new CJavaScriptExpression(
            'function(obj,json) { alert(json.error); }'
        ),
    ),
    
)); ?> 
    </div></div>
        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/* @var $this PassagesController */
/* @var $model Passages */
?>

<?php
$this->breadcrumbs=array(
	'Passages'=>array('index'),
	$model->idpassages,
);

$this->menu=array(
	array('label'=>'List Passages', 'url'=>array('index')),
	array('label'=>'Create Passages', 'url'=>array('create')),
	array('label'=>'Update Passages', 'url'=>array('update', 'id'=>$model->idpassages)),
	array('label'=>'Delete Passages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idpassages),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Passages', 'url'=>array('admin')),
);
?>

<h1>View Passages #<?php echo $model->idpassages; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'idpassages',
		'qid',
		'passage',
	),
)); ?>
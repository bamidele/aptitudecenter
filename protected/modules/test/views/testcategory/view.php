<?php
/* @var $this TestcategoryController */
/* @var $model Testcategory */
?>

<?php
$this->breadcrumbs=array(
	'Testcategories'=>array('index'),
	$model->cat_id,
);

$this->menu=array(
	array('label'=>'List Testcategory', 'url'=>array('index')),
	array('label'=>'Create Testcategory', 'url'=>array('create')),
	array('label'=>'Update Testcategory', 'url'=>array('update', 'id'=>$model->cat_id)),
	array('label'=>'Delete Testcategory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->cat_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Testcategory', 'url'=>array('admin')),
);
?>

<h1>View Testcategory #<?php echo $model->cat_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'cat_id',
		'cat_name',
		'date_created',
		'description',
	),
)); ?>
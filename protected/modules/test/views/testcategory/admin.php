<?php
/* @var $this TestcategoryController */
/* @var $model Testcategory */


$this->breadcrumbs=array(
	'Testcategories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Testcategory', 'url'=>array('index')),
	array('label'=>'Create Testcategory', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#testcategory-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#testcategory-grid a.ajaxupdate').live('click', function() {
        $.fn.yiiGridView.update('testcategory-grid', {
                type: 'POST',
                url: $(this).attr('href'),
                success: function() {
                        $.fn.yiiGridView.update('testcategory-grid');
                }
        });
        return false;
});
");
?>

<h1>Manage Testcategories</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'testcategory-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'cat_id',
		'cat_name',
                
		'description',
                 array(
					'type' => 'raw',
					'name' => Yum::t('created_by'),
					'value' => 'CHtml::link($data->cat_to_user->username, array(
							\'/profile/profile/view/\',
							"id" =>$data->created_by)
						)'
					),
                    array(
        'name'=>'publish',
        'type'=>'raw',
        'value'=>'CHtml::link(($data->publish==1)? \'yes\':\'no\',array(\'ajaxupdate\',
            \'id\'=>$data->cat_id), array("class"=>"ajaxupdate"))'
),
                 array(
				'name'=>'date_created',
				'filter' => false,
				'value'=>'date(UserModule::$dateFormat,$data->date_created)',
			),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
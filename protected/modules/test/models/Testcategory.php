<?php

/**
 * This is the model class for table "testcategory".
 *
 * The followings are the available columns in table 'testcategory':
 * @property integer $cat_id
 * @property string $cat_name
 * @property integer $date_created
 * @property string $description
 * @property integer $created_by
 */
class Testcategory extends CActiveRecord
{
    
        public $category_tree=array();
        public $name;
        
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Testcategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function getSpecialName(){
            $this->name = str_replace(" ", "-", $this->cat_name);
            return $this->name;
        }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'testcategory';
	}

	
        
        public function behaviors()
        {
            return array(
                array(
                    'class'=>'ext.seo.components.SeoRecordBehavior',
                    'route'=>'test/testcategory/list',
                    'params'=>array('name'=>$this->getSpecialName(),'id'=>  $this->cat_id),
                ),
                'nestedSetBehavior'=>array(
                    'class'=>'ext.nested.NestedSetBehavior',
                    'leftAttribute'=>'lft',
                    'rightAttribute'=>'rgt',
                    'levelAttribute'=>'level',
                    'hasManyRoots'=>true,
                ),
                
            );
        }
        
        /**
	 * @return array validation rules for model attributes.
	 */
        
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cat_name, date_created, description,keyword created_by', 'required'),
			array('date_created, created_by', 'numerical', 'integerOnly'=>true),
			array('cat_name', 'length', 'max'=>200),
			
                        array('lft, rgt,root', 'length', 'max'=>10),
			array('cat_id, cat_name,keyword, date_created,publish,meta,description,
                            lft, rgt, level,root, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                   
                    'cat_to_test' => array(self::MANY_MANY, 'Tests', 'cat_test(cat_id,test_id)'),
                    'cat_to_user' => array(self::BELONGS_TO, 'YumUser', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cat_id' => 'Cat',
			'cat_name' => 'Cat Name',
			'date_created' => 'Date Created',
                        'keyword'=>'Keyword',
                        'publish'=>'Publish',
                        'meta'=>'Meta-description',
			'lft' => 'Lft',
			'rgt' => 'Rgt',
                        'root'=>'Root',
			'level' => 'Level',
			'description' => 'Description',
			'created_by' => 'Created By',
		);
	}
        
        public function getDate()
	{
		return date('m-d-Y G:i:s',$this->date_created);
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('cat_name',$this->cat_name,true);
		$criteria->compare('date_created',$this->date_created);
		$criteria->compare('description',$this->description,true);
                $criteria->compare('publish',$this->publish);
                $criteria->compare('meta',$this->meta);
		$criteria->compare('lft',$this->lft);
		$criteria->compare('rgt',$this->rgt);
                $criteria->compare('root',$this->root);
		$criteria->compare('level',$this->level);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public static function getCat(){
            
        
            $roots=Testcategory::model()->roots()->findAll();
            $arr=array();
       
            foreach ($roots as $r){
                $criteria=new CDbCriteria();
                $criteria->order = 't.level DESC';
                
                 $des=$r->descendants()->find($criteria);
                 if($des!=null){
                 
                 $level=$des->level;
                 
                 $cri=new CDbCriteria();
                 $cri->order = 't.level DESC';
                 $all=$r->descendants()->findAll($cri);
                 foreach($all as $a){
                     $arr[]=$a->cat_name;
                   
                 }
                }else{
                    $arr[]=$r->cat_name;
                }
                    
                 }
            
          
           return $arr;
           }
            
           
      
        
        public static function getCatDrop(){
            
            $criteria= new CDbCriteria();
            $criteria->select=array('cat_id','cat_name','level');
            $res=Testcategory::model()->findAll($criteria);
            $data= array();
            
            foreach($res as $r){
                if($r->level==1)
                $data[]=$r->cat_name;
                if($r->level==2)
                    $data[]='-'.$r->cat_name;
                if($r->level==3)
                    $data[]='--'.$r->cat_name;
                if($r->level==4)
                    $data[]='---'.$r->cat_name;
            }
            return $data;
        }
        
        public static function  getAllTest($catid){            
     
             $dbReader = Yii::app()->db->createCommand("
                 SELECT te.id_test, te.test_name
                    FROM tests te, cat_test ct
                WHERE ct.cat_id =$catid
               ")->query();
             $arr=array();
             foreach($dbReader as $r) {
               $arr[]= Tests::model()->findByPk($r['id_test']);
               }
               return $r;
        }
        /*
         * 
         */
          public static function getTreeRecursive($categories= null) {
                $criteria = new CDbCriteria;
                $criteria->order = 'root, lft';
                $criteria->condition = 'level = 1';
                if($categories==null)
                    $categories = Testcategory::model()->findAll($criteria);

                foreach($categories as $n => $category) {
                    $category_r = array(
                        'label'=>$category->cat_name,
                        'url'=>url("/test/testcategory/list",array("id"=>$category->cat_id)),
                        //'level'=>$category->level,
                    );              
                    $category_tree[$n] = $category_r;

                    $children = $category->children()->findAll();
                    if($children)
                        $category_tree[$n]['items'] = self::getChildren($children);
                }
                return $category_tree;
            }
        private static function getChildren($children) {
            $result = array();
            foreach($children as $i => $child) {
                $category_r = array(
                    'label'=>$child->cat_name,
                    'url'=>url("/test/testcategory/list",array("id"=>$child->cat_id))
                );          
                $result[$i] = $category_r;
                $new_children = $child->children()->findAll();
                if($new_children) {
                    $result[$i]['items'] = self::getChildren($new_children);
                }           
            }
            return $result_items = $result;
        }
}
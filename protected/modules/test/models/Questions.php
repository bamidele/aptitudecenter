<?php

/**
 * This is the model class for table "questions".
 *
 * The followings are the available columns in table 'questions':
 * @property integer $id_question
 * @property integer $test_id
 * @property integer $questions_numbers
 * @property string $question
 * @property string $option_a
 * @property string $option_b
 * @property string $option_c
 * @property string $option_d
 * @property string $answer
 * @property string $option_e
 * @property string $working
 * @property integer $date_created
 * @property integer $user_id
 */
class Questions extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Questions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'questions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('test_id, questions_numbers, question', 'required'),
			array('test_id, questions_numbers, date_created, user_id', 'numerical', 'integerOnly'=>true),
		
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_question, test_id, questions_numbers, question, option_a, option_b, option_c, option_d, answer, option_e, working, date_created, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'question_to_test' => array(self::BELONGS_TO, 'Tests', 'test_id','with'=>'test_to_tt'),
       
                    'question_to_user' => array(self::BELONGS_TO, 'YumUser', 'user_id'),
                    'q_to_p'=>array(self::HAS_ONE, 'Passages','qid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_question' => 'Id Question',
			'test_id' => 'Test',
			'questions_numbers' => 'Questions Numbers',
			'question' => 'Question',
			'option_a' => 'Option A',
			'option_b' => 'Option B',
			'option_c' => 'Option C',
			'option_d' => 'Option D',
			'answer' => 'Answer',
			'option_e' => 'Option E',
			'working' => 'Working',
			'date_created' => 'Date Created',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_question',$this->id_question);
		$criteria->compare('test_id',$this->test_id);
		$criteria->compare('questions_numbers',$this->questions_numbers);
		$criteria->compare('question',$this->question,true);
		$criteria->compare('option_a',$this->option_a,true);
		$criteria->compare('option_b',$this->option_b,true);
		$criteria->compare('option_c',$this->option_c,true);
		$criteria->compare('option_d',$this->option_d,true);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('option_e',$this->option_e,true);
		$criteria->compare('working',$this->working,true);
		$criteria->compare('date_created',$this->date_created);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public static function getQuestionOption($id){
            $model= Tests::model()->findByPk($id);
            
            if($model->num_of_options==5){
            return array(
                'option A',
                'option B',
                'option C',
                'option D',
                'option E',
            );
            
            
            }
            if($model->num_of_options==4){
            return array('option A','option B','option C','option D',);}
            if($model->num_of_options==3){
            return array('option A','option B','option C',);}
            
        }
         public function beforeDelete()
         {
                
           
             Passages::model()->deleteAll('qid = '.$this->id_question); //using the shows relation
              return parent::beforeDelete();
             

         }
        public static function getAnswer($option){
             if($option==1)
                 return 'option A';
             if($option==2)
                 return 'option B';
            if($option==3)
                 return 'option C';
            if($option==4)
                 return 'option D';
            if($option==5)
                 return 'option E';
        }
}
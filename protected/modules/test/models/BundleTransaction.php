<?php

/**
 * This is the model class for table "bundle_transaction".
 *
 * The followings are the available columns in table 'bundle_transaction':
 * @property integer $id
 * @property integer $trans_id
 * @property integer $user_id
 * @property integer $bundle_id
 * @property integer $status
 * @property integer $payment_id 
 * @property integer $orderdate
 *
 * The followings are the available model relations:
 * @property Bundle $bundle
 */
class BundleTransaction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bundle_transaction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trans_id, user_id, bundle_id', 'required'),
			array('trans_id, user_id, bundle_id, status, orderdate, payment_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, trans_id, user_id, bundle_id, status, orderdate, payment_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bundle' => array(self::BELONGS_TO, 'Bundle', 'bundle_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'trans_id' => 'Trans',
			'user_id' => 'User',
			'bundle_id' => 'Bundle',
			'status' => 'Status',
			'orderdate' => 'Orderdate',
                        'payment_id'=>'Payment ID'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('trans_id',$this->trans_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('bundle_id',$this->bundle_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('orderdate',$this->orderdate);
                $criteria->compare('payment_id',$this->payment_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BundleTransaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function beforeValidate() {
		if($this->isNewRecord) {
			$this->user_id = Yii::app()->user->id;		
			$this->orderdate = time();
              
                                
                }

		return parent::beforeValidate();
	}
         public function getStatus()
        {
            if($this->status==0)
                return "Pending";
            else if($this->status==1)
                return "Successful";
            else if($this->status==2)
                return "Failed";
        }
         public function getStatusStr()
        {
            if($this->status==0)
                return "Pending";
            else if($this->status==1)
                return "Successful";
            else if($this->status==2)
                return "Failed";
        }
        public function getUserid(){
            $user= YumUser::model()->findByPk($this->user_id);
            return $user->profile->name;
        }
        public function getBundle(){
            $bundle= Bundle::model()->findByPk($this->bundle_id);
           return $bundle->name;
        }
        public function getPayment(){
           if($this->payment_id==0)
               return 'Free';
           elseif ($this->payment_id==3)
               return 'Credit Card';
           elseif ($this->payment_id==4)
               return 'Online Transfer';
           elseif ($this->payment_id==6)
               return 'Bank Deposit';
           
        }
}

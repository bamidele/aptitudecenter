<?php

/**
 * This is the model class for table "test_taken_by".
 *
 * The followings are the available columns in table 'test_taken_by':
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_test
 * @property integer $date_taken
 * @property integer $total_score
 * @property integer $total_question
 * @property integer $question_attempted
 * @property integer $attempt
 */
class TestTakenBy extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TestTakenBy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'test_taken_by';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, id_test', 'required'),
			array('id_user, id_test, date_taken, total_score, total_question, question_attempted, attempt', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_user, id_test, date_taken, total_score, total_question, question_attempted, attempt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'takenBy_to_test' => array(self::BELONGS_TO, 'Tests', 'id_test'),
                    'takenBy_to_user' => array(self::BELONGS_TO, 'YumUser', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_test' => 'Id Test',
			'date_taken' => 'Date Taken',
			'total_score' => 'Total Score',
			'total_question' => 'Total Question',
			'question_attempted' => 'Question Attempted',
			'attempt' => 'Attempt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_test',$this->id_test);
		$criteria->compare('date_taken',$this->date_taken);
		$criteria->compare('total_score',$this->total_score);
		$criteria->compare('total_question',$this->total_question);
		$criteria->compare('question_attempted',$this->question_attempted);
		$criteria->compare('attempt',$this->attempt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        /**
         *  registers a test that needs to  be taken
         * @param type $tid
         * @return boolean
         */
         public static function registerTest($tid){
             
             $model= new TestTakenBy;
             $model->id_test= $tid;
             $model->id_user= user()->id;
             if($model->save()){
                 return $model->id;
             }
             return false;
         }
         public static function registerPracticeTest($tid){
             
             $user= user()->id;
             $time = time();
             $dbCommand = Yii::app()->db->createCommand("
                  INSERT INTO `takepractice` (`test_id`,`user_id`,`time`)
                     VALUES ({$tid},{$user},{$time})
               ")->execute();
         } 
         /**
          * 
          * @return type
          */
         public function getTakenTest(){
            $criteria=new CDbCriteria;
            $criteria->with=('takenBy_to_test');  // only select the 'title' column
            $criteria->condition='id_user = :user';
            $criteria->order='date_taken DESC';
            $criteria->params=array(':user'=> user()->id);
            
             
         return new CActiveDataProvider(get_class($this), array(
        'criteria' => $criteria,
        'pagination' => array(
            'pageSize' => 5,
        ),
    ));
         }
         public static function getpractice(){
              $user= user()->id;
             $dbCommand = Yii::app()->db->createCommand("
                SELECT * FROM `takepractice` 
                JOIN  `tests` ON test_id= id_test
                where user_id = $user
               ")->query();
             $pre= 0;
            
             $cnt=1;
             $arCount=0;
             $arr=array();
             foreach ($dbCommand as $m){
                 if($pre!=$m['id_test']){
                     $cnt=1;
                     $pre=$m['id_test'];
                     $arCount++;
                     $arr[$arCount]['test_name']=$m['test_name'];
                     $arr[$arCount]['count']=$cnt;
                     $cnt++;
                 }
                 else{
                     $arr[$arCount]['count']=$cnt;
                     $cnt++;
                 }
               
                 
             }
               return  $arr;
             
             
         }
         public static function getTestPercent($totalscore, $question){
             
             return ($totalscore/$question)*100;
         }
          public static function getAverageScore(){
             $criteria=new CDbCriteria;
            $criteria->with=('takenBy_to_test');  // only select the 'title' column
            $criteria->condition='id_user = :user';
            $criteria->order='date_taken DESC';
            $criteria->params=array(':user'=> user()->id);
            $test= TestTakenBy::model()->findAll($criteria);
            $cnt=0;
            $total=0;
            foreach ($test as $t) {
                if($t->total_question!=0){
                   $total +=$t->total_score;
                   $cnt+=$t->total_question;
                }
            }
            if($cnt==0)
                return false;
             return ($total/$cnt)*100;
         }
}
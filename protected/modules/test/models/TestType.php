<?php

/**
 * This is the model class for table "test_type".
 *
 * The followings are the available columns in table 'test_type':
 * @property integer $id_tt
 * @property string $name
 * @property string $description
 * @property integer $tt_createdby
 */
class TestType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TestType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function behaviors()
        {
            return array(
                
                
                array(
                    'class'=>'ext.seo.components.SeoRecordBehavior',
                    'route'=>'test/testtype/view',
                    'params'=>array('id'=>$this->id_tt, 'name'=>$this->name),
                ),
            );
        }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'test_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tt_createdby', 'required'),
			array('tt_createdby,option,isMultiChoice','numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('description', 'length', 'max'=>100),
                        
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_tt, name, description,option,isMultiChoice, tt_createdby', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'tt_to_test' => array(self::HAS_MANY, 'Tests', 'test_type'),
               
                    'tt_to_user' => array(self::BELONGS_TO, 'YumUser', 'tt_createdby'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tt' => 'Id Tt',
			'name' => 'Name',
                        'option'=>'Option',
                        'isMultiChoice'=>'Multi Answer Question',
			'description' => 'Description',
                       
			'tt_createdby' => 'Tt Createdby',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tt',$this->id_tt);
		$criteria->compare('name',$this->name,true);
                $criteria->compare('option',$this->option);
                $criteria->compare('option',$this->isMultiChoice);
		$criteria->compare('description',$this->description,true);
               
		$criteria->compare('tt_createdby',$this->tt_createdby);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public static function getTypeList(){
            $data= TestType::model()->findAll();
            $ar=array();
            foreach ($data as $d){
                $ar[$d->id_tt]=$d->name.'('.TestType::returnOptionName($d->option).')';
            }
             return $ar;
        }
        
        public static function  returnOptionName($in){
            switch ($in):
            case 1: 
                return 'Objective';
                
            case 2:
                return 'Theory';
            case 3:
                return 'Subjective';
            default :
                return null;
            endswitch;
        }
        public static function getOption(){
            return array(
                '1'=>'Objective',
                '2'=>'Theory',
                '3'=>'Subjective'
            );
        }
}
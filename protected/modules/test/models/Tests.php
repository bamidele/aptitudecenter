<?php

/**
 * This is the model class for table "tests".
 *
 * The followings are the available columns in table 'tests':
 * @property integer $id_test
 * @property integer $test_type
 * @property string $test_name
 * @property string $description
 * @property integer $timestaken
 * @property integer $totalscores
 * @property string $average_score
 * @property integer $date_added
 * @property integer $test_added_by
 */
class Tests extends CActiveRecord
{
        public $isEnabled;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tests the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tests';
	}
        
        public function behaviors()
        {
            return array(
                array(
                    'class'=>'ext.seo.components.SeoRecordBehavior',
                    'route'=>'test/tests/details',
                    'params'=>array('name'=>str_replace(" ", "-", $this->test_name),'id'=>$this->id_test),
                ),
            );
            
        }
        public function scopes()
        {
            return array(
                'published'=>array(
                    'condition'=>'t.publish=1',
                ),
            );
        }
        
        public function recently($limit=10)
        {
            $this->getDbCriteria()->mergeWith(array(
                'order'=>'date_added DESC',
                'limit'=>$limit,
            ));
            return $this;
        }
         public function popular($limit=10)
            {
                $this->getDbCriteria()->mergeWith(array(
                    'order'=>'timestaken DESC',
                    'limit'=>$limit,
                ));
                return $this;
            }
        
        public function topfree($limit=10)
        {
            $this->getDbCriteria()->mergeWith(array(
                'condition'=>'isfree=1',
                'order'=>'timestaken DESC',
                'limit'=>$limit,
            ));
            return $this;
        }
        public function suggest($id=NULL,$limit=10){
            
            $this->getDbCriteria()->mergeWith(array(
                'condition'=>'isfree=1',
                'order'=>'timestaken DESC',
                'limit'=>$limit,
            ));
            return $this;
        }
	/**
	 * @return array validation rules for model attributes.
	 */
        
        
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('test_type, test_name, test_added_by,duration,num_of_questions,num_of_options,year', 'required'),
			array('test_type, timestaken, totalscores, date_added, test_added_by, duration,num_of_questions,num_of_options,year,date_modified', 'numerical', 'integerOnly'=>true),
			array('test_name', 'length', 'max'=>100),
			
			array('average_score', 'length', 'max'=>5),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_test, test_type, test_name, description, timestaken,date_modified, totalscores, average_score, year,date_added,publish, test_added_by, duration,num_of_questions, num_of_options', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'test_to_question' => array(self::HAS_MANY, 'Questions', 'test_id'),
                    'test_to_tt' => array(self::BELONGS_TO, 'TestType', 'test_type'),
                    'test_to_categories' => array(self::MANY_MANY, 'Testcategory', 'cat_test(test_id,cat_id)'),
                    'test_to_user' => array(self::BELONGS_TO, 'YumUser', 'test_added_by'),
                    'test_to_takenBy'=>array(self::HAS_MANY,'TestTakenBy','id_test'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_test' => 'Id Test',
			'test_type' => 'Test Type',
			'test_name' => 'Test Name',
			'description' => 'Description',
			'timestaken' => 'Timestaken',
			'totalscores' => 'Totalscores',
			'average_score' => 'Average Score',
                        'num_of_questions'=>'Number of Question',
                        'num_of_options'=>'Number of Options',
                        'Duration'=>'Duration (Minutes)',
			'date_added' => 'Date Added',
			'test_added_by' => 'Test Added By',
                        'year'=>'Year of Exam',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_test',$this->id_test);
		$criteria->compare('test_type',$this->test_type);
		$criteria->compare('test_name',$this->test_name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('timestaken',$this->timestaken);
		$criteria->compare('totalscores',$this->totalscores);
                $criteria->compare('duration',$this->duration);
                $criteria->compare('num_of_questions',$this->num_of_questions);
                $criteria->compare('num_of_options',$this->num_of_options);
                $criteria->compare('year',$this->year);
		$criteria->compare('average_score',$this->average_score,true);
		$criteria->compare('date_added',$this->date_added);
                $criteria->compare('free',$this->isfree);
		$criteria->compare('test_added_by',$this->test_added_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public  static function getAnswer($id){
            $model = Tests::model()->findByPk($id);
            $options= $model->num_of_options;
            $arr= array();
            for($i=1;$i<=$options; $i++){
                if($i==1)
                    $arr[$i]='Option A';
                if($i==2)
                    $arr[$i]='Option B';
                if($i==3)
                    $arr[$i]='Option C';
                if($i==4)
                    $arr[$i]='Option D';
                if($i==5)
                    $arr[$i]='Option E';

            }
            return $arr;
        }
               
                
        public static function addTestToCategory($test_id, $cat_id)
            {

               $dbCommand = Yii::app()->db->createCommand("
                  INSERT INTO `cat_test` (`cat_id`, `test_id`)
                     VALUES ({$cat_id},{$test_id})
               ")->execute();

      
            }
            
          
          public static function getTestToCat($id){
            
          

               $dbReader = Yii::app()->db->createCommand("
                SELECT cat.cat_name, cat.level
                FROM testcategory cat
                JOIN cat_test ct ON cat.cat_id = ct.cat_id
                WHERE ct.test_id =$id
               ")->query();
                 
              
                $arr=array();
                // using foreach to traverse through every row of data
                foreach($dbReader as $row) {
                    
                   $arr[]=$row['cat_name'];
                    /**
                     * if($row['level']==1)
                     
                        $arr[]=$row['cat_name'];
                if($row['level']==2)
                    $arr[]='-'.$row['cat_name'];
                if($row['level']==3)
                    $arr[]='--'.$row['cat_name'];
                if($row['level']==4)
                    $arr[]='---'.$row['cat_name'];
                    */
                }
               $str = implode (", ", $arr);
               
               return $str;
            
              
          }
          public static function getCatID($id){
              $dbReader = Yii::app()->db->createCommand("
                SELECT *
                FROM cat_test
                
                WHERE test_id=$id
               ")->query();
              foreach ($dbReader as $d){
                  $arr[]=$d; 
              }
              return $arr;
          }
          public static function getTestwithCat($id){
              $dbReader = Yii::app()->db->createCommand("
                SELECT *
                FROM tests t
                JOIN cat_test ct ON t.id_test= ct.test_id
                JOIN testcategory tt ON ct.cat_id= tt.cat_id
                WHERE t.id_test =$id
               ")->query();
              foreach($dbReader as $d)
              {
                  $ar[]= $d;
              }
              return $ar;
          }
          public function getIsEnabled(){
            if($this->publish){
                $this->isEnabled= 'yes';
            }
            else{
                $this->isEnabled= 'no';
            }
          }
          
         
         public function beforeDelete()
         {
                
                
                $criteria=new CDbCriteria;
                 $criteria->condition = 'test_id = :test_id';
                
                $criteria->params = array('test_id' =>$this->id_test);
                
                $questions= Questions::model()->findAll($criteria); //using the shows relation
                foreach($questions as $q) 
                {
                    
                   $q->delete();
                }
                
               /*
               /*
               $dbReader = Yii::app()->db->createCommand("
                DELETE FROM questions WHERE test_id=$this->id_test
               ")->query();
                */
                $dbReader = Yii::app()->db->createCommand("
                DELETE FROM cat_test WHERE test_id=$this->id_test
               ")->query();
               //Questions::model()->deleteAll('test_id = '.$this->id_test); 
               return parent::beforeDelete();
             

        }
        public function getFree()
        {
            if($this->isfree==1)
                return 'Yes';
            else
                return 'No';
        }
      
        
}
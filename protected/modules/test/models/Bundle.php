<?php

/**
 * This is the model class for table "bundle".
 *
 * The followings are the available columns in table 'bundle':
 * @property integer $idbundle
 * @property string $name
 * @property string $description
 * @property integer $isfree
 * @property integer $date_created
 *
 * The followings are the available model relations:
 * @property BundleTest[] $bundleTests
 */
class Bundle extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bundle';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('isfree, date_created', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
                        array('price', 'numerical'),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idbundle, name, description, isfree,price, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bundleTests' => array(self::HAS_MANY, 'BundleTest', 'bundle_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idbundle' => 'Idbundle',
			'name' => 'Name',
			'description' => 'Description',
			'isfree' => 'Isfree',
                        'price'=>'Price',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idbundle',$this->idbundle);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('isfree',$this->isfree);
                $criteria->compare('price',$this->price);
		$criteria->compare('date_created',$this->date_created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bundle the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function addTestToBundle($bid, $tid){
             $dbCommand = Yii::app()->db->createCommand("
                  INSERT INTO `bundle_test` (`test_id`, `bundle_id`)
                     VALUES ({$tid},{$bid})
               ")->execute();
        }
        
        public static function getSubscribed($userid, $bundleid){
            $dbCommand = Yii::app()->db->createCommand("
                  INSERT INTO `bundle_user` (`user_id`, `bundle_id`)
                     VALUES ({$userid},{$bundleid})
               ")->execute();
              
              
        }
         public  static function checkSub($userid, $bundleid)
         {
             $dbCommand = Yii::app()->db->createCommand("
                  SELECT * FROM  bundle_user where user_id= $userid and bundle_id=$bundleid 
               ")->execute();
             if($dbCommand)
                 return FALSE;
             else
                 return true;
         }
         public  static function checkTrans($userid,$bundleid, $paymentid)
         {
             $dbCommand = Yii::app()->db->createCommand("
                  SELECT * FROM  bundle_transaction where user_id= $userid and bundle_id=$bundleid  and payment_id=$paymentid
               ")->execute();
             if($dbCommand)
                 return FALSE;
             else
                 return true;
         }
        public function beforeValidate() {
		if($this->isNewRecord) {	
			$this->date_created = time();
                      
                                
                }

		return parent::beforeValidate();
	}
        public static function testNotInBundle($bid)
        {
            $dbCommand = Yii::app()->db->createCommand("
                  SELECT * FROM   tests LEFT OUTER JOIN bundle_test ON test_id = id_test 
                  WHERE bundle_id is null or not bundle_id =$bid
               ")->queryAll();
            return $dbCommand;
        }
        public static function testWithBundle($bid){
            $dbCommand = Yii::app()->db->createCommand("
                  SELECT * FROM   tests  JOIN bundle_test ON test_id = id_test WHERE bundle_id =$bid
               ")->queryAll();
            return $dbCommand;
        }
        
        /*
         *  get the bundle that  the user is not subscribed to
         */
        public static function getUnsubcribed()
        {
            $id= user()->id;
            $dbCommand = Yii::app()->db->createCommand("
                  SELECT  idbundle, concat(name,' (', price ,')') nameprice   from bundle 
                    LEFT OUTER JOIN bundle_user ON bundle_user.bundle_id = bundle.idbundle 
                    WHERE  isfree= 0 and bundle_user.user_id is null or bundle_user.user_id != $id  group by idbundle
               ")->queryAll();
            return $dbCommand;
        }
        public static function deleteBundle_test($id)
        {
            $dbCommand = Yii::app()->db->createCommand("
                  DELETE FROM bundle_test WHERE  bundle_id = $id
               ")->execute();
        }
        public function getNameprice()
        {
            return $this->name.'(N'.$this->price.')';
        }
        public function getNamewithdesc()
        {
            return '<b>'.$this->name.'</b>'.TbHtml::small("(".$this->description.")");
        }
         public function getFormatedprice()
        {
            return 'N'.$this->price.'';
        }
        public static  function getSubBundle($id){
            $dbCommand = Yii::app()->db->createCommand("
               SELECT * FROM  bundle 
                    JOIN bundle_user ON bundle_user.bundle_id = bundle.idbundle 
                    WHERE bundle_user.user_id = 3   group by idbundle
               ")->queryAll();
            return $dbCommand;
        }
        public static  function getFreeBundle(){
            $dbCommand = Yii::app()->db->createCommand("
               SELECT * FROM  bundle  WHERE  bundle.isfree= 1
               ")->queryAll();
            return $dbCommand;
        }
         public  static function getPremium($uid=null)
         {
             if($uid==null){
             $dbCommand = Yii::app()->db->createCommand("
               SELECT * FROM  bundle 
				LEFT OUTER JOIN bundle_user ON bundle_user.bundle_id = bundle.idbundle 
				
                    WHERE isfree =0  
               ")->queryAll();
            return $dbCommand;
             }
             else{
                  $dbCommand = Yii::app()->db->createCommand("
                SELECT * FROM  bundle 
				LEFT OUTER JOIN bundle_user ON bundle_user.bundle_id = bundle.idbundle 
				
                    WHERE isfree =0  AND (user_id is null or user_id=$uid)				group by idbundle
               ")->queryAll();
            return $dbCommand;
             }
             
         }
        public  static function getFree()
         {
             if(user()->id==null){
             $dbCommand = Yii::app()->db->createCommand("
              SELECT * FROM  bundle 
				LEFT OUTER JOIN bundle_user ON bundle_user.bundle_id = bundle.idbundle 
				
                    WHERE isfree =1
               ")->queryAll();
            return $dbCommand;
             }
             else{
                 $uid= user()->id;
                  $dbCommand = Yii::app()->db->createCommand("
              SELECT * FROM  bundle 
				LEFT OUTER JOIN bundle_user ON bundle_user.bundle_id = bundle.idbundle 
				
                    WHERE isfree =1  AND (user_id is null or user_id=$uid)				group by idbundle

               ")->queryAll();
            return $dbCommand;
             }
             
         }       
}


<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
        'links'=>array_merge(
            $model->thread->getBreadcrumbs(true),
            array('Edit post')
        ),
    ));
?>
<div class="row-fluid">
<div class="form" style="margin:20px;">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'post-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
	),
    )); ?>

        <div class="row">
            <?php echo $form->labelEx($model,'content'); ?>
            <div class="row-fluid">
                       
                        <?php 
                        $attribute = 'content';
                        $value = (isset($model->content))? $model->content:'';
                        $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>'content',
                        'value'=>$value,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attribute
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attribute
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attribute
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            
                        ),
           
                    )
                    );
                ?> 
            
           </div>
        </div>
        <br><br>
        <div class="row buttons">
            <?php echo TbHtml::submitButton('Submit', array('color'=>TbHtml::BUTTON_COLOR_PRIMARY)); ?>
        </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->
</div>
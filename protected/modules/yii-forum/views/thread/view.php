<?php

$this->widget('zii.widgets.CBreadcrumbs', array('links'=>$thread->getBreadcrumbs()));

$header = '<div class="preheader"><div class="preheaderinner">'. CHtml::encode($thread->subject) .'</div></div>';
$footer = $thread->is_locked?'':'<div class="footer">'. TbHtml::button('<i class="icon-reply icon-large"></i> Reply This Topic',
          
        array(
                'id'=>'replybtn',
                'onClick'=>'js:$("#reply").slideToggle("slow");
                            $("#replybtn").slideToggle("slow");
                            ',
        )
        ) .'</div>';
?>

<?php
    $this->widget('zii.widgets.CListView', array(
        //'htmlOptions'=>array('class'=>'thread-view'),
        'dataProvider'=>$postsProvider,
        'template'=>'{summary}{pager}'. $header .'{items}{pager}'. $footer,
        'itemView'=>'_post',
        'htmlOptions'=>array(
            'class'=>Yii::app()->controller->module->forumListviewClass,
        ),
    ));

 ?>
<div class="row-fluid" style="display: none" id="reply">
   <div class="panel panel-default">
    <div class="panel-heading">
    <p> Reply This Topic</p> 
    </div>
     <div class="panel-body">
         <div id="inner_error"></div>
    <div class="form">
    <?php echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_HORIZONTAL,'',
        'post',array('id'=>'post')) ?>

    <?php if(isset($forum)): ?>
        
        <?php
        $con='';
        if($model->subject!=null){
            $con= $model->subject;
        }
        echo TbHtml::textField('subject',$con, array('placeholder' => 'Type Subject ...')); ?>
        <?php// echo TbHtml::checkBox('checkMeOut', false, array('label' => 'Check me out')); ?>
        
    <?php endif; ?>
           <div class="row-fluid">
                       
                        <?php 
                        $attribute = 'content';
                        $value = (isset($model->content))? $model->content:'';
                        $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>'content',
                        'value'=>$value,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attribute
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attribute
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attribute
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            
                        ),
           
                    )
                    );
                ?> 
            
           </div>
        

        <?php if(Yii::app()->user->isAdmin()): ?>
            <div class="row-fluid rememberMe">
                
                <?php echo TbHtml::checkBox('lockthread', false, array('label' => 'Lock thread')); ?>
                <?php // echo $form->error($model,'lockthread'); ?>
            </div>
        <?php endif; ?>

        <div class="row-fluid buttons">
      
            <?php echo TbHtml::submitButton('<i class="icon-reply icon-large"></i> Reply ',array('color' => TbHtml::BUTTON_COLOR_PRIMARY,'id'=>'idSubmit')); ?>
        </div>
     <?php echo TbHtml::endForm(); ?>
    </div>
</div>
</div>
</div>

<script>
    $(document).ready(function(){
        $('#idSubmit').each(function(){
                        $(this).click(function(ev){
                                ev.preventDefault();
                               
                                $.ajax({
                                        "type":"POST",
                                        "url":"<?php echo $this->createUrl("/forum/thread/newreply1",array('id'=>$thread->id)); ?>",
                                        "data":$('#post').serialize(),
                                        "dataType": "json",
                                        "success":function(data){
                                        if(data.error){
                                            var s= '<ul>'+data.error+'</ul>';
                                           $("#inner_error").append(s).show();
                                           
                                        }else{
                                           
                                            var ms=data.passage;
                                           
                                             
                                            $("#inner_error").html(ms).show();
                                            
                                            location.reload();
                                            
                                            $("#inner_error").hide();
                                        }
                                        },
                                        
                                        });
                               
                            
                                });
                });
        });
</script>

   <div id="inner_error"></div>

    <?php echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_HORIZONTAL,'',
        'post',array('id'=>'post')) ?>

    <?php if(isset($forum)): ?>
        
        <?php
        $con='';
        if($model->subject!=null){
            $con= $model->subject;
        }
        echo TbHtml::textField('subject',$con, array('placeholder' => 'Type Subject ...')); ?>
        <?php// echo TbHtml::checkBox('checkMeOut', false, array('label' => 'Check me out')); ?>
        
    <?php endif; ?>
           <div class="row-fluid">
                       
                        <?php 
                        $attribute = 'content';
                        $value = (isset($model->content))? $model->content:'';
                        $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>'content',
                        'value'=>$value,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attribute
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attribute
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attribute
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            
                        ),
           
                    )
                    );
                ?> 
            
           </div>
        

        <?php if(Yii::app()->user->isAdmin()): ?>
            <div class="row-fluid rememberMe">
                
                <?php echo TbHtml::checkBox('lockthread', false, array('label' => 'Lock thread')); ?>
                <?php // echo $form->error($model,'lockthread'); ?>
            </div>
        <?php endif; ?>

        <div class="row-fluid buttons">
      
            <?php echo TbHtml::submitButton('<i class="icon-reply icon-large"></i> Create Topic',array('color' => TbHtml::BUTTON_COLOR_PRIMARY,'id'=>'idtopic')); ?>
        </div>
     <?php echo TbHtml::endForm(); ?>
    
<script>
    $(document).ready(function(){
        $('#idtopic').each(function(){
                        $(this).click(function(ev){
                                ev.preventDefault();
                               
                                $.ajax({
                                        "type":"POST",
                                        "url":"<?php echo $this->createUrl("/forum/thread/create",array('id'=>$forum->id)); ?>",
                                        "data":$('#post').serialize(),
                                        "dataType": "json",
                                        "success":function(data){
                                        if(data.error){
                                            var s= '<ul>'+data.error+'</ul>';
                                           $("#inner_error").append(s).show();
                                           
                                        }else{
                                           
                                            var ms=data.passage;
                                           
                                             
                                            $("#inner_error").html(ms).show();
                                            
                                            location.reload();
                                            
                                            $("#inner_error").hide();
                                        }
                                        },
                                        
                                        });
                               
                            
                                });
                });
        });
</script>

<?php
if(!$profile = $model->profile)
	$profile = new YumProfile;
$this->pageTitle = app()->name.' - '.Yum::t('Profile');

$this->breadcrumbs = array(Yum::t('Profile'),$model->username);
Yum::renderFlash();
?>

<div id="profile">
<div class="row-fluid shadowbox">
<div class="span4"><?php echo $model->getAvatar(); ?></div>
<div class="span8"><?php $this->renderPartial(Yum::module('profile')->publicFieldsView, array(
			'profile' => $model->profile));
if(!Yii::app()->user->isGuest && Yii::app()->user->id == $model->id) {
	echo CHtml::link(Yum::t('Edit profile'), array('//profile/profile/update'));
	echo '&nbsp;';
	echo CHtml::link(Yum::t('Upload avatar image'), array('//avatar/avatar/editAvatar'));
}
?>
  </div>  
<br />
</div>
<?php
if(Yum::hasModule('friendship'))
 $this->renderPartial(
		'application.modules.friendship.views.friendship.friends', array(
			'model' => $model)); ?>
<br />
<?php

if(@Yum::module('message')->messageSystem != 0)
$this->renderPartial('/message/write_a_message', array(
			'model' => $model)); ?>
<br />
<?php
if(Yum::module('profile')->enableProfileComments
		&& Yii::app()->controller->action->id != 'update'
		&& isset($model->profile))
	$this->renderPartial(Yum::module('profile')->profileCommentIndexView, array(
			 'model' => $model->profile)); ?>
 </div>
<div class="row-fuild">
    
    <?php 
     if(TestTakenBy::getAverageScore()==false){
         echo TbHtml::lead('You havent taken any test or finished any test yet ');
     }
    else{
    echo TbHtml::lead('Your Overall Average Score of Finished is '.TestTakenBy::getAverageScore().''); ?>
    <?php echo TbHtml::animatedProgressBar(TestTakenBy::getAverageScore()); 
    }
    ?>
    
</div>
<div class="row-fuild">
    <?php $this->widget('bootstrap.widgets.TbGridView',array(
	'dataProvider'=>TestTakenBy::model()->getTakenTest(),
	'columns'=>array(
	array(
				'name'=>'date_taken',
				'filter' => false,
				'value'=>'date(UserModule::$dateFormat,$data->date_taken)',
	),
         'takenBy_to_test.test_name',
                    array(
        'name'=>'total_score',
        'type'=>'raw',
        'value'=>'
            ($data->total_question==0)?"Not completed":
           TbHtml::animatedProgressBar(TestTakenBy::getTestPercent($data->total_score, $data->takenBy_to_test->num_of_questions))."
           ".TestTakenBy::getTestPercent($data->total_score, $data->takenBy_to_test->num_of_questions)."%"'
        ),
                    
	
	),
)); ?>
    
</div>
<div class="row-fluid"><?php 
    $new=Tests::model()->published()->recently(10)->findAll();
              $mostpopular=Tests::model()->published()->popular(10)->findAll();
              ?>
    <div class="span6">
<div class="panel panel-default">
  
  <div class="panel-heading"><i class="icon-heart"></i>   Popular Test </div>
  <!-- List group -->
  <ul class="list-group">
      <?php  foreach ($mostpopular as $p){ ?>
      <li class="list-group-item"><i class="icon-li icon-heart"></i>
             <b><a href="<?php echo $this->createUrl('/test/tests/details',array('id'=>$p->id_test));?>">
             <?php echo $p->test_name ?> 
       </a></b></li>
      <?php }?>
   
 
  </ul>
</div></div>
        <div class="span6">
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><label class="label"> New</label> Newly Added</div>
 

  <!-- List group -->
  <ul class="list-group">
      <?php  foreach ($new as $n){ ?>
      <li class="list-group-item"><label class="label"> New</label>
             <b><a href="<?php echo $this->createUrl('/test/tests/details',array('id'=>$n->id_test));?>">
             <?php echo $n->test_name ?> 
       </a></b></li>
      <?php }?>
   
 
  </ul>
</div></div>
</div>
 


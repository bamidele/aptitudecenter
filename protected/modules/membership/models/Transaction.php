<?php

/**
 * This is the model class for table "transaction".
 *
 * The followings are the available columns in table 'transaction':
 * @property integer $tid
 * @property string $trans_id
 * @property integer $user_id
 * @property integer $membership_id
 * @property integer $payment_id
 * @property integer $order_date
 * @property integer $status
 */
class Transaction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transaction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, membership_id, payment_id, order_date, status', 'required'),
			array('user_id, membership_id, payment_id, order_date, status', 'numerical', 'integerOnly'=>true),
			array('trans_id', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('tid, trans_id, user_id, membership_id, payment_id, order_date, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tid' => 'Tid',
			'trans_id' => 'Trans',
			'user_id' => 'User',
			'membership_id' => 'Membership',
			'payment_id' => 'Payment',
			'order_date' => 'Order Date',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tid',$this->tid);
		$criteria->compare('trans_id',$this->trans_id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('membership_id',$this->membership_id);
		$criteria->compare('payment_id',$this->payment_id);
		$criteria->compare('order_date',$this->order_date);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function beforeValidate() {
		if($this->isNewRecord) {
			$this->user_id = Yii::app()->user->id;		
			$this->order_date = time();
                        $this->status =	0;
                                
                }

		return parent::beforeValidate();
	}
        public function getStatus()
        {
            if($this->status==0)
                return "Pending";
            else if($this->status==1)
                return "successful";
            else if($this->status==2)
                return "pending";
        }
        
}

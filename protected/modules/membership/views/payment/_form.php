<div class="form">
        <p class="note">
        <?php echo Yum::requiredFieldNote(); ?>
        </p>

        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'payment-form',
                'enableAjaxValidation'=>false,
                )); 
                echo $form->errorSummary($model);
        ?>
        <?php echo $form->textFieldControlGroup($model,'title',array('size'=>60,'maxlength'=>255)); ?>
        
        <?php 
                    $attribute = 'text';
                    $value = (isset($model->text))? $model->text:'';
          ?>
                        <div class="row-fluid">
                        <div class="span2">
                            <label> Question :</label>
                        </div>
                        </div>
                        <div class="row-fluid">
                        
                        <div class="span9" style="min-height: 100px;">
                        <?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
                        'name'=>'text',
                        'value'=>$value,
                        'pluginOptions' => array(

                            'plugins' => array('fullscreen', 'fontsize', 'fontfamily'),
                            'fileUpload' => Yii::app()->createUrl('test/passages/fileUpload', array(
                                'attr' => $attribute
                            )),
                            'fileUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'imageUpload' => Yii::app()->createUrl('test/passages/imageUpload', array(
                                'attr' => $attribute
                            )),
                            'imageGetJson' => Yii::app()->createUrl('test/passages/imageList', array(
                                'attr' => $attribute
                            )),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                        ),

                    ));
                ?> 
                            </div>
                    </div>
   
        <div class="form-actions">
                <?php 
                echo TbHtml::Button(Yum::t('Cancel'), array(
			'submit' => array('payment/admin'),'size'=>TbHtml::BUTTON_SIZE_LARGE,));
                echo TbHtml::submitButton($model->isNewRecord ? Yum::t('Create payment type')
		:Yum::t('Save payment type'),array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
        </div>

        <?php           $this->endWidget(); ?>
</div> <!-- form -->

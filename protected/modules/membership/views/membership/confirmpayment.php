<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php $this->breadcrumbs = array(Yum::t('Payment'));?>

<?php $this->title = Yum::t('Payment'); ?>
         <div class ="row-fluid">
            <?php echo TbHtml::lead('Aptitudecenter Membership Payment'); ?>
            </div>
	<div class="row-fluid">
           
            <div class="span5 well">
               <div class="row-fluid ">
                   <h4>The Benefits of using Aptitude Center</h4>
                    <p> 
                    <ul class="icons-ul">
                    <li> <i class="icon-li icon-ok-sign icon-2x"></i>Just like the real thing</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i> Timed test simulations</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Instant online access 24/7</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Free access to updates</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Clear worked solutions</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Compare your performance</li>
                    </ul>
                    </p>
                 </div>
            </div>
             <div class="span7 shadowbox" style="padding: 10px;">
                 <div class="row-fluid">
                 <?php echo TbHtml::lead(' Payment Preview '); ?>
                </div>
                 
                    
         
                 
                 <div class="row-fluid">
		<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'payment',
			'enableAjaxValidation'=>false,
			)); 
			//echo $form->errorSummary($model);
		?>
   
                     <?php echo CHtml::hiddenField('confirm',1);?>
                     
                     <table>
                         <tbody>
                             <tr>
                                 <td>Name : </td>
                                 <td><?php  echo $user->profile->name;?></td>
                             </tr>
                             <tr>
                                 <td>Membership Type :</td>
                                 <td> <?php  echo $role->description;?></td>
                             </tr>
                             <tr>
                                 <td>Duration:</td>
                                 <td> <?php echo $role->getDuration();?></td>
                             </tr>
                             <tr>
                                 <td>Payment Type</td>
                                 <td> <?php echo $payment->context; ?></td>
                             </tr>
                             <tr>
                                 <td>Amount :</td>
                                 <td> &#8358; <?php echo $role->price; ?></td>
                             </tr>
                         </tbody>
                     </table>

                     <?php  echo TbHtml::submitButton(Yum::t('Continue'),
                             array('size'=>TbHtml::BUTTON_SIZE_LARGE, 'color'=>  TbHtml::BUTTON_COLOR_DANGER));?>
             <?php $this->endWidget(); ?>
                     </div>
                
		
           </div>
	</div> <!-- form -->
       

<?php $this->breadcrumbs = array(Yum::t('Membership'));?>

<?php $this->title = Yum::t('Membership'); ?>
         <div class ="row-fluid">
            <?php echo TbHtml::lead('Aptitudecenter Membership Subscription'); ?>
            </div>
	<div class="row-fluid">
           
            <div class="span5 well">
               <div class="row-fluid ">
                   <h4>The Benefits of using Aptitude Center</h4>
                    <p> 
                    <ul class="icons-ul">
                    <li> <i class="icon-li icon-ok-sign icon-2x"></i>Just like the real thing</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i> Timed test simulations</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Instant online access 24/7</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Free access to updates</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Clear worked solutions</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Compare your performance</li>
                    </ul>
                    </p>
                 </div>
            </div>
             <div class="span7 shadowbox" style="padding:10px;" >
                 <div class="row-fluid">
                 <?php echo TbHtml::lead('Subscription Options'); ?>
                 <?php echo TbHtml::em('*Please choose one of this options', array('color' => TbHtml::TEXT_COLOR_INFO)); ?>
                 </div>
                <div class="row-fluid">
		<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'membership-has-company-form',
			'enableAjaxValidation'=>true,
			)); 
			echo $form->errorSummary($model);
		?>
           
                    <table>
			<?php echo CHtml::activeRadioButtonList($model, 'membership_id', 
			CHtml::listData(YumRole::model()->findAll('price != 0'), 'id', 'description')
                         ,array('template' => '<tr><td><p>{input}</p></td><td>{label}</td></tr>')
			);
			?>
                    </table>
                        <?php echo $form->error($model,'membership_id'); ?>

                         <?php  echo TbHtml::submitButton(Yum::t('Order Membership'),
                             array('size'=>TbHtml::BUTTON_SIZE_LARGE, 'color'=>  TbHtml::BUTTON_COLOR_DANGER));?>
                    <?php $this->endWidget(); ?>
		</div>        
           </div>
	</div> <!-- form -->
        </div>
	
		<?php
    Yii::app()->clientScript->registerScript('toggle', "
		$('#detail-information').hide();	  
		$('#more-information').click(function() {
	   	$('#detail-information').fadeToggle('slow');
	   });
    ");
    ?>
	

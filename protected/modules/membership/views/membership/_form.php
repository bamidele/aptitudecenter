<div class="form">
<p class="note">
<?php echo Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required');?>.
</p>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
'id'=>'membership-form',
	'enableAjaxValidation'=>true,
	)); 
	echo $form->errorSummary($model);
?>
	<div class="row-fluid">
            <?php echo $form->labelEx($model,'type'); ?>
            <?php echo TbHtml::radioButtonListControlGroup($model, 'type', CHtml::listData(
            YumRole::model()->findAll('price != 0'), 'id', 'title')); ?>
        </div>

<?php
echo TbHtml::Button(Yii::t('app', 'Cancel'), array(
			'submit' => array('membership/index'))); 
echo TbHtml::submitButton(Yum::t('Buy membership')); 
$this->endWidget(); ?>
</div> <!-- form -->

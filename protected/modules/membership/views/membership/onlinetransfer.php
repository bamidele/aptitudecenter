<?php $this->breadcrumbs = array(Yum::t('Payment Instruction- Online Transfer'));?>

<?php $this->title = Yum::t('Payment Instruction- Online Transfer '); ?>
         <div class ="row-fluid">
            <?php echo TbHtml::lead('Payment Instruction- Online Transfer'); ?>
            </div>
	<div class="row-fluid">
           
            <div class="span5 well">
               <div class="row-fluid ">
                   <h4>The Benefits of using Aptitude Center</h4>
                    <p> 
                    <ul class="icons-ul">
                    <li> <i class="icon-li icon-ok-sign icon-2x"></i>Just like the real thing</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i> Timed test simulations</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Instant online access 24/7</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Free access to updates</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Clear worked solutions</li>
                    <li><i class="icon-li icon-ok-sign icon-2x"></i>Compare your performance</li>
                    </ul>
                    </p>
                 </div>
            </div>
            <div class="span6 well">
                
                <div class="row-fluid">
                    <table>
                        
                        <tbody>
                            <tr>
                                <td> Name :</td>
                                <td><?php  echo $user->profile->name; ?></td>
                            </tr>
                            <tr>
                                <td> Transaction ID : </td>
                                <td> <?php  echo $trans->trans_id; ?></td>
                            </tr>
                            <tr>
                                <td> Description</td>
                                <td> &#8358; <?php  echo $mem->price; ?></td>
                            </tr>
                            <tr>
                                <td> Description</td>
                                <td> <?php  echo $mem->description; ?></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="row-fluid">
                    <?php TbHtml::em('How to do Online Transfer to this account'); ?>
                    
                </div>
            </div>
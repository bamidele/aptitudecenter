<?php
Yii::import('application.modules.user.controllers.YumController');
Yii::import('application.modules.user.models.Yum');
Yii::import('application.modules.role.models.*');
Yii::import('application.modules.membership.models.*');

class YumMembershipController extends YumController {
	public $defaultAction = 'index';

	public function accessRules() {
		return array(
                                array('allow', 
					'actions'=>array('successonline', 'failureonline'),
					'users'=>array('*'),
					),
				array('allow', 
					'actions'=>array('index', 'order','payment', 'extend','preview','atm','doonlinetransfer',
                                            'confirmtrans','getonlinepayment'),
					'users'=>array('@'),
					),
				array('allow', 
					'actions'=>array('admin','delete', 'update', 'view', 'orders'),
					'expression' => 'Yii::app()->user->isAdmin()',
					),
				);
	}

	public function actionView()
	{
		return false;
	}

	public function actionExtend() {
		$membership = YumMembership::model()->findByPk($_POST['membership_id']);
		if(!$membership)
			throw new CHttpException(404);

		if($membership->user_id != Yii::app()->user->id)
			throw new CHttpException(403);

		$subscription = $_POST['subscription'];
		$membership->subscribed = $subscription == 'cancel' ? -1 : $subscription;
		$membership->save(false, array('subscribed'));
		Yum::setFlash('Your subscription setting has been saved');
		$this->redirect(Yum::module('membership')->membershipIndexRoute);
	}

	public function actionUpdate($id = null) {
		if($id !== null) 
			$model = YumMembership::model()->findByPk($id);

		if(isset($_POST['YumMembership'])) {
			$model = YumMembership::model()->findByPk($_POST['YumMembership']['id']); 
			$model->attributes = $_POST['YumMembership'];
			if($model->confirmPayment()) 
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
					'model'=>$model,
					));
	}
        
	public function actionOrder()
	{
                $this->layout= 'application.views.layouts.main';
		$model = new YumMembership;

		if(isset($_POST['YumMembership'])) {
                        
			$model->attributes = $_POST['YumMembership'];
                        //print_r($_POST['YumMembership']);
                           $this->redirect(array('payment', 'mem_id'=>$_POST['YumMembership']['membership_id']));
                      
			/*
                        if($model->save()) {
				$this->redirect(array('index'));
			}*/
		}

		$this->render('order',array( 'model'=>$model));
	}
        /**
         *  method  for payement action 
         * @param int $membership_id Use the membership_id to load the choosing membership subscription
         */
        public function actionPayment(){
            $this->layout= 'application.views.layouts.main';
            $model = new YumPayment;
            $membership= $_GET['mem_id'];
                    
            if(isset($_POST['membership_id'])) {
                        
                //$model->attributes = $_POST['payment'];
                // var_dump($_POST['YumPayment']);
                 //print_r($_POST['membership_id']);
                 $this->redirect(array('preview', 'mem_id'=>$_POST['membership_id'], 'payment_id'=>$_POST['YumPayment']['id']));
                
                
               // $this->_doTransaction($_POST['membership_id'],$_POST['YumPayment']);
            }
            $this->render('payment',array( 'model'=>$model, 'membership_id'=>$membership));
        }
        public function actionPreview(){
            $this->layout= 'application.views.layouts.main';
            $model = new YumPayment;
            $membership= $_GET['mem_id'];
            $payment_id= $_GET ['payment_id'];  
            $role= YumRole::model()->findByPk($membership);
            $user= YumUser::model()->findByPk(user()->id);
            $payment = YumPayment::model()->findByPk($payment_id);
            
            if(isset($_POST['confirm'])) {
                 switch ($payment_id){
               case 3:
                    $this->redirect(array('doonlinepayment','mem'=> $membership));
                    break;
                case 4:
                    $this->redirect(array('dofundstransfer','mem'=> $membership));
                    break;
          
                case 6:
                   $this->redirect(array('dobankdeposit','mem'=> $membership));
                   break;
                
            }
                
            }
            $this->render('confirmpayment',array( 'role'=>$role, 'user'=>$user, 'payment'=>$payment));
        }
        /**
         *  Deletes a memebership
         * @throws CHttpException
         */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			$this->loadModel()->delete();

			if(!isset($_GET['ajax']))
			{
				if(isset($_POST['returnUrl']))
					$this->redirect($_POST['returnUrl']); 
				else
					$this->redirect(array('admin'));
			}
		}
		else
			throw new CHttpException(400,
					Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('YumMembership', array(
					'criteria' => array(
						'condition' => 'user_id = '.Yii::app()->user->id),
					));

		$this->render('index',array(
					'dataProvider'=>$dataProvider,
					));
	}

	public function actionOrders()
	{
		$model=new YumMembership('search');
		$model->unsetAttributes();

		if(isset($_GET['YumMembership']))
			$model->attributes = $_GET['YumMembership'];

		$this->render('orders',array(
					'model'=>$model,
					));
	}

	public function actionAdmin() {
		$this->layout = Yum::module()->adminLayout;

		$model=new YumMembership('search');
		$model->unsetAttributes();

		if(isset($_GET['YumMembership']))
			$model->attributes = $_GET['YumMembership'];

		$this->render('admin',array(
					'model'=>$model,
					));
	}
       
        /**
         *  do the online payment method
         * @param type $mem
         */
        
        public function actionSuccessonline()
        {
            $mem= $_GET['mem'];
           
            $memtype= YumRole::model()->findByPk($mem);
            $this->render('successonline',array('mem'=>$memtype));
        }
        
        public function actionFailureonline()
        {
            $message= $_GET['message'];
            $this->render('failureonline',array('message'=>$message));
        }
        public function actionDoonlinepayment(){
            $mem= $_GET['mem'];
            $this->layout= 'application.views.layouts.main';
            $transaction= new Transaction();
            $memtype= YumRole::model()->findByPk($mem);
            $this->render('doonline',array('mem'=>$memtype));
            
        }
        public function actionGetonlinepayment()
        {
            //finish onlne ransaction
            if(isset($_POST['transaction_id'])){
                //get the full transaction details as an xml from voguepay
                $xml = file_get_contents('https://voguepay.com/?v_transaction_id='.$_POST['transaction_id']);
                //parse our new xml
                $xml_elements = new SimpleXMLElement($xml);
                //create new array to store our transaction detail
                $transaction = array();
                //loop through the $xml_elements and populate our $transaction array
                foreach($xml_elements as $key => $value) 
                {
                        $transaction[$key]=$value;
                }
                /*
                Now we have the following keys in our $transaction array
                $transaction['merchant_id'],
                $transaction['transaction_id'],
                $transaction['email'],
                $transaction['total'], 
                $transaction['merchant_ref'], 
                $transaction['memo'],
                $transaction['status'],
                $transaction['date'],
                $transaction['referrer'],
                $transaction['method']
                */
if($transaction['total'] == 0)
                  $this->redirect(array('failureonline','message'=>'Invalid total'));
                    
                if($transaction['status'] != 'Approved')
                    $this->redirect(array('failureonline','message'=>'Failed transaction'));
               
                    $trans= new Transaction();
                    
                    $memtype= YumRole::model()->find(array('condition'=>'title=:title',
    'params'=>array(':title'=>$transaction['memo']),));
                    $trans->trans_id= $transaction['transaction_id'];
                    $trans->payment_id= 3;
                    $trans->status=1;
                    $trans->membership_id= $memtype->id;
                    if($trans->save()){
                         $this->redirect(array('successonline','mem'=>$trans->membership_id));   
                    }  
                    else{
                        echo "not saved";
                    }
                }

       }
        /**
         * 
         */
        public function actionDoonlinetransfer($mem){
            $this->layout= 'application.views.layouts.main';
            $transaction= new Transaction();
           // $memtype= YumRole::model()->findByPk($mem);
            $transaction->trans_id= time().mt_rand(1000,9999);
            $transaction->payment_id= 5;
            $transaction->membership_id= $mem;
            if($transaction->save()){
               
                $memtype= YumRole::model()->findByPk($mem);
                $user= YumUser::model()->findByPk(user()->id);
                $this->render('onlinetransfer', array(
                'trans'=>$transaction,'mem'=>$memtype,'user'=>$user
                         ));
  
            } 
        }
        
        public function actionDofundstransfer(){
            $mem= $_GET['mem'];
            $this->layout= 'application.views.layouts.main';
            $transaction= new Transaction();
            
            $transaction->trans_id= time().mt_rand(1000,9999);
            $transaction->payment_id= 4;
            $transaction->membership_id= $mem;
            if($transaction->save()){
    
                $memtype= YumRole::model()->findByPk($mem);
                //$memtype= YumRole::model()->findByPk($mem);
                $user= YumUser::model()->findByPk(user()->id);
                $this->render('atm', array(
                'trans'=>$transaction,'mem'=>$memtype,'user'=>$user
                         ));
            } 
        }
      
       public function actionDobankdeposit(){
           $this->layout= 'application.views.layouts.main';
            $mem= $_GET['mem'];
            $transaction= new Transaction();
            $memtype= YumRole::model()->findByPk($mem);
            $transaction->trans_id= time().mt_rand(1000,9999);
            $transaction->payment_id= 6;
            $transaction->membership_id= $mem;
            if($transaction->save()){
                $memtype= YumRole::model()->findByPk($mem);
                //$memtype= YumRole::model()->findByPk($mem);
                $user= YumUser::model()->findByPk(user()->id);
                $this->render('bankdeposit', array(
                'trans'=>$transaction,'mem'=>$memtype,'user'=>$user
                         ));
            } 
        }
        public function actionConfirmtrans(){
            $trans= $_GET['trans_id'];
        }
        
}

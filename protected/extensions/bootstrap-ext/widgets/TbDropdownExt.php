<?php

Yii::import('zii.widgets.CMenu');

/**
 * Bootstrap dropdown menu.
 * @see http://twitter.github.com/bootstrap/javascript.html#dropdowns
 */
class TbDropdownExt extends CMenu
{
	public $view;
	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		parent::init();

		if (isset($this->htmlOptions['class']))
			$this->htmlOptions['class'] .= ' dropdown-menu';
		else
			$this->htmlOptions['class'] = 'dropdown-menu';
	}

	/**
	 * Renders the content of a menu item.
	 * Note that the container and the sub-menus are not rendered here.
	 * @param array $item the menu item to be rendered. Please see {@link items} on what data might be in the item.
	 * @return string the rendered item
	 */

	protected function renderMenu($items){
		echo CHtml::openTag('ul', $this->htmlOptions);
		$this->render($this->view);
		echo '</ul>';
	}
}

<?php
/* @var $this PassagesController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Passages',
);

$this->menu=array(
	array('label'=>'Create Passages','url'=>array('create')),
	array('label'=>'Manage Passages','url'=>array('admin')),
);
?>

<h1>Passages</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
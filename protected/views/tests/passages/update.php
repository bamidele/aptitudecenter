<?php
/* @var $this PassagesController */
/* @var $model Passages */
?>

<?php
$this->breadcrumbs=array(
	'Passages'=>array('index'),
	$model->idpassages=>array('view','id'=>$model->idpassages),
	'Update',
);

$this->menu=array(
	array('label'=>'List Passages', 'url'=>array('index')),
	array('label'=>'Create Passages', 'url'=>array('create')),
	array('label'=>'View Passages', 'url'=>array('view', 'id'=>$model->idpassages)),
	array('label'=>'Manage Passages', 'url'=>array('admin')),
);
?>

    <h1>Update Passages <?php echo $model->idpassages; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
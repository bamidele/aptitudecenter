<?php
/* @var $this PassagesController */
/* @var $model Passages */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'idpassages',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'qid',array('span'=>5)); ?>

                    <?php echo $form->textAreaControlGroup($model,'passage',array('rows'=>6,'span'=>8)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
/* @var $this PassagesController */
/* @var $data Passages */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('idpassages')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idpassages),array('view','id'=>$data->idpassages)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qid')); ?>:</b>
	<?php echo CHtml::encode($data->qid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('passage')); ?>:</b>
	<?php echo CHtml::encode($data->passage); ?>
	<br />


</div>
<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="row-fluid homeads">
<a class="statnotice" href="<?php echo url('/registration/registration')?>">
    <div class="shadowbox homeimg">
            <div class="span9" style="margin-left:0;">
               <?php 
               $questions= Questions::model()->count();
               $content= "Over <b>$questions</b> Aptitude Test Questions of <b>JAMB, WAEC, 
                   JOB, Aptitude Exams! </b>What are you waiting for? Hurry Up!!!,
                   <b>Sign Up</b> and study for your exams and get your Dream Job!";
               $content2= "Over $questions Aptitude Test Questions of JAMB, WAEC, 
                   JOB, Aptitude Exams! What are you waiting for? Hurry Up!!!,
                   Sign Up and study for your exams and get your Dream Job!";
               echo TbHtml::lead($content); ?>
            </div>
        <div class="span3 signup">
            <?php  echo TbHtml::button("Sign Up", array('url'=>array('/registration/registration'), 
                'color'=>  TbHtml::BUTTON_COLOR_SUCCESS, 'size'=>  TbHtml::BUTTON_SIZE_LARGE))?>
        </div>
        
  </div></a>
</div>
<!-- AddThis Button END -->
 
 <div class="row_fluid">
     <div class="span8">
         <div class="row-fluid">
               <h3>Welcome Message</h3>
                      
                     <p> <i class="icon-quote-left icon-4x pull-left icon-muted"></i>
           Welcome to an online platform for exams and tests preparation and practice. 
               It consists of a comprehensive database of various tests and exams. This includes
           Job Aptitude Tests, University Entry Exams, IQ tests,WAEC, JAMB, GMATS, GRE, and lots more. 
           It contain both free practice test and premium practice 
           aptitude tests used by recruiters and employers offering graduate & professional jobs. This online
           database will go a long way in helping  students, job seekers 
           be more aware of the kind of exams they are about to write as well as beating the competition.
           Just login or sign up to have a access to a database practice tests and worked examples!</p>
          
           <div class="addthis_toolbox addthis_default_style">
<a class="addthis_button_facebook_like" fb:like:layout="button_count" fb:like:href="http://aptitudecenter.com"
   addthis:title="Welcome Message"
   addthis:description="Welcome to an online platform for exams and tests preparation and practice. 
               It consists of a comprehensive database of various tests and exams. This includes
           Job Aptitude Tests, University Entry Exams, IQ tests,WAEC, JAMB, GMATS, GRE, and lots more. 
           It contain both free practice test and premium practice 
           aptitude tests used by recruiters and employers offering graduate & professional jobs. This online
           database will go a long way in helping  students, job seekers 
           be more aware of the kind of exams they are about to write as well as beating the competition.
           Just login or sign up to have a access to a database practice tests and worked examples!"></a>
<a class="addthis_button_tweet"></a>
<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
<a class="addthis_button_linkedin_counter"></a>

<!-- AddThis Button BEGIN -->


<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5231aaa22fdab04e"></script>
<!-- AddThis Button END -->
                </div>
                <div class="span12" style="display: none">
                    <div class="middle"> 
                        <img src="<?php echo baseUrl("images/first.png")?>" alt="Aptitude Test"/>
                      <h3><i class="icon-quote-left icon-4x pull-left icon-muted"></i>Aptitude Test </h3>
                      
            Numerical, Verbal & Diagrammatic reasoning tests are psychometric aptitude
            tests used by recruiters and employers offering graduate & professional 
            jobs. Practice Aptitude Tests.com offer industry standard employer
            psychometric tests designed for graduates & professionals seeking 
            careers in: banking, accountancy, finance, law, engineering, business,
            marketing & similar.
                <br>
                <br>
          
                      </div>
                </div>
           </div>
 
          <div class="row_fluid shadowboxb">
           
              <br>
              <hr>
              <br>
              <?php 
              
              $new=Tests::model()->published()->recently(20)->findAll();
              $mostpopular=Tests::model()->published()->popular(20)->findAll();
              $topfree=Tests::model()->published()->topfree(20)->findAll();
          echo TbHtml::tabbableTabs(array(
    array('label' => TbHtml::lead(' <i class="icon-circle-arrow-down"></i> New'), 'active' => true,
        'content' => $this->renderPartial('_newlyadded',array('new'=>$new),true)),
              
    array('label' =>TbHtml::lead(' <i class="icon-heart"></i> Most Popular') ,
        'content' => $this->renderPartial('_popular',array('popular'=>$mostpopular),true)),
     array('label' => TbHtml::lead('<i class="icon-unlock-alt"></i>Top Free') ,
         'content' => $this->renderPartial('_topfree',array('topfree'=>$topfree),true)),
    
)); ?>
             </div>   
          
           <div class="row_fluid indexlist">
              <?php 
              
              $new=Tests::model()->published()->recently(10)->findAll();
              $mostpopular=Tests::model()->published()->popular(10)->findAll();
              ?>
               <div class="row-fluid">
               <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><label class="label"> New</label> Newly Added</div>


                    <!-- List group -->
                    <ul class="list-group">
                        <?php  foreach ($new as $n){ ?>
                        <li class="list-group-item"><label class="label"> New</label>
                               <b><a href="<?php echo $this->createUrl('/test/tests/details',array('id'=>$n->id_test));?>">
                               <?php echo $n->test_name ?> 
                         </a></b></li>
                        <?php }?>


                    </ul>
                    </div>
               </div>
               <div class="row-fluid">
               <div class="panel panel-default">

                <div class="panel-heading"><i class="icon-heart"></i>   Popular Test </div>
                <!-- List group -->
                <ul class="list-group">
                    <?php  foreach ($mostpopular as $p){ ?>
                    <li class="list-group-item"><i class="icon-li icon-heart"></i>
                           <b><a href="<?php echo $this->createUrl('/test/tests/details',array('id'=>$p->id_test));?>">
                           <?php echo $p->test_name ?> 
                     </a></b></li>
                    <?php }?>


                </ul>
                </div>
               </div>
             </div>   
      </div>
      <div class="span4">
          <div class="row_fluid socials">
                
                    <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab"> <i class="icon-facebook-sign icon-3x"></i></a></li>
                    <li><a href="#tab2" data-toggle="tab"> <i class="icon-twitter-sign icon-3x"></i></a></li>
                    <li><a href="#tab3" data-toggle="tab"> <i class="icon-google-plus-sign icon-3x"></i></a></li>
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                    <div class="fb-like-box" data-href="https://www.facebook.com/aptitudecenter" data-width="295"
                            data-show-faces="true"
                            data-header="true" data-stream="false" 
                            data-show-border="true"></div>
                    </div>
                    <div class="tab-pane" id="tab2">
                    <p>Follow aptitudecenter  on twitter</p><br>
                        <a href="https://twitter.com/AptitudeCenter" class="twitter-follow-button" data-show-count="true" data-lang="en" data-size="large">Follow @twitter</a>
                        <br><p>The latest Twitter feed isn't piping through at the moment, check back soon!

                            Sorry for the inconvenience</p>
                    </div>
                   <div class="tab-pane" id="tab3">
                       
                    <p>Follow our page on GooglePlus</p><br>
                    <div class="g-plusone" data-annotation="inline" data-width="300" data-href="https://plus.google.com/102805766501482018369&quot; rel="></div>

                      <script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
                    </div>
                    </div>
                    </div>
             
          </div>
                <div class="row-fluid indexads">  
                <script type="text/javascript">
                ad_client = "pub-3763";
                ad_slot = 1173;
                ad_type = "hybrid-280-3";
                </script>
                <script type="text/javascript" src="http://promo.com.ng/scripts/adstation.js"></script>
                
                <script type="text/javascript">
ad_client = "pub-3763";
ad_slot = 1349;
ad_type = "hybrid-280-2";
</script>
<script type="text/javascript" src="http://promo.com.ng/scripts/adstation.js"></script>
                </div>
           </div>
      </div>
 

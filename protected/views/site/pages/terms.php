<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Terms and Condition';
$this->breadcrumbs=array(
	'Terms and Condition',
);
?>

<div class="row_fluid">
    
    <p>
	<strong>TERMS OF SERVICE</strong>
</p>
<p>
	<strong>1. ACCEPTANCE OF TERMS</strong>
</p>
<p>
	www.Aptitudecenter.com provides its service to you, subject to the following Terms of Service. These policies may be updated by us from time to time without notice of you. You can review our updated terms and conditions always in this page. Usage of this site is subject to these Terms, Conditions and Policies, therefore please read the following information carefully.
</p>
<p>
	<strong>2. USAGE OF THIS SITE</strong>
</p>
<p>
	www.Aptitudecenter.com is an Open Database and operates with a motto of collecting and sharing questions and answers that were asked in various interviews. This data is collected and submitted by various users/visitors around the world. www.Aptitudecenter.com does not guarantee the re-occurrences or repetition of questions in same interviews or any other interviews. The purpose of this site is to provide information to the public. Even though every care has been taken in compiling the information forwarded by certain enthusiastic users, www.Aptitudecenter.com doesn’t guarantee the accuracy of the content or information that provided in this site and won’t take responsible for any INCORRECT questions, answers, content and any form of errors. www.Aptitudecenter.com will not be responsible in anyway for any damages/consequences that might occur due to inclusion of some incorrect content or information in this site. You can use this information solely at your own risk. Users submitting questions or answers or code to this site, asserts that he or she owns that data or otherwise has the right to redistribute it freely. www.Aptitudecenter.com assumes no liability for disputes regarding ownership, copyright, or trademarks of the data submitted to this site. www.Aptitudecenter.com reserves the right to refuse to post any submission, to alter submissions, and to remove a submission from the site. www.Aptitudecenter.com may exercise this right without any advance notice to the individual submitting the code or content. All trademarks and company names published in this site are subjected to their respected owners and companies.
</p>
<p>
	<strong>3. REGISTRATION INTO SITE</strong>
</p>
<p>
	This is an OPTIONAL service provided by www.Aptitudecenter.com. Visitors can register into the site on their own interest, it is not mandatory. This registration into the site permits the user to maintain and modify of the data submitted to the site. The personal information that collected during this registration process will NOT, be distributed and shared with any other third-parties. While submitting registration form, you also agreed to provide and maintain true, accurate, current and complete information about yourself as prompted by the Service's registration form. www.Aptitudecenter.com NEVER verifies the accuracy of Personal Information that is submitted to the site, it is solely responsibility of the user. www.Aptitudecenter.com reserves the right to suspend or permanently delete any user, If the data or information provided by the user is inaccurate, not related or incomplete or if the user violated our terms and conditions in any manner.
</p>
<p>
	<strong>4. ANTI-SPAM POLICY</strong>
</p>
<p>
	www.Aptitudecenter.com is against in sending spam, unsolicited emails. You should not use our service and you are not authorized to use our referral or email services, for your personal or commercial purposes. By usage of this service you agreed that to our policies. Violating these policies in any manner subject to violation of respected laws and necessary action will be initiated against the defaulters.
</p>
<p>
	<strong>5. COPYRIGHT POLICY</strong>
</p>
<p>
	All pages, data and graphics presented on this website are the property of www.Aptitudecenter.com. The pages may not be redistributed or reproduced in any way, shape, or form without the written permission of www.Aptitudecenter.com. www.Aptitudecenter.com respects the copyrights, trademarks and intellectual property of others and also we expect this from other users. In this site, if you found any information that is owned by you or any content that violates your intellectual property rights, please contact to us with all necessary documents/information that authenticate your authority on your property.
</p>
<p>

</div>
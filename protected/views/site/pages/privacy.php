<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Privacy Policy';
$this->breadcrumbs=array(
	'privacy policy',
);
?>

<div class="row-fluid">	
<strong>PRIVACY POLICY</strong>
</p>
<p>
	The Personal information, email that submitted while registering to the site,
        will NOT be distributed, shared with any other third-parties. 
        We only use this data for our information, for research, to improve our 
        services and for contacting you purposes. www.Aptitudecenter.com is an Open Database Site, so Registering into site is NOT mandatory and also we won’t verify your email address. Registration into the site is purely of user’s interest. www.Aptitudecenter.com reserves the right to change this policy at any time. Any significant changes will be notified to you by sending an email to your email address that you provided while registering with us or by placing a notice on our site.
</p>
</div>
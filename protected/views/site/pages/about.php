<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>
<div class="row-fluid">
    <div class="span8">
 <?php  $questions= Questions::model()->count(); ?>
<h4>About aptitudecenter.com</h4>
<p> Aptitude center is an online platform where one can practice and prepare for any test or exam
    before the actual exam.
    you are allow to practices question with the answers and explanation shown. it is a good way to 
    practice and prepare of exams. there is also an Online CBT(Computer Base Test) aim at testing you based 
    on timing which is a simulation of the actual test. We have over <strong><?php echo $questions; ?> and still counting...</strong>
    We aim have questions in all aspects of life and exams for Nigerians and the world at large
    For Now we have 
    
</p>
<div class="accordion" id="accordion2">
    <div class="accordion-group">
        <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                WASSCE EXAMS
                </a>
        </div>
    <div id="collapseOne" class="accordion-body collapse in">
            <div class="accordion-inner">
            The West African Senior School Certificate Examination (WASSCE)
            is a type of standardized test in West Africa.
            It is administered by the West African Examinations Council . 
            It is only offered to candidates residing in Anglophone West African countries.
            There are two different types of the examination: Our database include 

            <li><strong>The WASSCE (November/December):</strong> Also known as the GCE by its old name (General Certificate Examinations),
                all students both ones from private and public schools are allowed to 
                take this examination, and uniforms are not compulsory. 
                However, other rules and regulations are applied on every candidate. 
                This examination is only offered during the autumn season, and the
                results are available by December.</li>
            <li><strong>The WASSCE (May/June):</strong>Also known as the SSCE by its old name 
                (Senior School Certificate Examinations), this examination is made for all
                private and public schools in West Africa. They must also wear distinctive
                uniforms as being described. This examination is offered during the summer(May to June), 
                and the results are available by August.</li>
            </div>
        </div>
    </div>
<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
        JAMB(UTME and POST-UTME)
        </a>
    </div>
    <div id="collapseTwo" class="accordion-body collapse">
            <div class="accordion-inner">
                The Joint Admissions and Matriculations Board (JAMB)
                is Nigeria's official entrance examination board for tertiary-level institutions. 
                The examinations being administered are available for most students who choose to
                apply to Nigerian public and private monotechnics, polytechnics, colleges of 
                education and universities. Most of these candidates must already have concluded 
                prior examinations, administered either by the West African Examinations C
                ouncil (WAEC) or the Nigerian National Examinations Council (NECO).
               <h4>What is JAMB UTME?</h4>
            JAMB used to conduct two matriculation examinations in Nigeria: the University
            Matriculation Examination (UME), and the matriculation examination targeted at
            Monotechnics, Polytechnics, and Colleges of Education (MPCE), also called Poly Jamb.

            From 2010 onwards, however, JAMB started conducting only one examination.
            The JAMB UME and MPCE were replaced by the Unified Tertiary Matriculation Examination (JAMB UTME),
            a matriculation examination for admission into all tertiary institutions.
            Unlike UME and MPCE results which are valid for only one year, the JAMB UTME 
            result is valid for two years.

            In addition, candidates for the UTME will select two Universities, 
            two Polytechnics, and two Colleges of Education during their registration. 
            Based on the UTME results, candidates will be allotted to any of the six
            institutions selected. The beauty of the UTME is that candidates who fail to
            secure admission into Universities still have the opportunity of securing 
            admission into Polytechnics and Colleges of Education instead of waiting at
            home for one whole year to write another JAMB.
            <br>
            <a href="http://www.studyandscholarships.com/2008/10/jamb-exam.html#ixzz2gTRXU300">Read more</a> 

            </div>
        </div>
    </div>
     <div class="accordion-group">
        <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                GRE (Graduate Record Examination General Test)
                
                </a>
        </div>
    <div id="collapseThree" class="accordion-body collapse in">
            <div class="accordion-inner">
                <h4>Graduate Record Examination General Test (GRE)</h4>
           <p>The Graduate Record Examination General Test (GRE) is for 
            college graduates or soon-to-be graduates who are planning
            to attend graduate school or business school. GRE scores are
            used by admissions departments to be included in application
            packages and can also be used to support scholarship applications.
            The exam requires the test taker to think the way they would have to 
            in graduate or business school and is a way to determine whether he 
            or she is prepared for graduate work. The test can be taken on 
            a continuous basis at any time throughout the year for the 
            computer-based exam, although paper exams are only available on a limited basis</p>
            </div>
        </div>
    </div>
    <div class="accordion-group">
        <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                GMAT
                </a>
        </div>
    <div id="collapseFour" class="accordion-body collapse in">
            <div class="accordion-inner">
                <h4>GMAT (The Graduate Management Admission Test)</h4>

The Graduate Management Admission Test (GMAT) is a test that has been produced 
by the Graduate Management Admission Council (GMAC), and is used to help business 
schools in making admissions decisions. It is taken by students who are applying
for admission to either MBA or other graduate management programs.
The test is given in English only, and it tests analytical writing,
integrated reasoning, quantitative, and verbal skills. The GMAT is always taken on
a computer, and, with the exception of the essay, is in multiple-choice format.
            </div>
        </div>
    </div>
    <div class="accordion-group">
        <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
                SAT(Scholastic Assessment Test)
                </a>
        </div>
    <div id="collapseFive" class="accordion-body collapse in">
            <div class="accordion-inner">
<p>The SAT test, also known as the SAT Reasoning Test
    is used in the application process to colleges and universities in the United States.
    The test measures critical thinking skills and the ability to analyze and solve problems,
    and is often thought of as a measure of future college success. 
    The SAT test is administered by the Educational Testing Service (ETS)
    at various locations across the country, and it is developed, published,
    and scored by the College Board.</p>
            </div>
        </div>
    </div>
</div>
</div>
    <div class="span4">
        <script type="text/javascript">
ad_client = "pub-3763";
ad_slot = 1347;
ad_type = "hybrid-280-3";
</script>
<script type="text/javascript" src="http://promo.com.ng/scripts/adstation.js"></script>
<script type="text/javascript">
ad_client = "pub-3763";
ad_slot = 1212;
ad_type = "hybrid-280-3";
</script>
<script type="text/javascript" src="http://promo.com.ng/scripts/adstation.js"></script>
    </div>
 </div>
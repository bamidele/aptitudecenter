<?php /* @var $this Controller */
Yii::setPathOfAlias('_partials', dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'_partials'); ?>
<!DOCTYPE html>
<html lang="en" xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/dist/css/bootstrap-theme.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/dist/css/bootstrap2.css" />
     <meta property="og:image" content="http://aptitudecenter.com/images/logo.png"/>
	<?php Yii::app()->controller->widget('ext.seo.widgets.SeoHead',array(
    'defaultDescription'=>'Learn and practice Aptitude questions and answers with explanation for interview,Nigerian Exams, WAEC, NECO, Job Interviews, GRE,GMAT and TOEFL competitive examination and entrance test.',
    'defaultKeywords'=>'aptitude, questions, answers, interview, placement,reasoning, program, verbal, numerical, knowledge, mechanical, non-verbal, spatial, problem, 
        online, test, exam, comprehension, vocabulary papers,GRE, WAEC,JAMB, NECO, GCE, TOEFL, GRE,GMAT, SAT, Verbal, Quantitative, 2013, reasoning, program, verbal, gk, knowledge, language, explanation, solution, problem, online, test, exam, quiz',
)); ?>

	<title><?php 
        //YumMembership::syncMemberships();
        echo $this->pageTitle; ?></title>
           
         <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/style.css" />
         
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.countdown.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/font-awesome/css/font-awesome.min.css">
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.countdown.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.paginate.js"></script>
	
            <?php 
        Yii::app()->bootstrap->register();
        //<script type="text/javascript" src=" echo Yii::app()->baseUrl;/dist/js/bootstrap2.min.js">
?>
        
        
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/styles.css" />
        
        <link rel="shortcut icon" href="<?php echo baseUrl('favicon.ico') ?>" />
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</head>

<body>
    <div id="fb-root"></div>

<script>
    (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=544170282315737";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

</script>

    <div class="row-fluid">
    <div class="container-fluid" id ="page">
    <div class= "row-fluid topheader">
         <div class="span3 logo">
            <a href="<?php echo Yii::app()->getBaseUrl(true); ?>"><img src="<?php echo Yii::app()->baseurl; ?>/images/logo.png"/></a> 
         </div>
         <div class="span4">
             <div class="row-fluid searchtop">
                     <?php echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_SEARCH,''); ?>
      <?php echo TbHtml::textField('search', '',
    array('append' => TbHtml::button('Search!'), 'span' => 12)); ?>
    <?php echo TbHtml::endForm(); ?>
             </div>
            </div>          
            <div class="span5 righttop">
                <ul class="inline">
					<li><a href="<?php echo Yii::app()->baseurl; ?>" accesskey="1" title="Home">Home</a></li>
					<li><a href="<?php echo Yii::app()->baseurl; ?>/forum" title="forum">Forum</a></li>
					<li><a href="<?php echo Yii::app()->baseurl; ?>/contact.html" title="Contact Us">Contact Us</a></li>
					<li><a href="<?php echo Yii::app()->baseurl; ?>/advertiser.html" title="Advertise with us">Advertise with Us</a></li>
					<li><a href="<?php echo Yii::app()->baseurl; ?>/terms.html" accesskey="3" title="Terms and Condition">Terms</a></li>
                </ul>

            </div>
</div>
<div class="row-fluid">
<div class="span12" style="">
        
  <?php 
   
    
    if(user()->isAdmin()){
 $items=$this->createWidget('ext.menubuilder.widgets.EMBMenu', array(
                'menuIds'=>array('main'),
                'scenarios' =>'backend',
                //'userRoles'=>'admin',
                'menuBuilderItemsBefore' => true,
                'menuOptions'=>array(
                    'items'=>array(
                        array('url'=>array('/menubuilder'),'label'=>'Menubuilder'),
                     	
                        array('label'=>'Logout ('.user()->name.')', 'url'=>array('//user/user/logout'), 'visible'=>!user()->isGuest),
                        array(
                                        //'icon'=>'icon-tasks',
                                        'label'=>'Login',
                                        'url'=>'javascript:void(0)',
                                        'visible'=>user()->isGuest,
                                        'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>'dropdown'),
                                        'itemOptions'=>array('class'=>'grey'),
                                        'class'=>'ext.bootstrap-ext.widgets.TbDropdownExt',
                                        'view'=>'_partials._usermenu_loginform',
                                        'submenuOptions'=>array('class' => 'fixed-panel pull-right dropdown-navbar dropdown-caret dropdown-closer'),
                            ),
                        
                    )
                ),
               )
              )->getItems();

        $this->widget('bootstrap.widgets.TbNavbar', array(
            'collapse' => true,
            'display'=> TbHtml::NAVBAR_DISPLAY_NONE,
            'brandLabel'=>false,
            'items' => array(
                array(
                    //'class' => 'bootstrap.widgets.TbNav',
                    //'encodeLabel'=>false,
                   
                    //'items' => $items,
                    'class'=>'ext.bootstrap-ext.widgets.TbMenuExt',
                    'htmlOptions'=>array('class'=>'pull-right'),
                    'items'=>$items,
                    
                )
            ),
            
        ));
    }
    else{

            $this->widget('application.modules.test.components.CategoryWidget');
        }
        ?>
    </div>
</div>
        <!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5231aaa22fdab04e"></script>
<script type="text/javascript">
  addthis.layers({
    'theme' : 'transparent', 
    'follow' : {
      'services' : [
        {'service': 'facebook', 'id': 'aptitudecenter'},
        {'service': 'twitter', 'id': 'AptitudeCenter'},
        {'service': 'google_follow', 'id': '102805766501482018369'}
      ]
    }   
  });
</script>
<!-- AddThis Smart Layers END -->
         <div class="row-fluid topads">
        <script type="text/javascript">
      ad_client = "pub-3763";
      ad_slot = 1;
      ad_type = "imagead-728-1";
      </script>
<script type="text/javascript" src="http://openadnation.com/scripts/adstation.js"></script>
             
         </div>
       <div class="row-fluid topfortab">
                 
<script type="text/javascript">
      ad_client = "pub-3763";
      ad_slot = 1;
      ad_type = "mtext-320-1";
      </script>
<script type="text/javascript" src="http://openadnation.com/scripts/adstation.js"></script>
             </div>
<div class="row-fluid topadsmobile">
                 
<script type="text/javascript">
      ad_client = "pub-3763";
      ad_slot = 1;
      ad_type = "mtext-320-2";
      </script>
<script type="text/javascript" src="http://openadnation.com/scripts/adstation.js"></script>
             </div>
    <?php if(!empty($this->breadcrumbs)):?>
                <div class="row-fluid crumbs">
		<?php $this->widget('bootstrap.widgets.TbBreadcrumb', array(
			'links'=>$this->breadcrumbs,
		)); ?>
                </div>
		<?php endif?>
               
               <div class="row-fluid mainCnt">
                 <?php echo $content; ?>
               </div>
            <hr />
    </div>
 </div>    
    <div class="row-fluid">
        
            <div id="footer">
                <div class="row_fluid footertop">
                <div class="span4">
                    <?php echo TbHtml::lead('Popular Links') ?>
                   
                    <ul class="icons-ul">
					<li><a href="<?php echo Yii::app()->baseurl; ?>" accesskey="1" title="Home">Home</a></li>
					<li><a href="<?php echo Yii::app()->baseurl; ?>/forum" title="forum">Forum</a></li>
					<li><a href="<?php echo Yii::app()->baseurl; ?>/about.html" title="About Us">About Us </a></li>
					<li><a href="<?php echo Yii::app()->baseurl; ?>/advertiser.html" title="Advertise with us">Advertise with Us</a></li>
					<li><a href="<?php echo Yii::app()->baseurl; ?>/terms.html" accesskey="3" title="Terms and Condition">Terms</a></li>
                                        <li><a href="<?php echo Yii::app()->baseurl; ?>/privacy.html" title="Privacy Policy">Privacy Policy</a></li>
                    </ul>
                </div> 
                <div class="span4">
                    <?php echo TbHtml::lead('Popular Tests') ;
                    $mostpopular=Tests::model()->published()->popular(7)->findAll();
                    
                    ?>
                    <ul class="icons-ul">
                        <?php  foreach($mostpopular as $p): ?>
					<li><a href="<?php echo Yii::app()->baseurl; ?>/test/tests/details/id/
                                            <?php  echo $p->id_test; ?>"  title="<?php  echo $p->test_name; ?>"><?php  echo $p->test_name; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div> 
                <div class="span4">
                   <?php echo TbHtml::lead('Contact us') ?>
                    
                    <ul class="icons-ul">
					<li><i class="icon-envelope icon-2x"></i>  &nbsp info@aptitudecenter.com</li>
					<li><i class="icon-phone icon-2x"></i> &nbsp (+234)7030628331</li>
                                        
                    </ul>
                    <?php echo TbHtml::lead('<i class="icon-group"></i>     Our Community') ?>
                    
                    <ul class="icons-ul">
                    <li><a href="https://www.facebook.com/aptitudecenter"><i class="icon-facebook-sign icon-3x"></i></a> &nbsp
                    <a href="https://twitter.com/AptitudeCenter"><i class="icon-twitter-sign icon-3x"></i></a> &nbsp
                    <a href="https://plus.google.com/102805766501482018369"><i class="icon-google-plus-sign icon-3x"></i></a>
                    </li>
					
                                        
                    </ul>
                </div> 
                         </div>
                <div class="row-fluid footer">
                    Copyright &copy; <?php echo date('Y'); ?> by BamiSoft.<br/>
                                All Rights Reserved.<br/>
                                <script id="_waulp3">var _wau = _wau || []; _wau.push(["small", "2nbkare2g6vp", "lp3"]);
(function() {var s=document.createElement("script"); s.async=true;
s.src="http://widgets.amung.us/small.js";
document.getElementsByTagName("head")[0].appendChild(s);
})();</script>
                   </div>
          </div>
        
   </div>
<script>
$(document).ready(function(){
   $('#register').click(function(){
       window.location.href = "<?php echo $this->createUrl('/registration/registration'); ?>"; 
   });
  
});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44006506-1', 'aptitudecenter.com');
  ga('send', 'pageview');

</script>
    
</body>
</html>

<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row-fluid">
    <div class="span9">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
    <div class="span3">
        
        <?php
            
            $this->widget('bootstrap.widgets.TbNav', array(
                'type' => TbHtml::NAV_TYPE_LIST,
                'items'=>$this->menu,
                'htmlOptions'=>array('class'=>'well'),
            ));
          
        ?>
       
    </div>
    
</div>
<?php $this->endContent(); ?>
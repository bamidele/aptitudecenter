-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2013 at 02:54 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `no` int(100) NOT NULL AUTO_INCREMENT,
  `question` varchar(5000) NOT NULL,
  `option1` varchar(100) NOT NULL,
  `option2` varchar(100) NOT NULL,
  `option3` varchar(100) NOT NULL,
  `option4` varchar(100) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`no`, `question`, `option1`, `option2`, `option3`, `option4`) VALUES
(1, 'What will be the output of the following statement ?\r\n\r\nprintf("%X%x%ci%x",11,10,''s'',12);\r\n', ' error', 'basc', 'Bas94c', 'none of these'),
(2, '#include<stdio.h>\r\nint main(){\r\n  char *str;\r\n  scanf("%[^\\n]",str);\r\n  printf("%s",str); \r\n  return 0;\r\n}', 'It will accept a word as a string from user.', 'It will accept a sentence as a string from user.', 'It will accept a paragraph as a string from user.', 'Compiler error'),
(3, ' void main()\r\n{\r\nint a=10,b=20;\r\nchar x=1,y=0;\r\nif(a,b,x,y)\r\n{\r\nprintf("EXAM");\r\n}\r\n}\r\nWhat is the output?\r\n', 'XAM is printed', 'exam is printed', 'Compiler Error', 'Nothing is printed'),
(4, '#include<stdio.h>\r\nint main(){\r\n  char *url="c:\\tc\\bin\\rw.c";\r\n  printf("%s",url); \r\n  return 0;\r\n}\r\nWhat will be the output of the program?', 'c:\\tc\\bin\\rw.c', 'c:/tc/bin/rw.c', 'c: c inw.c', 'c:cinw.c'),
(5, '#include<stdio.h>\r\nint main(){\r\n  int x=5,y=10,z=15;\r\n  printf("%d %d %d"); \r\n  return 0;\r\n}\r\nWhat will be the output of the program?', 'Garbage Garbage Garbage', '5 10 15', '15 10 5', 'Compiler error'),
(6, 'What will be the output of the program if the array begins at address 65486? \r\n#include<stdio.h>\r\n\r\nint main()\r\n{\r\n    int arr[] = {12, 14, 15, 23, 45};\r\n    printf("%u, %u\\n", arr, &arr);\r\n    return 0;\r\n}\r\n', '65486, 65488', '65486, 65486', '65486, 65490', '65486, 65487'),
(7, '#include<stdio.h>\r\nint main(){\r\n  int i=11;\r\n  int const * p=&i;\r\n  p++;\r\n  printf("%d",*p); \r\n  return 0;\r\n}\r\nWhat is the output?\r\n', '11', '12', 'Garbage value', 'Compiler error'),
(8, 'void *ptr;\r\n int  myArray[10];\r\nptr = myArray;\r\nWhich of the following is the correct way to increment the variable "ptr"?\r\n', 'ptr = ptr + sizeof(myStruct);', '++(int*)ptr;', 'ptr = ptr + sizeof(int);     ', 'increment(ptr);'),
(9, '#define PRINT printf("c");printf("c++");\r\nint main(){\r\n  float a=5.5;\r\n  if(a==5.5)\r\n    PRINT\r\n  else\r\n    printf("Not equal"); \r\n  return 0;\r\n}\r\nWhat will be the output of the program?', 'c c++', 'Not equal', 'cc++', 'Compiler error'),
(10, 'main()\r\n{\r\nstatic char names[5][20]={"pascal","ada","cobol","fortran","perl"};\r\nint i;\r\nchar *t;\r\nt=names[3];\r\nnames[3]=names[4];\r\nnames[4]=t;\r\nfor (i=0;i<=4;i++)\r\nprintf("%s",names[i]);\r\n}\r\n', 'Compiler error: Lvalue required in function main', 'Output:pascal,ada,cobol,perl,fortran', 'Output: cobol,perl,fortran, pascal,ada', 'Output: cobol,perl,fortran,ada'),
(11, '#include<stdio.h>\r\n#define call(x,y) x##y\r\nint main(){\r\nint x=5,y=10,xy=20;\r\nprintf("%d",xy+call(x,y));\r\nreturn 0;\r\n\r\n}\r\nWhat will be the output of the program?', '35', '510', '15', '40'),
(12, '#include<stdio.h>\r\nint check (int, int);\r\n\r\nint main()\r\n{\r\n    int c;\r\n    c = check(10, 20);\r\n    printf("c=%d\\n", c);\r\n    return 0;\r\n}\r\nint check(int i, int j)\r\n{\r\n    int *p, *q;\r\n    p=&i;\r\n    q=&j;\r\n    i>=45 ? return(*p): return(*q);\r\n}\r\nWhat will be the output of the program?\r\n', 'Print 10', 'Print 20', 'Print 1', 'Compiler error'),
(13, 'Which of the following statements are correct about the program? \r\n#include<stdio.h>\r\n\r\nint main()\r\n{\r\n    unsigned int num;\r\n    int c=0;\r\n    scanf("%u", &num);\r\n    for(;num;num>>=1)\r\n    {\r\n        if(num & 1)\r\n            c++;\r\n    }\r\n    printf("%d", c);\r\n    return 0;\r\n}\r\n', 'It counts the number of bits that are ON (1) in the number num.', 'It counts the number of bits that are OFF (0) in the number num.', 'It sets all bits in the number num to 1', 'Error'),
(14, 'int z,x=5,y=-10,a=4,b=2; \r\nz = x++ - --y * b / a;\r\nWhat number will z in the sample code above contain?\r\n', '5', '6', '10', '11'),
(15, ' "My salary was increased by 15%!" \r\nSelect the statement which will EXACTLY reproduce the line of text above.\r\n', 'printf("\\"My salary was increased by 15/%\\!\\"\\n");', 'printf("My salary was increased by 15%!\\n");', 'printf("My salary was increased by 15''%''!\\n");', 'printf("\\"My salary was increased by 15%%!\\"\\n");'),
(16, ' int a=10,b;\r\nb=a++ + ++a;\r\nprintf("%d,%d,%d,%d",b,a++,a,++a);\r\nwhat will be the output when following code is executed\r\n', '12,10,11,13', '22,10,11,13', '12,11,11,11', '22,13,13,13'),
(17, ' int x = 0; \r\nfor (x=1; x<4; x++); \r\nprintf("x=%d\\n", x);\r\nWhat will be printed when the sample code above is executed? \r\n', 'x=0 ', 'x=1', 'x=3', 'x=4'),
(18, 'double x = -3.5, y = 3.5; \r\nprintf( "%.0f : %.0f\\n", ceil( x ), ceil( y ) ); \r\nprintf( "%.0f : %.0f\\n", floor( x ), floor( y ) );\r\nhat will the code above print when executed?\r\n', '-3 : 4 <br> -4 : 3 ', '-4 : 4 <br> -3 : 3 ', '-4 : 3<br> -4 : 3 ', '-4 : 3 <br>  -3 : 4'),
(19, '	#include <stdio.h> \r\nint i; \r\nvoid increment( int i ) \r\n{	 \r\n 	  i++; \r\n} \r\n\r\nint main() \r\n{ \r\n 	  for( i = 0; i < 10; increment( i ) ) \r\n 	  { \r\n  	 } \r\n  	 printf("i=%d\\n", i); \r\n  	 return 0; \r\n}\r\n\r\nWhat will happen when the program above is compiled and executed? \r\n', 'It will not compile. ', 'It will print out: i=9. ', 'It will print out: i=11. ', 'It will loop indefinitely'),
(20, '  int i = 4; \r\nint x = 6; \r\ndouble z; \r\nz =  x / i; \r\nprintf("z=%.2f\\n", z);\r\nWhat will print when the sample code above is executed?\r\n', 'z=0.00 ', 'z=1.00 ', 'z=1.50 ', 'z=2.00'),
(21, 'char * dwarves [] = { \r\n  "Sleepy", \r\n  "Dopey" "Doc", \r\n  "Happy", \r\n  "Grumpy" "Sneezy", \r\n  "Bashful", \r\n};\r\nHow many elements does the array dwarves (declared above) contain? Assume the C compiler employed strictly complies with the requirements of Standard C. \r\n', '4', '5', '6', '7'),
(22, 'main()\r\n{\r\nprintf("\\nab");\r\nprintf("\\bsi");\r\nprintf("\\rha");\r\n}\r\nWhat will be the output of the program', 'ab <br>si<br> ha', 'absiha', 'hai', 'ash'),
(23, 'What will be the output of the program? \r\n\r\nmain()\r\n{ clrscr();\r\n} clrscr();\r\n', 'Error:Type Mismatch in Redeclaration', 'No output/error', 'Error:Function Declaration missing', 'Error:Function call outside main'),
(24, 'void main()\r\n{\r\nchar far *farther,*farthest;\r\nprintf("%d..%d",sizeof(farther),sizeof(farthest));\r\n}\r\nWhat will be the output of the program? \r\n', '4..2', '4..4', '1..4', '4..1'),
(25, 'struct marks{\r\nint p:3;\r\nint c:3;\r\nint m:2;\r\n};\r\nvoid main(){\r\nstruct marks s={2,-6,5};\r\nprintf("%d %d %d",s.p,s.c,s.m); \r\n}\r\nWhat will be the output of the program? \r\n', '2 2 1', '3 3 2', '2 3 2', '2 2 2'),
(26, 'main()\r\n{\r\nchar *ptr = "Ramco Systems";\r\n(*ptr)++;\r\nprintf("%s\\n",ptr);\r\nptr++;\r\nprintf("%s\\n",ptr);\r\n}\r\nWhat will be the output of the program? \r\n', ' Ramco Systems <br>amco systems', 'Samco Systems<br> amco systems', 'Ramco Systems <br>Ramco Systems', 'Error'),
(27, 'What will be the output of the program?\r\n#include<stdio.h>\r\nint main()\r\n{\r\n    float a = 0.7;\r\n    if(0.7 > a)\r\n        printf("Hi\\n");\r\n    else\r\n        printf("Hello\\n");\r\n    return 0;\r\n}\r\n', 'Hi', 'Hello', 'Hi Hello', 'None of above	 '),
(28, 'Assuming a integer 2-bytes, What will be the output of the program?\r\n#include<stdio.h>\r\nint main()\r\n{\r\n    printf("%x\\n", -1<<3);\r\n    return 0;\r\n}\r\n', 'ffff', 'fff8', '0 ', '-1'),
(29, 'What will be the output of the program?\r\n#include<stdio.h>\r\n#define SQR(x)(x*x)\r\nint main()\r\n{\r\n    int a, b=3;\r\n    a = SQR(b+2);\r\n    printf("%d\\n", a);\r\n    return 0;\r\n}\r\n', '25', '11', 'Error', '10'),
(30, 'What will be the output of the program (sample.c) given below if it is executed from the command line (Turbo C in DOS)?\r\ncmd> sample 1 2 3\r\n/* sample.c */\r\n#include<stdio.h>\r\n\r\nint main(int argc, char *argv[])\r\n{\r\n    int j;\r\n    j = argv[1] + argv[2] + argv[3];\r\n    printf("%d", j);\r\n    return 0;\r\n}\r\n', '6', 'sample 6', 'Error', 'Garbage value'),
(31, 'On executing the below program what will be the contents of ''target.txt'' file if the source file contains a line "To err is human"?\r\n#include<stdio.h>\r\nint main()\r\n{\r\n    int i, fss;\r\n    char ch, source[20] = "source.txt", target[20]="target.txt", t;\r\n    FILE *fs, *ft;\r\n    fs = fopen(source, "r");\r\n    ft = fopen(target, "w");\r\n    while(1)\r\n    {\r\n        ch=getc(fs);\r\n        if(ch==EOF)\r\n            break;\r\n        else\r\n        {\r\n            fseek(fs, 4L, SEEK_CUR);\r\n            fputc(ch, ft);\r\n        }\r\n    }\r\n    return 0;\r\n}\r\n', 'r n', 'Trh', 'err', 'None of above'),
(32, 'What will be the output of the program?\r\n#include<stdio.h>\r\nint main()\r\n{\r\n    int i=4, j=-1, k=0, w, x, y, z;\r\n    w = i || j || k;\r\n    x = i && j && k;\r\n    y = i || j &&k;\r\n    z = i && j || k;\r\n    printf("%d, %d, %d, %d\\n", w, x, y, z);\r\n    return 0;\r\n}\r\n', '1, 1, 1, 1', '1, 1, 0, 1', '1, 0, 0, 1', '1, 0, 1, 1'),
(33, 'Point out the error, if any in the program.\r\n#include<stdio.h> \r\nint main()\r\n{\r\n    int a = 10, b;\r\n    a >=5 ? b=100: b=200;\r\n    printf("%d\\n", b);\r\n    return 0;\r\n}\r\n', '100', '200', 'Error: L value required for b', 'Garbage value'),
(34, 'What will be the output of the program in 16-bit platform (Turbo C under DOS) ?\r\n#include<stdio.h>\r\n\r\nint main()\r\n{\r\n    printf("%d, %d, %d", sizeof(3.0f), sizeof(''3''), sizeof(3.0));\r\n    return 0;\r\n}\r\n', '8, 1, 4', '4, 2, 8', '4, 2, 4', '10, 3, 4'),
(35, 'Which of the following statements should be used to obtain a remainder after dividing 3.14 by 2.1 ?', 'rem = 3.14 % 2.1;', 'rem = modf(3.14, 2.1);', 'rem = fmod(3.14, 2.1);', 'Remainder cannot be obtain in floating point division.'),
(36, 'main()\r\n{\r\nchar *p = “ayqm”;\r\nprintf (“%c”, ++*(p++));}\r\nWhat will be the output of the program? \r\n', 'b', ' z', 'q', 'n'),
(37, ' What will be the output of the following program?\r\nmain()\r\n{\r\nint i = 5;\r\nprintf(“%d”, i=++i==6);\r\n}\r\n', '0', '1', '7', '6'),
(38, 'The output of the following is\r\nint a = 75;\r\nprintf (“%d%%”, a);\r\n', ' 75', '75%%', '75% ', 'None of the above'),
(39, 'What will happen if the following loop is executed?\r\nint num = 0;\r\ndo\r\n{\r\n--num;\r\nprintf(“%d”, num);\r\nnum++;\r\n}while (num >= 0);\r\n}\r\n', 'The loop will run infinite number of times.', 'The program will not enter the loop.', 'There will be a compilation error.', 'There will be runtime error.'),
(40, ' What will be the output of following program?\r\nmain()\r\n{\r\nint x=15;\r\nprintf(“\\n%d%d%d”, x!=15, x=20, x<30);\r\n}\r\n', '0, 20, 1 ', '15, 20, 30', '0, 0, 0 ', 'Error'),
(41, 'How many times the following loop be executed\r\n{\r\n…\r\nch = ‘b’;\r\nwhile(ch >= ‘a’ && ch <= ‘z’)\r\nch++; }\r\n', '0', '25', '26', '1'),
(42, '#include<stdio.h>\r\ndouble i;\r\n\r\nint main()\r\n{\r\n    (int)(float)(char) i;\r\n    printf("%d", sizeof((int)(float)(char)i));\r\n    return 0;\r\n}\r\nWhat will be the output of the program? \r\n', '2', 'Error', '4', '8'),
(43, 'int i;\r\nmain(){\r\nint t;\r\nfor ( t=4;scanf("%d",&i)-t;printf("%d\\n",i))\r\nprintf("%d--",t--);\r\n}\r\nWhat will be the output of the program? \r\n// If the inputs are 0,1,2,3 find the o/p\r\n', '4--0<br> 3--1', '4--0<br> 3--1<br>2--2', '3--1<br> 2--2', 'Error'),
(44, 'main( )\r\n{\r\nstatic int a[ ] = {0,1,2,3,4};\r\nint *p[ ] = {a,a+1,a+2,a+3,a+4};\r\nint **ptr = p;\r\nptr++;\r\nprintf(“\\n %d %d %d”, ptr-p, *ptr-a, **ptr);\r\n*ptr++;\r\nprintf(“\\n %d %d %d”, ptr-p, *ptr-a, **ptr);\r\n*++ptr;\r\nprintf(“\\n %d %d %d”, ptr-p, *ptr-a, **ptr);\r\n++*ptr;\r\nprintf(“\\n %d %d %d”, ptr-p, *ptr-a, **ptr);\r\n}\r\nWhat will be the output of the program? \r\n', 'Error', '111<br>222<br>333<br>444', '111<br>222<br>333<br>344', '111<br>222<br>333<br>343'),
(45, 'main( )\r\n{\r\nvoid *vp;\r\nchar ch = ‘g’, *cp = “goofy”;\r\nint j = 20;\r\nvp = &ch;\r\nprintf(“%c”, *(char *)vp);\r\nvp = &j;\r\nprintf(“%d”,*(int *)vp);\r\nvp = cp;\r\nprintf(“%s”,(char *)vp + 3);\r\n}\r\nWhat will be the output of the program? \r\n', 'oofy', 'goofy', 'g20fy', 'edcbji'),
(46, 'main()\r\n{ char *\r\ncptr,c;\r\nvoid *vptr,v;\r\nc=10; v=0;\r\ncptr=&c; vptr=&v;\r\nprintf("%c%v",c,v);\r\n}\r\nWhat will be the output of the program? \r\n', 'a.	Compiler error: size of v is Unknown', '100', 'Garbage Value', '10 0'),
(47, 'void main()\r\n{\r\nint *mptr, *cptr;\r\nmptr = (int*)malloc(sizeof(int));\r\nprintf(“%d”,*mptr);\r\nint *cptr = (int*)calloc(sizeof(int),1);\r\nprintf(“%d”,*cptr);\r\n}\r\nWhat will be the output of the program? \r\n', '0 0', 'garbage-value 0', 'Error', 'garbage-value garbage-value'),
(48, 'What will be the output of the following statements ?\r\n\r\nlong int a = scanf("%ld%ld",&a,&a); printf("%ld",a);\r\n', ' error', 'garbage value', ' 0', '2'),
(49, '#include<stdio.h>\r\nint main(){\r\n  float f=3.4e39;\r\n  printf("%f",f); \r\n  return 0;\r\n}\r\nWhat will be the output of the program? \r\n', '3.4e39', '3.40000…', '+INF', 'Compiler error'),
(50, '#include<stdio.h>\r\nint main(){\r\n  asm{\r\n    mov bx,8;\r\n    mov cx,10\r\n    add bx,cx;\r\n  }\r\n  printf("%d",_BX); \r\n  return 0;\r\n}\r\nWhat will be the output of the program? \r\n', '18', '8', '0', 'Compiler error');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
